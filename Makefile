OBJS = helm2d.o assembly.o modify.o getabc.o mypart.o myopts.o computeA.o computef.o solutions.o kspdd.o kspdirect.o checksol.o NelderMead.o search.o
default: clean helm2d
helm2d: $(OBJS)
#-@${MKDIR} bin
	${CLINKER} -o $@ $^ -llapacke -llapack ${PETSC_LIB}
#	${DSYMUTIL} $@
	${RM} -f *.o
clean::
	${RM} helm2d
#	${RM} -r helm2d.dSYM

NMtest: NMtest.o NelderMead.o
	${CLINKER} -o $@ $^ ${PETSC_LIB}
	${RM} -f *.o
readvec: readvec.o
	${CLINKER} -o $@ $^ ${PETSC_LIB}
	${RM} -f *.o
PETSC_DIR= /Users/huizhang/Software/petsc
PETSC_ARCH= z-debug
include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules
#include ${PETSC_DIR}/conf/base
#include ${PETSC_DIR}/conf/test
