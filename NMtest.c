static char help[]= "test NelderMead.c";

#include "petscsys.h"
#include <stdio.h>

typedef struct
{
  int nx; /* dim of x */
} myfun_ctx;

PetscReal myfun(PetscReal *x, myfun_ctx *ctx);

/* ----------------------------------------------------------------------- 
   A test driver for NelderMead.c
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **args)
{
  myfun_ctx ctx;
  PetscReal *x, *xopt, fval;
  PetscInt i, iter;
  PetscErrorCode ierr;  

  PetscInitialize(&argc,&args,PETSC_NULL,help);

  ctx.nx= 4;
  ierr= PetscMalloc((ctx.nx+1)*ctx.nx*sizeof(PetscReal),&x);CHKERRQ(ierr);
  for (i = 0; i < ctx.nx; ++i) {
    x[i]= i*1.0;
  }
  fval=  myfun(x,&ctx);
  printf("fval= %g\n",fval);
  
  /* initial simplex */
  for (i = 1; i < ctx.nx+1; ++i) {
    ierr= PetscMemcpy(x+i*ctx.nx,x,ctx.nx*sizeof(PetscReal));CHKERRQ(ierr);
    x[i*ctx.nx+i-1] += 0.05;
  } 
  x[3*ctx.nx]+= 1;
  /* optimization by Nelder-Mead */
  ierr= PetscMalloc(ctx.nx*sizeof(PetscReal),&xopt);CHKERRQ(ierr);
  iter= NelderMead(xopt,&fval,myfun,&ctx,ctx.nx,x,1e-4,1e-4,500);

  PetscFinalize();
  return 0;
}

#undef __FUNCT__ 
#define __FUNCT__ "myfun"
PetscReal myfun(PetscReal *x, myfun_ctx *ctx)
{
  PetscInt i;
  PetscReal result;

  result= 0;
  for (i = 0; i < ctx->nx; ++i) {
    result += x[i]*x[i];
  }
  
  return(result);
}
