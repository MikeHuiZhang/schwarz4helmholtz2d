#include "petscsys.h"


/* ----------------------------------------------------------------------- 
   NelderMead( PetscReal *xopt, PetscReal *fopt, 
     PetscReal (*f)(PetscReal *x, void *ctx), const void *ctx, PetscInt nx, 
     const PetscReal *x0, PetscReal tolx, PetscReal tolf, PetscInt maxiter )
     tries to find a minimum of the target function 'f'
  
   Reference: Martin's NelderMead.m, 2010 Gao implementing the Nelder-Mead
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "NelderMead"
PetscInt NelderMead(PetscReal *xopt, PetscReal *fopt, PetscReal (*f)(PetscReal *x, void *ctx), void *ctx, PetscInt nx, const PetscReal *x0, PetscReal tolx, PetscReal tolf, PetscInt maxiter)
{
  PetscReal s= 0.5, g= 0.5, c= 2., r= 1.; /* parameters for Nelder-Mead, c.f. NelderMead.m */
  PetscInt k; /* Nelder-Mead iteration counter */
  PetscReal *fk, fr, fe, fi, fo, fkj; /* function values at various x */
  PetscReal *x, *xj, *xb, *xr, *xe, *xi, *xo; /* coordinates of simplex vertices */
  PetscErrorCode ierr;
  PetscInt i, j;
  PetscInt ptsize; /* bytes for coordinates of one point */
  PetscReal disx, disf; 

  PetscFunctionBegin;
  
  ptsize= nx*sizeof(PetscReal);
  ierr= PetscMalloc((nx+1)*ptsize,&x);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xj);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xb);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xr);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xe);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xi);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&xo);CHKERRQ(ierr);
  ierr= PetscMalloc((nx+1)*sizeof(PetscReal),&fk);CHKERRQ(ierr);

  k= 0;
  ierr= PetscMemcpy(x,x0,(nx+1)*ptsize);CHKERRQ(ierr);
  for (i = 0; i < nx+1; ++i) {
    fk[i]= f(x+i*nx,ctx);
  }
  disx= tolx + 1.0;  disf= tolf + 1.0;
  while (k < maxiter  && disx > tolx && disf > tolf){ 
    /* sort fk and x ascendingly */
    for (i = 0; i < nx; ++i) {
      for (j = 0; j < nx-i; ++j) {
	if (fk[j]>fk[j+1]) {
	  fkj= fk[j];  fk[j]= fk[j+1]; fk[j+1]= fkj;
	  ierr= PetscMemcpy(xj,x+j*nx,ptsize);CHKERRQ(ierr);
	  ierr= PetscMemcpy(x+j*nx,x+(j+1)*nx,ptsize);CHKERRQ(ierr);
	  ierr= PetscMemcpy(x+(j+1)*nx,xj,ptsize);CHKERRQ(ierr);
	}
      }
    }
    printf("fk = \n   ");
    for (i = 0; i < nx+1; ++i) {
      printf("%g  ",fk[i]);
    }
    printf("\n");
    printf("xk = \n   ");
    for (i = 0; i < nx+1; ++i) {
      for (j = 0; j < nx; ++j) {
	printf("% 4.3f  ",x[i*nx+j]);
      }
      printf("\n   ");
    }
    printf("\n");

    /* reflection */
    ierr= PetscMemzero(xb,ptsize);
    for (j = 0; j < nx; ++j) {
      for (i = 0; i < nx; ++i) {
	xb[j] += x[i*nx + j];
      }
      xb[j]= xb[j]/nx;
      xr[j]= xb[j] + r*(xb[j]-x[nx*nx+j]); 
    }
    fr= f(xr,ctx);
    if (fr>=fk[0] && fr<fk[nx-1]) { 
      ierr= PetscMemcpy(x+nx*nx,xr,ptsize);CHKERRQ(ierr);
      fk[nx]= fr;
    }

    /* expansion */
    else if (fr<fk[0]) {
      for (j = 0; j < nx; ++j) {
	xe[j]= xb[j] + c*(xr[j]-xb[j]);
      }
      fe= f(xe,ctx);
      if (fe<fr) {
	ierr= PetscMemcpy(x+nx*nx,xe,ptsize);CHKERRQ(ierr);
	fk[nx]= fe;
      }
      else{
	ierr= PetscMemcpy(x+nx*nx,xr,ptsize);CHKERRQ(ierr);
	fk[nx]= fr;
      }
    }

    /* either contraction or shrinking */
    else {
      for (j = 0; j < nx; ++j) {
	xi[j]= xb[j] - g*(xr[j]-xb[j]);
	xo[j]= xb[j] + g*(xr[j]-xb[j]);
      }
      fi= f(xi,ctx); 
      fo= f(xo,ctx);
      if (fr<fk[nx] && fo<=fr) {  /* outside contraction */
	ierr= PetscMemcpy(x+nx*nx,xo,ptsize);CHKERRQ(ierr);
	fk[nx]= fo;
      }
      else if (fr>=fk[nx] && fi<fk[nx]) { /* inside contraction */
	ierr= PetscMemcpy(x+nx*nx,xi,ptsize);CHKERRQ(ierr);
	fk[nx]= fi;
      }
      else {  /* shrinking */
	for (i = 1; i < nx+1; ++i) {
	  for (j = 0; j < nx; ++j) {
	    x[i*nx+j] += s*(x[i*nx+j]-x[j]);
	  }
	  fk[i]= f(x+i*nx,ctx);
	}
      }
    }

    /* compute stopping indicators */
    disx= 0;  disf= 0;
    for (i = 0; i < nx; ++i) {
      for (j = 0; j < nx; ++j) {
	disx += fabs(x[i*nx+j]-x[(i+1)*nx+j]);
      }
      disf += fabs(fk[i]-fk[i+1]);
    }
    printf("disx= %f, disf= %f\n\n",disx,disf);

    k++;
  }
    
  /* find minimum from fk */
  *fopt= fk[nx];
  ierr= PetscMemcpy(xopt,x+nx*nx,ptsize);CHKERRQ(ierr);
  for (j = 0; j < nx; ++j) {
    if (*fopt>fk[j]) {
      *fopt= fk[j];
      ierr= PetscMemcpy(xopt,x+j*nx,ptsize);CHKERRQ(ierr);
    }
  }
  printf("fopt = % f\n", *fopt);
  printf("xopt = ");
  for (j = 0; j < nx; ++j) {
    printf("% f  ",xopt[j]);
  }
  printf("\n  ");
  printf("iterations: %d\n",k);

  ierr= PetscFree(x);CHKERRQ(ierr);
  ierr= PetscFree(xj);CHKERRQ(ierr);
  ierr= PetscFree(xb);CHKERRQ(ierr);
  ierr= PetscFree(xr);CHKERRQ(ierr);
  ierr= PetscFree(xe);CHKERRQ(ierr);
  ierr= PetscFree(xi);CHKERRQ(ierr);
  ierr= PetscFree(xo);CHKERRQ(ierr);
  ierr= PetscFree(fk);CHKERRQ(ierr);
  PetscFunctionReturn(k);
}


