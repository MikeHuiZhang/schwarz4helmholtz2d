# Parallel Optimised Schwarz Methods for Helmholtz Problems #

Parallel implementation, based on PETSC, of optimised Schwarz methods with overlap for the Helmholtz equation in a 2-D rectangle filled with heterogeneous media.

### This is a research program ###

* for testing optimised Schwarz methods with different transmission conditions,
* for searching optimal parameters in transmission conditions,
* for testing coarse problems for accelerating convergence,
* also for testing parallel direct solvers through interfaces of PETSC.

### Dependencies ###

* MPI
* [PETSC](http://www.mcs.anl.gov/petsc/)
* Lapack with C interface Lapacke

### Compile and run ###

* make
* run with "./helm2d -h" to see running options