#include <petscmat.h>

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "LoadDirichlet" set Dirichlet values in f. Since it *sets* values,
   the user would call this after all *ADD_VALUES* and assemblybegin/end.

   INPUT
   ni:   number of *nodes*
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "LoadDirichlet"
PetscErrorCode LoadDirichlet(Vec f, PetscScalar (*g)(PetscReal), PetscReal sl, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni)
{
  PetscInt *rows, i;
  PetscScalar *vals;
  PetscErrorCode ierr;

  ierr= PetscMalloc(ni*sizeof(PetscInt),&rows);CHKERRQ(ierr);
  ierr= PetscMalloc(ni*sizeof(PetscScalar),&vals);CHKERRQ(ierr); 
  for (i=0; i<ni; ++i) {
    rows[i]= startidx + i*neighbor_disp;  vals[i]= g(sl+i*h);
  }
  ierr= VecSetValuesLocal(f,ni,rows,vals,INSERT_VALUES);CHKERRQ(ierr);

  ierr= PetscFree(rows);CHKERRQ(ierr);
  ierr= PetscFree(vals);CHKERRQ(ierr);
  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "LoadPointSource" ADD_VALUEs to f, using true Delta sources.
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "LoadPointSource"
PetscErrorCode LoadPointSource(Vec f, PetscInt nsrc, PetscReal *xsrc, PetscReal *ysrc, PetscReal xl, PetscReal yl, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp)
{
  PetscInt i, ix, iy, Ii[4];
  PetscReal x, y;
  PetscScalar vals[4];
  PetscErrorCode ierr;

  for (i = 0; i < nsrc; ++i) {
    ix= floor((xsrc[i]-xl)/hx); iy= floor((ysrc[i]-yl)/hy);
    if (ix>=0 && ix<nxp && iy>=0 && iy<nyp) {
      /* values of basis functions at the source location */
      x= (xsrc[i]-xl-ix*hx)/hx; y= (ysrc[i]-yl-iy*hy)/hy;
      vals[0]= (1-x)*(1-y);
      vals[1]= x*(1-y);
      vals[2]= (1-x)*y;
      vals[3]= x*y;
      Ii[0]= ix*(nyp+1)+iy; Ii[1]= Ii[0]+nyp+1; 
      Ii[2]= Ii[0]+1;       Ii[3]= Ii[0]+1+nyp+1; 
      ierr= VecSetValuesLocal(f,4,Ii,vals,ADD_VALUES);CHKERRQ(ierr);
    }
  }

  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
  "GaussPoints" returns Gauss points in (0,1)
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "GaussPoints"
PetscErrorCode GaussPoints(PetscInt n, PetscReal *x, PetscReal *w)
{
  switch (n) {
  case 1:
    x[0]= 0.5; w[0]= 1;
    break;
  case 2:
    x[0]= 0.788675134594813; x[1]= 0.211324865405187; w[0]= 0.5; w[1]= 0.5;
    break;
  case 3:
    x[0]= 0.887298334620742; x[1]= 0.5; x[2]= 0.112701665379258; 
    w[0]= 0.277777777777777; w[1]= 0.444444444444444; w[2]= w[0];
    break;
  case 4: default:
    x[0]= 0.930568155797026; x[1]= 0.669990521792428; x[2]= 0.330009478207572;
    x[3]= 0.069431844202974; 
    w[0]= 0.173927422568727; w[1]= 0.326072577431273; w[2]= w[1]; w[3]= w[0];
    break;
  }
  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "LoadABC" add the load from the absorbing/Neumann boundary values on 
   one side.
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "LoadABC"
PetscErrorCode LoadABC(Vec f, PetscScalar (*g)(PetscReal), PetscReal sl, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni, PetscInt nquad)
{
  PetscInt i, j, Ii[2];
  PetscReal s, *quad_points, *quad_weights;
  PetscScalar valf, vals[2];
  PetscErrorCode ierr;

  ierr= PetscMalloc(nquad*sizeof(PetscReal),&quad_points);CHKERRQ(ierr);
  ierr= PetscMalloc(nquad*sizeof(PetscReal),&quad_weights);CHKERRQ(ierr);
  GaussPoints(nquad,quad_points,quad_weights);
  for (i = 0; i < ni; ++i) {
	s= sl + i*h;
	vals[0]= 0; vals[1]= 0;
	for (j = 0; j < nquad; ++j) {
	  valf= g(s+quad_points[j]*h)*quad_weights[j];
	  vals[0]+= valf*(1-quad_points[j]);
	  vals[1]+= valf*quad_points[j];
	}
	vals[0]*= h; vals[1]*= h;
	Ii[0]= startidx + i*neighbor_disp; Ii[1]= Ii[0] + neighbor_disp;
	ierr= VecSetValuesLocal(f,2,Ii,vals,ADD_VALUES);CHKERRQ(ierr);
  }
  
  PetscFree(quad_points); PetscFree(quad_weights);
  return 0;
}



/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "LoadSmoothSource" adds the load (i.e. the rhs)  from the smooth 
   source function. 

   INPUT
   xl, yl: coordinates of the left corner of the rectangle owned by this
           processor

 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ ""
PetscErrorCode LoadSmoothSource(Vec f, PetscScalar (*S)(PetscReal,PetscReal), PetscReal xl, PetscReal yl, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp, PetscInt nquad)
{
  PetscInt Ii[4], ix, iy, i, j;
  PetscErrorCode ierr;
  PetscReal *quad_points, *quad_weights, x, y;
  PetscScalar vals[4], valf;

  ierr= PetscMalloc(nquad*sizeof(PetscReal),&quad_points);CHKERRQ(ierr);
  ierr= PetscMalloc(nquad*sizeof(PetscReal),&quad_weights);CHKERRQ(ierr);
  GaussPoints(nquad,quad_points,quad_weights);
  PetscPrintf(PETSC_COMM_WORLD,"number of Gauss quadrature points in 1d: %i\n",nquad);
  for (ix = 0; ix < nxp; ++ix) { 
    x= xl + ix*hx; /* x coordinate of left corner on one element */ 
    for (iy = 0; iy < nyp; ++iy) {
      y= yl + iy*hy;
      /* nodes on one element, numbering c.f. Ks in AssemblyHelmholtz()*/
      vals[0]= 0; vals[1]= 0; vals[2]= 0; vals[3]= 0;
      /* weighting basis function times source function at quadrature points */
      for (i = 0; i < nquad; ++i) {
	for (j = 0; j < nquad; ++j) {
	  valf= quad_weights[i]*quad_weights[j]*S(x+quad_points[i]*hx,y+quad_points[j]*hy);
	  vals[0]+= valf*(1-quad_points[i])*(1-quad_points[j]);
	  vals[1]+= valf*quad_points[i]*(1-quad_points[j]);
	  vals[2]+= valf*(1-quad_points[i])*quad_points[j];
	  vals[3]+= valf*quad_points[i]*quad_points[j];
	}
      }
      vals[0]*= hx*hy; vals[1]*= hx*hy; vals[2]*= hx*hy; vals[3]*= hx*hy;
      Ii[0]= ix*(nyp+1)+iy; Ii[1]= Ii[0]+nyp+1; 
      Ii[2]= Ii[0]+1;       Ii[3]= Ii[0]+1+nyp+1; 
      ierr= VecSetValuesLocal(f,4,Ii,vals,ADD_VALUES);CHKERRQ(ierr);
    }
  }

  PetscFree(quad_points); PetscFree(quad_weights);
  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "ApplyNeumann" applies Neumann b.c. on one side by re-assembly in the
   volumen elements that are ajacent to the side, only the interactions
   along the boundary nodes are modified by firt INSERT_VALUES and then
   ADD_VALUES.

   Collective

   For the processors not along the side, we can pass in ni= 0.

   INPUT
   A:      the matrix ready for INSERT_VALUES;
	   after this call, A is in ADD_VALUES mode and may need to be 
	   MatAssemblyBegin()/End()
   hs, hn: mesh sizes along tangential and normal directions
   ni:     number of *elements* along the side
= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "ApplyNeumann"
PetscErrorCode ApplyNeumann(Mat A, PetscReal k2, PetscReal hs, PetscReal hn, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni)
{
  PetscScalar Ks[2], Ms[2];    /* element stiffness and mass stencils along the side (tangential) */
  PetscScalar vals[4];         /* values of element matrix representing interactions along the side */
  PetscInt    i, Ii[2];  
  PetscErrorCode ierr;

  PetscFunctionBegin;

  /* DEBUG: */
  /* printf("before SetValues \n"); */
  /* ierr= MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatView(A,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */

  /* zeroing the corresponding entries of A */
  vals[0]= 0.0; vals[1]= 0.0; vals[2]= 0.0; vals[3]= 0.0;
  for (i = 0; i < ni; ++i) 	
  {
    Ii[0]= startidx + i*neighbor_disp;  Ii[1]= Ii[0] + neighbor_disp;
    ierr= MatSetValuesLocal(A,2,Ii,2,Ii,vals,INSERT_VALUES);CHKERRQ(ierr);
  }
  MatAssemblyBegin(A,MAT_FLUSH_ASSEMBLY);
  MatAssemblyEnd(A,MAT_FLUSH_ASSEMBLY);
  
  /* DEBUG: */
  /* printf("after SetValues\n"); */
  /* ierr= MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatView(A,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */

  /*
     Set array values for element stiffness stencil and mass stencil, 
     connection to nodes 2,3 are ignored 
          2  3
          | /
          0--1  
  */
  Ks[0]= (hs/hn + hn/hs)/3.0;  Ks[1]= (0.5*hs/hn-hn/hs)/3.0; 
  Ms[0]= hs*hn/9.0; Ms[1]= hs*hn/18.0; 
  
  /* Set array values for element matrix representing boundary nodes 
     interactions of Helmholtz operator */
  vals[0]= Ks[0] - k2*Ms[0]; vals[1]= Ks[1] - k2*Ms[1];
  vals[2]= vals[1];          vals[3]= vals[0];
 
  /* add vals to the corresponding rows/cols of A */
  for (i = 0; i < ni; ++i)
  {
    Ii[0]= startidx + i*neighbor_disp;  Ii[1]= Ii[0] + neighbor_disp;
    ierr= MatSetValuesLocal(A,2,Ii,2,Ii,vals,ADD_VALUES);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "ApplyDirichlet" applies Dirichlet boundary conditions by zeroing rows
   and putting one's on diagonal entries. Local (this processor's) numerbing
   is used. *All* processors of A should call this function because 
   MatZeroRowsLocal needs and for the same reason A should be MatAssembly 'ed
   before calling to this function. Set ni to zero if no Dirichlet nodes
   on this processor.

   INPUT
   A:   the matrix that has been assembled
   ni:  number of *nodes*
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "ApplyDirichlet"
PetscErrorCode ApplyDirichlet(Mat A, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni)
{
  PetscInt *rows=0, i;
  PetscErrorCode ierr;

  ierr= PetscMalloc((ni+1)*sizeof(PetscInt),&rows);CHKERRQ(ierr);
  for (i = 0; i < ni; ++i) {
    rows[i]= startidx + i*neighbor_disp;
  }
  ierr= MatZeroRowsLocal(A,ni,rows,1.0,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscFree(rows);CHKERRQ(ierr);
  
  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "ApplyABCNode" applies absorbing conditions for only one node's *self*-
   interaction.

   INPUT
     node: the node we want to apply ABC
    
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "ApplyABCNode"
PetscErrorCode ApplyABCNode(Mat A, PetscScalar *abc_coef, PetscReal h, PetscInt node)
{
  PetscScalar val;
  PetscErrorCode ierr;
  
  val= abc_coef[0]*h/3.0 + abc_coef[1]/h;
  ierr= MatSetValuesLocal(A,1,&node,1,&node,&val,ADD_VALUES);CHKERRQ(ierr);

  return 0;
}
    
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "ApplyABC" applies Robin-like boundary conditions for one side. 
    It needs only be executed by processors along this side. 
   INPUT
     A:  the matrix that allows "MatSetValuesLocal" method and corresponds
         to Neumann b.c. along the input side
     abc_coef: abc of u is u_n + abc_coef[0] u - abc_coef[1] u_{ss}
     h:  mesh size along the side
     startidx: start index in A under local (this processor's) numbering
     neighbor_disp: the offset between two neighboring nodes on the side,
                    counted under local numbering
     ni: number of interface *elements*
   OUTPUT
     A: the matrix that allows MatSetValuesLocal with ADD_VALUES
 = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "ApplyABC"
PetscErrorCode ApplyABC(Mat A, PetscScalar *abc_coef, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni)
{
  PetscScalar Ksi[2], Msi[2], vals[4];
  PetscInt i, Ii[2], Ij[2];
  PetscErrorCode ierr;

  Ksi[0]= 1.0/h; Ksi[1]= -1.0/h;
  Msi[0]= h/3.0; Msi[1]=  h/6.0;
  vals[0]= abc_coef[0]*Msi[0] + abc_coef[1]*Ksi[0];
  vals[1]= abc_coef[0]*Msi[1] + abc_coef[1]*Ksi[1];
  vals[2]= vals[1];  vals[3]= vals[0];
  for (i = 0; i < ni; ++i) {
    Ii[0]= startidx + i*neighbor_disp; Ii[1]= Ii[0]+neighbor_disp;
    Ij[0]= Ii[0];  Ij[1]= Ii[1];
    ierr= MatSetValuesLocal(A,2,Ii,2,Ij,vals,ADD_VALUES);CHKERRQ(ierr);
  }
  return 0;
}

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   "AssemblyHelmholtz" assembles the matrix corresponding to Q1-FEM 
   discretization of the Helmholtz operator with Neumann b.c. 
   INPUT
     A:  the matrix that allows "MatSetValuesLocal" method
     k2: square of the wavenumber
     hx, hy: mesh sizes
     nxp, nyp: number of elements
   OUTPUT
     A:  we add values to A by "MatSetValuesLocal", the user is responsible to
         MatAssemblyBegin()/MatAssemblyEnd()  when necessary before and/or 
	 after calling this function
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
#undef __FUNCT__
#define __FUNCT__ "AssemblyHelmholtz"
PetscErrorCode AssemblyHelmholtz(Mat A, PetscReal k2, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp)
{
  PetscScalar Ks[4], Ms[4]; /* element stiffness and mass stencils */
  PetscScalar vals[16];        /* values of element matrix */
  PetscInt    count,ix,iy, Ii[16], Ij[16];  
  PetscErrorCode ierr;

  /*
     Set array values for element stiffness stencil and mass stencil
          2  3
          | /
          0--1  
     and edge element stiffness and mass
          0--1
  */
  Ks[0]= (hx/hy + hy/hx)/3.0;  Ks[1]= (0.5*hx/hy-hy/hx)/3.0; 
  Ks[2]= (0.5*hy/hx-hx/hy)/3.0; Ks[3]= -0.5*(hx/hy + hy/hx)/3.0; 
  Ms[0]= hx*hy/9.0; Ms[1]= hx*hy/18.0; Ms[2]= Ms[1]; Ms[3]= hx*hy/36.0; 
  /* PetscPrintf(PETSC_COMM_WORLD,"Ks=[%f %f %f %f]\n",Ks[0],Ks[1],Ks[2],Ks[3]); */
  /* PetscPrintf (PETSC_COMM_WORLD,"Ms=[%f %f %f %f]\n",Ms[0],Ms[1],Ms[2],Ms[3]);  */
  
  /* Set array values for element matrix of Helmholtz operator */
  for (count = 0; count < 4; ++count)  /* see definition of Ks, Ms */
  {
      vals[count*4+count]= Ks[0] - k2*Ms[0]; 
      vals[count*4+count+(count%2?-1:1)]= Ks[1] - k2*Ms[1];
      vals[count*4+count+(count<2?2:-2)]= Ks[2] - k2*Ms[2]; 
      vals[count*4+count+(count%2?-1:1)+(count<2?2:-2)]= Ks[3] - k2*Ms[3];
  }
  /* * begin test the element matrix * */
  /* if (rank==0) { */
  /* ierr= MatCreateSeqDense(PETSC_COMM_SELF,4,4,PETSC_NULL,&vals_mat);CHKERRQ(ierr); */
  /*   for (count = 0; count < 4; ++count) */
  /*   { */
  /* 	Ii[count]= count; Ij[count]= count; */
  /*   } */
  /*   ierr= MatSetValues(vals_mat,4,Ii,4,Ij,vals,INSERT_VALUES);CHKERRQ(ierr); */
  /*   ierr= MatAssemblyBegin(vals_mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /*   ierr= MatAssemblyEnd(vals_mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /*   ierr= MatView(vals_mat, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /*   MatDestroy(&vals_mat); */
  /* } */
  /* * end test of the element matrix * */ 

  /* add vals to the corresponding rows/cols of A */
  for (ix = 0; ix < nxp; ++ix)
  {
      for (iy = 0; iy < nyp; ++iy)
      {
	  Ii[0]= ix*(nyp+1)+iy;   /* down-left corner */
	  Ii[1]= Ii[0]+nyp+1;     /* down-right corner */
	  Ii[2]= Ii[0]+1;         /* up-left corner */
	  Ii[3]= Ii[0]+1+nyp+1;   /* up-right corner */
	  Ij[0]= Ii[0]; Ij[1]= Ii[1]; Ij[2]= Ii[2]; Ij[3]= Ii[3];
	  ierr= MatSetValuesLocal(A,4,Ii,4,Ij,vals,ADD_VALUES);CHKERRQ(ierr);
      }
  }
  
  /* DEBUG: */
  /* ierr= MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY); */
  /* ierr= MatView(A, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */

  return 0;
}
