#ifndef _HELM2D_ASSEMBLY
#define _HELM2D_ASSEMBLY
#include <petscmat.h>

PetscErrorCode LoadDirichlet(Vec f, PetscScalar (*g)(PetscReal), PetscReal sl, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni);

PetscErrorCode LoadPointSource(Vec f, PetscInt nsrc, PetscReal *xsrc, PetscReal *ysrc, PetscReal xl, PetscReal yl, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp);

PetscErrorCode LoadABC(Vec f, PetscScalar (*g)(PetscReal), PetscReal sl, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni, PetscInt nquad);

PetscErrorCode LoadSmoothSource(Vec f, PetscScalar (*S)(PetscReal,PetscReal), PetscReal xl, PetscReal yl, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp, PetscInt nquad);

PetscErrorCode ApplyNeumann(Mat A, PetscReal k2, PetscReal hs, PetscReal hn, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni);

PetscErrorCode ApplyDirichlet(Mat A, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni);

PetscErrorCode ApplyABCNode(Mat A, PetscScalar *abc_coef, PetscReal h, PetscInt node);

PetscErrorCode ApplyABC(Mat A, PetscScalar *abc_coef, PetscReal h, PetscInt startidx, PetscInt neighbor_disp, PetscInt ni);

PetscErrorCode AssemblyHelmholtz(Mat A, PetscReal k2, PetscReal hx, PetscReal hy, PetscInt nxp, PetscInt nyp);

#endif
