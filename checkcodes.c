/* /\* view indices *\/ */
  /* ierr= ISCreateGeneral(PETSC_COMM_SELF,dimp,MyNatureIndices,PETSC_COPY_VALUES,&MyNatureIS);CHKERRQ(ierr); */
  /* printf("[%i] natural indices \n",rank); */
  /* ierr= ISView(MyNatureIS,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* /\* test 1 for our ao *\/ */
  /* ierr= PetscMalloc(dimp*sizeof(PetscInt),&indices);CHKERRQ(ierr);  */
  /* for (count=0; count<dimp; ++count) */
  /* { */
  /*   indices[count]= MyNatureIndices[count]; */
  /* } */
  /* ierr= AOApplicationToPetsc(petsc_nature_ao,dimp,indices);CHKERRQ(ierr); */
  /* ierr= ISDestroy(&MyNatureIS);CHKERRQ(ierr); */
  /* ierr= ISCreateGeneral(PETSC_COMM_SELF,dimp,indices,PETSC_COPY_VALUES,&MyNatureIS);CHKERRQ(ierr); */
  /* printf("[%i] mapping results, petsc indices\n",rank); */
  /* ierr= ISView(MyNatureIS,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* /\* test 2 for our ao *\/ */
  /* ierr= AOPetscToApplication(petsc_nature_ao,dimp,indices);CHKERRQ(ierr); */
  /* ierr= ISDestroy(&MyNatureIS);CHKERRQ(ierr); */
  /* ierr= ISCreateGeneral(PETSC_COMM_SELF,dimp,indices,PETSC_COPY_VALUES,&MyNatureIS);CHKERRQ(ierr); */
  /* printf("[%i] second mapping results, natural indices\n",rank); */
  /* ierr= ISView(MyNatureIS,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* /\* test 3 for our ao *\/ */
  /* ierr= PetscFree(indices);CHKERRQ(ierr); */
  /* ierr= PetscMalloc(5*sizeof(PetscInt),&indices); */
  /* indices[0]= 1; indices[1]= 19; indices[2]= 5; indices[3]= 6; indices[4]= 10;  */
  /* ierr= AOApplicationToPetsc(petsc_nature_ao,5,indices);CHKERRQ(ierr); */
  /* ierr= ISDestroy(&MyNatureIS);CHKERRQ(ierr); */
  /* ierr= ISCreateGeneral(PETSC_COMM_SELF,5,indices,PETSC_COPY_VALUES,&MyNatureIS);CHKERRQ(ierr); */
  /* printf("[%i] third mapping results, petsc indices\n",rank); */
  /* ierr= ISView(MyNatureIS,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* PetscFree(indices); */
  
/* * check A  * */
  /* ierr= MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
  /* ierr= MatView(A, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */
  /* ierr= MatTranspose(A,MAT_INITIAL_MATRIX,&A1);CHKERRQ(ierr); */
  /* ierr= MatAXPY(A1,-1.0,A,SAME_NONZERO_PATTERN);CHKERRQ(ierr); */
  /* PetscPrintf(PETSC_COMM_WORLD,"A-A.'\n"); */
  /* ierr= MatView(A1, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */
