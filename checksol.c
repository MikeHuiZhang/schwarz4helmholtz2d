#include <petscksp.h>


/* ----------------------------------------------------------------------- 
   Check solution and output results
   
   ----------------------------------------------------------------------- */
PetscErrorCode CHKErrNorm(Vec uerr, const Vec u, const Vec ustar, PetscReal hx, PetscReal hy)
{
  
  PetscReal norm;
  PetscErrorCode ierr;

  VecCopy(u,uerr);
  ierr = VecAXPY(uerr,-1.0,ustar);CHKERRQ(ierr);
  ierr = VecNorm(uerr,NORM_INFINITY,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Maximum %g,  ",norm);CHKERRQ(ierr);
  ierr = VecNorm(uerr,NORM_2,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"l^2*sqrt(hx*hy) %g\n",norm*sqrt(hx*hy));CHKERRQ(ierr);

  return 0;
}

/* ----------------------------------------------------------------------- 
   This is a serial file I/O function.  Call this function with one and only
   one process!
   ----------------------------------------------------------------------- */
PetscErrorCode WriteResHistory(const KSP kspdd, const char *filename)
{
  PetscReal *resnorm; 
  PetscInt numres, i;
  PetscErrorCode ierr;  
  FILE *file;

  ierr= KSPGetResidualHistory(kspdd,&resnorm,&numres);CHKERRQ(ierr);
  file= fopen(filename,"w");
  if (!file) {
    printf("Unable to open file!\n");
  return 1;
  }
  fprintf(file,"resnorm= [%g\n",resnorm[0]);
  for (i = 1; i < numres-1; ++i) {
    fprintf(file,"          %g\n",resnorm[i]);
  }
  fprintf(file,"          %g];\n",resnorm[numres-1]);
  fprintf(file,"semilogy(resnorm,'*-'); set(gca,'FontSize',12);\n");
  fclose(file);

  return 0;
}


/*
     Output data into files
  */
  /* PetscViewer    matlab_viewer; */
  /* ierr= PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_MATLAB);CHKERRQ(ierr); */
  /* VecView(u,PETSC_VIEWER_STDOUT_WORLD); */
  /* VecView(u,PETSC_VIEWER_STDOUT_WORLD); */
  /* need -with-matlab when compiling petsc */
  /* ierr= PetscViewerSocketOpen(PETSC_COMM_WORLD,PETSC_NULL,PETSC_DEFAULT,&matlab_viewer);CHKERRQ(ierr); */
  /* ierr= VecView(u,matlab_viewer);CHKERRQ(ierr); */
  /* ierr= VecView(ustar,matlab_viewer);CHKERRQ(ierr); */
