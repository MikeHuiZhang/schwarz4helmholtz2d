#include "helmbvp.h"
#include "mypart.h"
#include "assembly.h"
#include <petscao.h>


/*
  INPUT
    helm_bvp    extern global 
    part

  
  OUTPUT 
    A                  Mat
    ao                 AO
    ltog               ISLocalToGlobalMapping

*/

#undef __FUNCT__
#define __FUNCT__ "ComputeA"
PetscErrorCode ComputeA(Mat *A, AO *ao, ISLocalToGlobalMapping *ltog, MyPartition *part)
{
  PetscInt         Istart, Iend, ix, iy, count, Ii, Ij;
  IS               PetscIS;
  const PetscInt  *PetscIndices;
  PetscInt        *indices;
  PetscErrorCode   ierr;
  PetscReal        hx, hy;
  PetscScalar      val;
  

  /*
     Create parallel matrix, specifying its local and global dimensions.
     When using MatCreate(), the matrix format can be specified at runtime.
     Also, the parallel partitioning of the matrix is determined by PETSC
     at runtime.
  */
  ierr= MatCreateAIJ(PETSC_COMM_WORLD,part->dimp,part->dimp,part->dim,part->dim,9,PETSC_NULL,5,PETSC_NULL,A);CHKERRQ(ierr);
  ierr= MatSetOption(*A,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE); /* we need this for modifying submatrices */
  ierr= MatSetFromOptions(*A);CHKERRQ(ierr);
  
  /*
     Currently, all PETSC parallel matrix formats are partitioned by
     contiguous chunks of rows across the processors. Determine which
     rows of the matrix are locally owned, Istart ~ Iend-1
  */
  ierr= MatGetOwnershipRange(*A,&Istart,&Iend);CHKERRQ(ierr);

  /*
     Create AO with natural global ordering mapped to PETSC's ordering: for Mat A
  */
  ierr= ISCreateStride(PETSC_COMM_SELF,part->dimp,Istart,1,&PetscIS);CHKERRQ(ierr);
  ierr= ISGetIndices(PetscIS,&PetscIndices);CHKERRQ(ierr);
  ierr= PetscMalloc(part->dimp*sizeof(PetscInt),&indices);CHKERRQ(ierr);
  count= 0;
  for (ix=part->elem_startx; ix<part->elem_startx+part->dimxp; ++ix)
  {
    for (iy=part->elem_starty; iy<part->elem_starty+part->dimyp; ++iy)
    {
      indices[count++]= ix*(part->ny+1) + iy;
    }
  }
  ierr= AOCreateBasic(PETSC_COMM_WORLD,part->dimp,indices,PetscIndices,ao);CHKERRQ(ierr);
  ierr= ISRestoreIndices(PetscIS,&PetscIndices);CHKERRQ(ierr);
  ISDestroy(&PetscIS);  PetscFree(indices);
  
  /* DEBUG: */
  /* ierr= AOView(*ao,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */

  /*
     Set LocalToGlobal mapping.
  */
  ierr= PetscMalloc((part->nxp+1)*(part->nyp+1)*sizeof(PetscInt),&indices);
  count= 0;
  for (ix = part->elem_startx; ix < part->elem_endx+1; ++ix)
  {
    for (iy = part->elem_starty; iy < part->elem_endy+1; ++iy)
    {
      indices[count++]= ix*(part->ny+1)+iy;
    }
  }
  ierr= AOApplicationToPetsc(*ao,(part->nxp+1)*(part->nyp+1),indices);
  ierr= ISLocalToGlobalMappingCreate(PETSC_COMM_WORLD,1,(part->nxp+1)*(part->nyp+1),indices,PETSC_COPY_VALUES,ltog);CHKERRQ(ierr);
  ierr= PetscFree(indices);CHKERRQ(ierr);
  ierr= MatSetLocalToGlobalMapping(*A,*ltog,*ltog);CHKERRQ(ierr);
  
  /*
     Set matrix elements in parallel.
  */
  hx= (helm_bvp.xr-helm_bvp.xl)/part->nx; 
  hy= (helm_bvp.yr-helm_bvp.yl)/part->ny;
  AssemblyHelmholtz(*A, helm_bvp.k2, hx, hy, part->nxp, part->nyp);

  /* absorbing boundary */
  if (helm_bvp.bctype[0]==1 && part->rx==0) { /* left side */
    ApplyABC(*A, helm_bvp.abc_coef, hy, 0, 1, part->nyp);
  }
  if (helm_bvp.bctype[1]==1 && part->rx==part->px-1) { /* right side */
    ApplyABC(*A, helm_bvp.abc_coef, hy, part->nxp*(part->nyp+1), 1, part->nyp);
  }
  if (helm_bvp.bctype[2]==1 && part->ry==0) { /* bottom side */
    ApplyABC(*A, helm_bvp.abc_coef, hx, 0, part->nyp+1, part->nxp);
  }
  if (helm_bvp.bctype[3]==1 && part->ry==part->py-1) { /* top side */
    ApplyABC(*A, helm_bvp.abc_coef, hx, part->nyp, part->nyp+1, part->nxp);
  }
  
  /* corner condition arising from second-order term of the ABC */
  if (helm_bvp.abc_order==2) {
    if (helm_bvp.bctype[0]==1 && helm_bvp.bctype[2]==1 && part->rx==0 && part->ry==0) { /* corner (0,0) */
      val= helm_bvp.abc_coef[3];
      Ii= 0;  Ij= Ii;
      ierr= MatSetValuesLocal(*A,1,&Ii,1,&Ij,&val,ADD_VALUES);CHKERRQ(ierr);
    }
    if (helm_bvp.bctype[0]==1 && helm_bvp.bctype[3]==1 && part->rx==0 && part->ry==part->py-1) { /* corner (0,ny) */
      val= helm_bvp.abc_coef[3];
      Ii= part->nyp;  Ij= Ii;
      ierr= MatSetValuesLocal(*A,1,&Ii,1,&Ij,&val,ADD_VALUES);CHKERRQ(ierr);
    }
    if (helm_bvp.bctype[1]==1 && helm_bvp.bctype[2]==1 && part->rx==part->px-1 && part->ry==0) { /* corner (nx,0) */
      val= helm_bvp.abc_coef[3];
      Ii= part->nxp*(part->nyp+1);  Ij= Ii;
      ierr= MatSetValuesLocal(*A,1,&Ii,1,&Ij,&val,ADD_VALUES);CHKERRQ(ierr);
    }
    if (helm_bvp.bctype[1]==1 && helm_bvp.bctype[3]==1 && part->rx==part->px-1 && part->ry==part->py-1) { /* corner (nx,ny) */
      val= helm_bvp.abc_coef[3];
      Ii= (part->nxp+1)*(part->nyp+1)-1;  Ij= Ii;
      ierr= MatSetValuesLocal(*A,1,&Ii,1,&Ij,&val,ADD_VALUES);CHKERRQ(ierr);
    }
  }
  ierr= MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr= MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  /* Dirichlet Boundary */
  if (helm_bvp.bctype[0]==2) { /* left side */
    ApplyDirichlet(*A, 0, 1, (part->rx==0)?part->nyp+1:0);
  }
  if (helm_bvp.bctype[1]==2) { /* right side */
    ApplyDirichlet(*A, part->nxp*(part->nyp+1), 1, (part->rx==part->px-1)?part->nyp+1:0);
  }
  if (helm_bvp.bctype[2]==2) { /* bottom side */
    ApplyDirichlet(*A, 0, part->nyp+1, (part->ry==0)?part->nxp+1:0);
  }
  if (helm_bvp.bctype[3]==2) { /* top side */
    ApplyDirichlet(*A, part->nyp, part->nyp+1, (part->ry==part->py-1)?part->nxp+1:0);
  }

  /* Set symmetric for A */
  ierr= MatSetOption(*A,MAT_SYMMETRIC,PETSC_TRUE);CHKERRQ(ierr);

  return 0;
}
