#include "mypart.h"
#include "assembly.h"
#include "solutions.h"
#include <time.h>

/*
  Set exact solution; then compute right-hand-side vector.

  INPUT
    helm_bvp    extern global 
    A          
    part
    ltog
    nquad
    initrand
  
  OUTPUT 
    f                  Vec          the right hand side
    u                  Vec          the solution vector (all zeros or random)
    ustar              Vec          the exact solution (for model_type 0~2)
    

*/

#undef __FUNCT__
#define __FUNCT__ "Computef"
PetscErrorCode Computef(Vec *f, Vec *u, Vec *ustar, Mat A, MyPartition *part, ISLocalToGlobalMapping ltog, PetscInt nquad, PetscBool initrand)
{
  PetscErrorCode ierr;
  PetscRandom    rctx;
  PetscMPIInt    rank;
  PetscInt       i, count, ix, iy, Ii;
  PetscReal      hx, hy, x, y;
  PetscScalar   *ua, val;
  

  /*
     Create parallel vectors.
      - When using VecCreate(), VecSetSizes() and VecSetFromOptions(),
        we specify the vector's both global and local dimension; the
        parallel ordering is determined at runtime.
      - Note: We form 1 vector from scratch and then duplicate as needed.
      - Confirmed from petsc's developers, that Vec and Mat are partitioned
        and ordered between processors in the same way contiguously from rank 0
  */
  ierr= VecCreate(PETSC_COMM_WORLD,f);CHKERRQ(ierr);
  ierr= VecSetSizes(*f,part->dimp,part->dim);CHKERRQ(ierr);
  ierr= VecSetFromOptions(*f);CHKERRQ(ierr);
  ierr= VecSetLocalToGlobalMapping(*f,ltog);CHKERRQ(ierr);
  ierr = VecDuplicate(*f,u);CHKERRQ(ierr);
  ierr = VecDuplicate(*f,ustar);CHKERRQ(ierr);

  /*
     mesh sizes
  */
  hx= (helm_bvp.xr-helm_bvp.xl)/part->nx; 
  hy= (helm_bvp.yr-helm_bvp.yl)/part->ny;

  /*
     Create random context
  */
  ierr = PetscRandomCreate(PETSC_COMM_WORLD,&rctx);CHKERRQ(ierr);
  ierr = PetscRandomSetInterval(rctx, 0.0, 1.0 + PETSC_i);
  ierr = PetscRandomSetFromOptions(rctx);CHKERRQ(ierr);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  // PetscRandomSetSeed(rctx,rank+time(NULL)); PetscRandomSeed(rctx);
  PetscRandomView(rctx,PETSC_VIEWER_STDOUT_WORLD);

  /*
     set initial guess for u
  */
  if (initrand) {
    ierr= VecSetRandom(*u,rctx);CHKERRQ(ierr);
  }


  /* use random vector as exact discrete solution */
  if (helm_bvp.model_type==0) { 
    ierr = VecSetRandom(*ustar,rctx);CHKERRQ(ierr);
    ierr = MatMult(A,*ustar,*f);CHKERRQ(ierr);
  }

  /* use random vector as rhs: ustar is not setted */
  else if (helm_bvp.model_type>2 || helm_bvp.model_type<0) { 
    ierr= VecSetRandom(*f,rctx);CHKERRQ(ierr);
  }

   /* point sources */
  else if (helm_bvp.model_type==2) {
    LoadPointSource(*f,helm_bvp.nsrc,helm_bvp.xsrc,helm_bvp.ysrc,helm_bvp.xl+part->elem_startx*hx,helm_bvp.yl+part->elem_starty*hy,hx,hy,part->nxp,part->nyp);
    ierr= VecAssemblyBegin(*f);CHKERRQ(ierr);
    ierr= VecAssemblyEnd(*f);CHKERRQ(ierr);
    /* exact solution in free space */
    ierr= VecGetArray(*ustar,&ua);CHKERRQ(ierr);
    for (i = 0; i < helm_bvp.nsrc; ++i)
    {
  	count= 0;
  	for (ix = part->elem_startx; ix < part->elem_startx + part->dimxp; ++ix)
        {
  	    for (iy = part->elem_starty; iy < part->elem_starty + part->dimyp; ++iy)
            {
  		ua[count]+= GreenFun(helm_bvp.xl+ix*hx,helm_bvp.yl+iy*hy,helm_bvp.xsrc[i],helm_bvp.ysrc[i]);
  		count++;
  	    }
  	}
    }
    ierr= VecRestoreArray(*ustar,&ua);CHKERRQ(ierr);
  }

  else if (helm_bvp.model_type==1) { /* use cont. exact sol. & cont. sources */
    /* * exact solution * */
    ierr= VecGetArray(*ustar,&ua);CHKERRQ(ierr);  count= 0;
    for (ix = part->elem_startx; ix < part->elem_startx + part->dimxp; ++ix)
    {
      for (iy = part->elem_starty; iy < part->elem_starty + part->dimyp; ++iy)
      {
        ua[count++]= ufun(helm_bvp.xl+ix*hx,helm_bvp.yl+iy*hy);
      }
    }
    ierr= VecRestoreArray(*ustar,&ua);CHKERRQ(ierr);
    //ierr = MatMult(A,ustar,f);CHKERRQ(ierr); /* if want zero discretization error, use this instead of the following */
    /* * source term * */
    LoadSmoothSource(*f,SourceFunction,helm_bvp.xl+part->elem_startx*hx,helm_bvp.yl+part->elem_starty*hy,hx,hy,part->nxp,part->nyp,nquad);
    /* * Neumann boundary values * */
    if (helm_bvp.bctype[0]==0 && part->rx==0) {  /* left side */
      LoadABC(*f,gnxl,helm_bvp.yl+part->elem_starty*hy,hy,0,1,part->nyp,nquad);
    }
    if (helm_bvp.bctype[1]==0 && part->rx==part->px-1) {  /* right side */
      LoadABC(*f,gnxr,helm_bvp.yl+part->elem_starty*hy,hy,part->nxp*(part->nyp+1),1,part->nyp,nquad);
    }
    if (helm_bvp.bctype[2]==0 && part->ry==0) {  /* bottom side */
      LoadABC(*f,gnyl,helm_bvp.xl+part->elem_startx*hx,hx,0,part->nyp+1,part->nxp,nquad);
    }
    if (helm_bvp.bctype[3]==0 && part->ry==part->py-1) {  /* top side */
      LoadABC(*f,gnyr,helm_bvp.xl+part->elem_startx*hx,hx,part->nyp,part->nyp+1,part->nxp,nquad);
    }
    /* * absorbing boundary values including corner conditions * */
    if (helm_bvp.bctype[0]==1 && part->rx==0) {  /* left side */
      LoadABC(*f,gabcxl,helm_bvp.yl+part->elem_starty*hy,hy,0,1,part->nyp,nquad);
    }
    if (helm_bvp.bctype[1]==1 && part->rx==part->px-1) {  /* right side */
      LoadABC(*f,gabcxr,helm_bvp.yl+part->elem_starty*hy,hy,part->nxp*(part->nyp+1),1,part->nyp,nquad);
    }
    if (helm_bvp.bctype[2]==1 && part->ry==0) {  /* bottom side */
      LoadABC(*f,gabcyl,helm_bvp.xl+part->elem_startx*hx,hx,0,part->nyp+1,part->nxp,nquad);
    }
    if (helm_bvp.bctype[3]==1 && part->ry==part->py-1) {  /* top side */
      LoadABC(*f,gabcyr,helm_bvp.xl+part->elem_startx*hx,hx,part->nyp,part->nyp+1,part->nxp,nquad);
    }
    /* * corner condition arising from second-order abc * */
    if (helm_bvp.abc_order==2) {
      if (helm_bvp.bctype[0]<2 && helm_bvp.bctype[2]<2 && (helm_bvp.bctype[0] || helm_bvp.bctype[2]) && part->rx==0 && part->ry==0) { /* down-left corner */
  	x= helm_bvp.xl; y= helm_bvp.yl;
  	if (helm_bvp.bctype[0] && helm_bvp.bctype[2]) {
  	  val= -Dyufun(x,y) - Dxufun(x,y) + helm_bvp.abc_coef[2]*ufun(x,y);
  	}
  	else if (helm_bvp.bctype[0]==0) {
  	  val= -Dxufun(x,y);
  	}
  	else {
  	  val= -Dyufun(x,y);
  	}
  	val*= helm_bvp.abc_coef[1];
  	Ii= 0; 
  	ierr= VecSetValuesLocal(*f,1,&Ii,&val,ADD_VALUES);CHKERRQ(ierr);
      }
      if (helm_bvp.bctype[0]<2 && helm_bvp.bctype[3]<2 && (helm_bvp.bctype[0] || helm_bvp.bctype[3]) && part->rx==0 && part->ry==part->py-1) { /* up-left corner */
  	x= helm_bvp.xl; y= helm_bvp.yr;
  	if (helm_bvp.bctype[0] && helm_bvp.bctype[3]) {
  	  val= Dyufun(x,y) - Dxufun(x,y) + helm_bvp.abc_coef[2]*ufun(x,y);
  	}
  	else if (helm_bvp.bctype[0]==0) {
  	  val= -Dxufun(x,y);
  	}
  	else {
  	  val= Dyufun(x,y);
  	}
  	val*= helm_bvp.abc_coef[1];
  	Ii= part->nyp; 
  	ierr= VecSetValuesLocal(*f,1,&Ii,&val,ADD_VALUES);CHKERRQ(ierr);
      }
      if (helm_bvp.bctype[1]<2 && helm_bvp.bctype[2]<2 && (helm_bvp.bctype[1] || helm_bvp.bctype[2]) && part->rx==part->px-1 && part->ry==0) { /* down-right corner */
  	x= helm_bvp.xr; y= helm_bvp.yl;
  	if (helm_bvp.bctype[1] && helm_bvp.bctype[2]) {
  	  val= -Dyufun(x,y)+Dxufun(x,y)+ helm_bvp.abc_coef[2]*ufun(x,y);
  	}
  	else if (helm_bvp.bctype[1]==0) {
  	  val= Dxufun(x,y);
  	}
  	else {
  	  val= -Dyufun(x,y);
  	}
  	val*= helm_bvp.abc_coef[1];
  	Ii= part->nxp*(part->nyp+1); 
  	ierr= VecSetValuesLocal(*f,1,&Ii,&val,ADD_VALUES);CHKERRQ(ierr);
      }
      if (helm_bvp.bctype[1]<2 && helm_bvp.bctype[3]<2 && (helm_bvp.bctype[1] || helm_bvp.bctype[3]) && part->rx==part->px-1 && part->ry==part->py-1) { /* up-right corner */
  	x= helm_bvp.xr; y= helm_bvp.yr;
  	if (helm_bvp.bctype[1] && helm_bvp.bctype[3]) {
  	  val= Dyufun(x,y) + Dxufun(x,y) + helm_bvp.abc_coef[2]*ufun(x,y);
  	}
  	else if (helm_bvp.bctype[1]==0) {
  	  val= Dxufun(x,y);
  	}
  	else {
  	  val= Dyufun(x,y);
  	}
  	val*= helm_bvp.abc_coef[1];
  	Ii= (part->nxp+1)*(part->nyp+1)-1; 
  	ierr= VecSetValuesLocal(*f,1,&Ii,&val,ADD_VALUES);CHKERRQ(ierr);
      }
    }
    /* * migrate from add_values to insert_values * */
    ierr= VecAssemblyBegin(*f);CHKERRQ(ierr);
    ierr= VecAssemblyEnd(*f);CHKERRQ(ierr);
    /* * Dirichlet boundary values * */
    if (helm_bvp.bctype[0]==2 && part->rx==0) { /* left side */
      LoadDirichlet(*f,uxl,helm_bvp.yl+part->elem_starty*hy,hy,0,1,part->dimyp);
    }
    if (helm_bvp.bctype[1]==2 && part->rx==part->px-1) { /* right side */
      LoadDirichlet(*f,uxr,helm_bvp.yl+part->elem_starty*hy,hy,part->nxp*(part->nyp+1),1,part->dimyp);
    }
    ierr= VecAssemblyBegin(*f);CHKERRQ(ierr);
    ierr= VecAssemblyEnd(*f);CHKERRQ(ierr);
    if (helm_bvp.bctype[2]==2 && part->ry==0) { /* bottom side */
      LoadDirichlet(*f,uyl,helm_bvp.xl+part->elem_startx*hx,hx,0,part->nyp+1,part->dimxp);
    }
    if (helm_bvp.bctype[3]==2 && part->ry==part->py-1) { /* top side */
      LoadDirichlet(*f,uyr,helm_bvp.xl+part->elem_startx*hx, hx,part->nyp,part->nyp+1,part->dimxp);
    } 
    /* * assembly for the above VecSetValues * */
    ierr= VecAssemblyBegin(*f);CHKERRQ(ierr);
    ierr= VecAssemblyEnd(*f);CHKERRQ(ierr);
  }

  ierr= PetscRandomDestroy(&rctx);CHKERRQ(ierr);
  return 0;
}
