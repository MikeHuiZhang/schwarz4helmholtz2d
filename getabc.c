/* =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =
   "GetABCCoef" calculates coefficients of various types of b.c.
  
   INPUT
   - type    0 for Neumann, 1 for Absorbing, 2 for Optimized, 3 for a priori
   - order   effects only when 3 > type > 0, 0 or 1 for first-order Absorbing
             and zero-order Optimized (they have different name conventions)
   - ctx     context for optimized methods, effects only when type==2
     
   = =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  */

#include "modify.h"
 
#undef __FUNCT__
#define __FUNCT__ "GetABCCoef"
PetscErrorCode GetABCCoef(PetscScalar *coef, PetscReal k_coef, PetscInt type, PetscInt order, OptParamCtx *ctx)
{
  PetscScalar    a= 0.0 + 0.0*PETSC_i, b= 0.0 + 0.0*PETSC_i;
  PetscReal      h, g, kmin, Cw, kmin2, CL, kmax, kmax2;
  PetscReal      k2= k_coef*k_coef;

  switch (type) {
  case 0:
    coef[0]= 0.0;  coef[1]= 0.0;  coef[2]= 0.0; coef[3]= 0.0;
    break;
  case 1:
    coef[0]= -PETSC_i*k_coef;
    if (order==2 && k_coef!=0) {
      coef[1]= PETSC_i*0.5/k_coef;
      coef[2]= -PETSC_i*1.5*k_coef;
      coef[3]= 0.75;
    }
    else {
      coef[1]= 0.0;  coef[2]= 0.0; coef[3]= 0.0;
    }
    break;
  case 2:
    if (ctx==PETSC_NULL) {
      printf("GetABCCoef: optimization context is missing when type=2! \
We use instead type=1 i.e. the absorbing condition\n");
      GetABCCoef(coef, k_coef, 1, order,ctx);
      break;
    }
    h= ctx->h; g= ctx->g; kmin= ctx->kmin; kmin2= kmin*kmin;
    CL= ctx->CL/2.0; Cw= k_coef*pow(h,g); kmax= PETSC_PI/h; kmax2= kmax*kmax;
    switch (order) {
    case 0: case 1:
      if (CL) {  /* overlapping */
	/* mode h TODO: add formula for other modes */
	if (k_coef>kmin) {  /* indefinite */
	  coef[0]= (1.0-PETSC_i)*pow(2.*k_coef*kmin-kmin2,1.0/3.0)*pow(CL*h,-1.0/3.0)/2.0;
	}
	else { /* positive definite */
	  coef[0]= pow(0.5*(kmin2-k2)/h/CL,1.0/3.0);
	}
      }
      else {   /* non-overlapping */
	/* printf("setting non-overlapping parameters\n"); */
	/* printf("h= %g, kmin= %g, k2= %g, kmax2= %g\n",h,kmin,k2,kmax2); */
	if (k_coef<kmin) { /* positive definite */
	  coef[0]= pow((kmin2-k2)*(kmax2-k2),.25);
	}
	else {
	  coef[0]= (1.0-PETSC_i)*sqrt(sqrt((2.*k_coef*kmin-kmin2)*(kmax2-k2))/2.);
	}
      }
      coef[1]= 0.0; coef[2]= 0.0; coef[3]= 0.0;
      break;
    default: case 2:
      if (k_coef<kmin) { /* positive definite, h-asymptotic */
	coef[0]= pow((kmin2-k2)*(kmin2-k2)/8./h/CL,.2); /* assume overlapping */
	coef[1]= pow(CL*h,.6)/pow((kmin2-k2)/2.,.2);
	coef[2]= 0.0; coef[3]= 0.0; /* TODO: what corner condition? */
      }
      else { /* indefinite */
	if (CL) { /* overlapping */
	  if (!strcmp(ctx->mode,"w")) { /* mode "w" */
	    if (8./9. < g) {  /* g<=1 is assumed but not checked here */
	      a= pow(Cw,7./8.)*pow(2.*kmin,1./8.)*pow(h,-7.*g/8.);
	      b= .5*pow(Cw,5./8.)*pow(2.*kmin,3./8.)*pow(h,-5.*g/8.);
	    }
	    else {
	      a= .5/pow(CL,.2)*pow(Cw*kmin,.4)*pow(h,-(2.*g+1)/5.);
	      b= .5/pow(CL,.6)*pow(Cw*kmin,.2)*pow(h,-(3.0+g)/5.);
	    }
	    a= a - a*PETSC_i;  b= b - b*PETSC_i;
	  }
	  else {  /* mode "h" */
	    a= .25*pow(2.,.8)*pow(2.*k_coef*kmin-kmin2,.2)*pow(CL*h,-.6);
	    b= .25*pow(2.,.6)*pow(2.*k_coef*kmin-kmin2,.4)*pow(CL*h,-.2);
	    a= a - a*PETSC_i;  b= b - b*PETSC_i;
	    /* another choice */
	    /* a= .5*pow(2,.2)*pow(2.*k_coef*kmin-kmin2,.2)*pow(CL*h,-.6); */
	    /* b= .25*pow(2.,.4)*pow(2.*k_coef*kmin-kmin2,.4)*pow(CL*h,-.2); */
	    /* if (h>=1/800) a= a - a*PETSC_i;  */
	    /* b= b - b*PETSC_i; */
	  }
	  /* a= 21.-74.*PETSC_i; b= 21.-21.*PETSC_i; */
	  /* a= 13.8787-69.7151*PETSC_i; b= 23.8948-18.1208*PETSC_i; */
	  /* a= 64.6039-73.4603*PETSC_i; b= 23.2326-23.6149*PETSC_i; */
	  /* a= 31.8518-26.6555*PETSC_i; b= 32.3566-131.5948*PETSC_i; */
	  /* a= 44.4507-40.4450*PETSC_i; b= 84.7080-252.4103*PETSC_i; */
	  /* a= 23.4740 - 17.7480*PETSC_i;  b= 13.4387 - 69.8816*PETSC_i; */
	  /* a= 2.6832 -  12.3158*PETSC_i; b= 7.5857 - 23.3158*PETSC_i; */
	  /* a= 1.9002 - 5.4015*PETSC_i;  b= 4.4261 - 24.9118*PETSC_i; */
	}
	else {  /* non-overlapping, Gander 2002 */
	  a= (k2-kmin2)*(2*k_coef*kmin-kmin2); a= -PETSC_i*pow(a,.25);
	  b= (kmax2-k2)*(kmax2-2*kmax*k_coef); b= pow(b,.25);
	}
	coef[0]= (a*b-k2)/(a+b);
	coef[1]= 1./(a+b);
	coef[2]= -PETSC_i*k_coef*(2.0+PETSC_i*k_coef*coef[1]); /* c.f. 1990 Bamberger */
	coef[3]= coef[1]*coef[2];
      }
      printf("a= %g+%gi, b= %g+%gi\n",PetscRealPart(a),PetscImaginaryPart(a),PetscRealPart(b),PetscImaginaryPart(b));
      break;
    }
    break;
  default: case 3:  /* do nothing, assume given a priori */
    break;
  }
 
  printf("coef[0]=%g+%gi  coef[1]=%g+%gi\n",PetscRealPart(coef[0]),PetscImaginaryPart(coef[0]),PetscRealPart(coef[1]),PetscImaginaryPart(coef[1]));
  return 0;
}
