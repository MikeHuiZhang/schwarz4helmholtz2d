#ifndef _HELM2D_GETABC
#define _HELM2D_GETABC

PetscErrorCode GetABCCoef(PetscScalar *coef, PetscReal k_coef, PetscInt type, PetscInt order, void *optctx);

#endif
