static char help[] = "Solves the Helmholtz equation in 2D. Usage:\n \
  mpirun -np <p> helm2d -nx <nx> -ny <ny> -px <px> -xr <xr> \\ \n \
  -model_type <0,1,2,3> -ksp_type <e.g. preonly> -pc_type <e.g. lu> \\ \n \
  -pc_factor_mat_solver_package <e.g. mumps>, or\n \
  mpirun -np 4 helm2d -px 2 -nx 256 -sx 1 -freq 20 -model_type 3 -bctype\n \
  1,1,1,1   -xr 1 -nsrc 3 -xsrc 0.45,0.6 -ysrc 0.3,0.3 -abc_order 2 \n \
  -overlap 2 -ksp_rtol 1.0e-8 -ksp_gmres_restart 200 -initial_guess_random 0 -ksp_monitor";


/*
   Description: Solves the Helmholtz equation in 2D.

   The model problem:
      - Delta u - k^2 u = S,  in (xl,xr)X(yl,yr),
        where k= 2*pi*freq/c, freq constant, c cell-wise constants, 
        S is the source function which can be mutiple point sources plus 
        a complex-valued numerical function,
   with Dirichlet or Absorbing boundary conditions on the sides.
      u_n - 1i*k u -0.5i/k u_{ss} = 0,  in the edges,
      u_{n1} + u_{n2} - 1.5i*k u  = 0,  at the corners.
   To discretize the second order abc, we note that
      -\int a1 u_{ss} v 
    = \int a1 u_s v_s + a1 \sum_c (u_{n1}(c) + u_{n2}(c)) v(c),
   substitution of the corner conditions into the above rhs yields
      -\int a1 u_{ss} v 
    = \int a1 u_s v_s - a1 a2 \sum_c u(c) v(c),
    
   Use the 2-D, nine-point P1 element stencil.
   
   We assume:
   (1)  uniform mesh sizes along x, y directions, the number of elements
        along x, y directions are nx, ny, respectively, this makes 
        coordinates can be easily computed from indices, so we may not
	store coordinates
   (2)  px-by-py processors, corresponding to px-by-py pseudo-subdomains
        of the mesh grid;  each pseudo-subdomain is further partitioned 
        to sx-by-sy subdomains; nx>=px, ny>=py       
   (3)  2d subscripts to 1d indices obeys the rule that y runs first
   
   Notes on implementation: 
   (1) Vec index and Mat row/column index start from 0
   (2) Mat is row major, i.e. entries in one row are stored contiguously
   (3) of parallel Vec and Mat, we can control global and local *sizes* 
       but not global *ordering*, PETSC will mangae where a procesor's rows
       go in the Vec/Mat; fortunately, global indices under any global ordering
       that we want can be mapped to the indices under PETSC's ordering, by 
       AOCreateBasic, just passing the local owner ship for the mapped indices, 
       read the manual.pdf on AO (the note on html page is obscure)
   (4) in view of (3), for the linear system Au=f, the ordering of f should 
       use that of A's rows, and the ordering of A's columns should use that
       of u, fortunately, I got answer from petsc's user list that any Mat/Vec
       are partitioned contiguously with processor rank increasing; to make
       sure Vec is compatible with a Mat, one can also uses MatGetVecs
   (5) DMDA can manage cartesian partition of a logically cartesian mesh, 
       but it distributes all residues to the last processor; DMDA can manage
       communication between pseudo-subdomains (i.e. processors), but it 
       always communicate all the ghost-cells (i.e. overlapping region) and
       can not communicate only the boundary of the overlapping region
   (6) in view of (4), we manage partition by hand, and communicate by using
       VecScatter (which is used by PETSC for implementation of RAS)
   (7) local to global mapping through 2d subscripts
   (8) subdomain matrices, two ways (a) first assemble subdomain matrices, 
       then composite global matrix, cons: it is not cheap to composite global
       matrix in the overlapping case, pros: easy for Neumann b.c., suitable 
       for non-matching grids  (b) first assemble global matrix as usual, then 
       restric to subdomains, and treat Neumann/Robin b.c. (algebraically or
       geometrically) which corresponds to interface interactions.
       In both (a) and (b), GetSubMatrix/GetLocalSubMatrix could be useful.
       We choose (b).
   (9) in the volume form of ddm, the residual is nonzero only at a small number
       of nodes, if we first solve M x0 = b with M the subdomains solver to 
       obtain the initial guess x0, then r0= b - A x0 is sparse, this property 
       is useful for steady iterations to compute non-zero entries of the 
       residual; but it can not be retained when we build Krylov subspace 
                       {r0, A r0, A^2 r0, ...},
       becuase every time we multiply A on a sparse vector r, the result has more
       non-zeros than r; anyway, the cost is dominated by solving M but not 
       multiplication by A
   (10) special treatment of cross-points, methods: (a) leave it as an interior
        node, (b) do first-order Robin only along interface (c) fix them as 
	primal constraints (equivalent to coarse basis on the fine grid that is
	harmonic extension of the coarse d.o.f), or do coarse grid by Galerkin
   
   
   Coding hints:
   (1) count++ should be done with correct times and at correct places
   (2) malloc and free should locate in the same statement block such that they will be executed simultaneously
   (3) insert_values before assemblybegin/asemblyend, different processors must NOT insert values at the same global location
   (4) the binary values operator ? must be enclosed by parantheses because its priority is very low
   (5) never pass less arguments than required when calling a function, even though
   sometimes it still works

   BUG:

   (1) when using MUMPS as LUSolver, with bctype= [0 2 0 0], [1 2 1 1], [1 2 0 0],
       etc.  mpirun -np 4 helm2d -nx 64 -px 2 -py 2 -model_type 1
       gives error, any change of bctype, parallel layout or mesh size can correct it,
       but SUPERLU_DIST always works fine 

   (2) mpirun -np 2 ./helm2d -nx 4 -px 1 -py 2 -sx 2 -sy 1 -freq 1.6 -ksp_type gmres -sub_abc_type 0 -nd 0
       gives wrong submatrix for the up-right subdomain, but if we use -px 2 -py 2, or
       -sx 2 -sy 2, or -px 2 -py 1 -sx 1 -sy 2, they give the same submatrix
       The problem comes from PETSC when PCASMSetSortIndices is set to FALSE and PCASMGetSubMatrices
       goes wrong.

   (3) mpirun -np 2 ./helm2d -nx 128 -px 2 -py 1 -sx 1 -sy 2 -freq 16 -ksp_type gmres -sub_abc_type 1 -nd 0 -model_type 1
       under petsc-dev gives large errors:
       discretization error: Maximum 2.28087,  l^2*sqrt(hx*hy) 0.486375
       iterative error: Maximum 2.28087,  l^2*sqrt(hx*hy) 0.486375 
       but under petsc-3.2 the erros are very small;  even under petsc-dev, but change px, py, sx, sy the erros are also
       small; or we change in kspdirect.c from MATSOLVERSUPERLU_DIST to MATSOLVERMUMPS
       

   TODO:
   () special treatment of cross-points
   () complex (PetscScalar) k_coef and k2
   () postprocessing: data output into files and database (PyTables), visualization
   () PML
   () multiple processors for each subdomain, but PCGASM is broken
   () substructured form
   () non-overlapping, no RAS form, only extended or substurctured

*/


/* 
   Include "petscksp.h" so that we can use KSP solvers.  Note that this file
   automatically includes:
      petscsys.h,  petscvec.h, petscmat.h, petscis.h - index sets, 
      petscviewer.h - viewers,  petscpc.h - preconditioners
 */   
#include <petscksp.h>
#include <petscao.h>
#include <time.h>      /* for random seed */
#include "solutions.h" /* example of exact solutions */
#include "assembly.h"  
#include "modify.h"    /* structure types for modifying submatrices */
#include "getabc.h"
#include "mypart.h"


PetscErrorCode HelmDDInit(PetscMPIInt rank, MyPartition *part, SubMatCtx *ctx, PetscInt *nquad, PetscInt *nd, PetscInt *cform, PetscBool *init_rand, PetscBool *search_direct, PetscBool *search_nm, PetscInt *search_form, PetscInt *search_maxit);

PetscErrorCode ComputeA(Mat *A, AO *ao, ISLocalToGlobalMapping *ltog, MyPartition *part);

PetscErrorCode Computef(Vec *f, Vec *u, Vec *ustar, Mat A, MyPartition *part, ISLocalToGlobalMapping ltog, PetscInt nquad, PetscBool initial_guess_random);

PetscErrorCode DDSetupKSP(KSP *kspdd, Mat A, MyPartition *part, AO ao, SubMatCtx *ctx, PetscBool initrand, PetscInt nd, PetscInt form);

PetscErrorCode DirectSetupKSP(KSP *ksp, Mat A);

PetscErrorCode CHKErrNorm(Vec uerr, const Vec u, const Vec ustar, PetscReal hx, PetscReal hy);

PetscErrorCode WriteResHistory(const KSP kspdd, const char *filename);

HelmBVP helm_bvp;

typedef struct
{
  PetscInt    np;		/* number of search parameters */
  Mat         A;
  MyPartition *part;
  AO          ao;
  SubMatCtx   *submat_ctx;
  PetscBool   irand;		/* initial guess random */
  Vec         f;
  Vec         udd;
  Vec         uinit;
  PetscInt    nd;
  PetscInt    cform;
  PetscInt    search_form;
  PetscInt    maxit;
} KSPDDCtx;

PetscInt NelderMead(PetscReal *xopt, PetscReal *fopt, PetscReal (*f)(PetscReal *x, void *ctx), void *ctx, PetscInt nx, const PetscReal *x0, PetscReal tolx, PetscReal tolf, PetscInt maxiter);

PetscErrorCode DirectSearch(PetscReal *xopt, PetscReal *fopt, const char *filename, PetscReal (*f)(PetscReal *x, void *ctx), void *ctx, PetscInt dimx, const PetscReal *xl, const PetscReal *xr, const PetscInt *nj, PetscBool iwrite);


PetscReal Param2Res(PetscReal *p, KSPDDCtx *ctx);

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **args)
{
  PetscBool      init_rand, search_direct, search_nm; 
  PetscInt       nquad, nd, cform, search_form, search_maxit;
  Vec            u,f,ustar,udd, uerr;   /* approx solution, RHS, exact solution, error function */
  Mat            A;               /* linear system matrix */
  KSP            ksp, kspdd;      /* linear solver contexts */
  PetscMPIInt    rank;            /* process rank */
  ISLocalToGlobalMapping ltog;
  AO             petsc_nature_ao;
  PetscErrorCode ierr;
  PetscReal      hx, hy;  
  PetscScalar    a0= 1.0+PETSC_i;  /* this assigns a wrong value 1+0i */
  MyPartition    part;
  SubMatCtx      submat_ctx;
  
  PetscInitialize(&argc,&args,PETSC_NULL,help);
#if !defined(PETSC_USE_COMPLEX)
  SETERRQ(PETSC_COMM_WORLD,1,"helm2d.c: requires complex numbers version of petsc");
#endif
  a0= 1.0+PETSC_i;  /* this assigns the value correctly */
  PetscPrintf (PETSC_COMM_WORLD, "%g+%gi\n",PetscRealPart(a0),PetscImaginaryPart(a0));

  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  HelmDDInit(rank,&part,&submat_ctx,&nquad,&nd,&cform,&init_rand,&search_direct,&search_nm,&search_form,&search_maxit);  
  hx= submat_ctx.hx;  hy= submat_ctx.hy;
  helm_bvp_view(&helm_bvp);
  mypart2d_view(&part, rank);



  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Compute the matrix and RHS that define the linear system, Au = f.
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
 
  ComputeA(&A,&petsc_nature_ao,&ltog,&part);
  Computef(&f,&udd,&ustar,A,&part,ltog,nquad,init_rand);
  VecDuplicate(f,&u);


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Solve the linear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /*
     Direct Solver
  */
  DirectSetupKSP(&ksp, A);
  ierr = KSPSolve(ksp,f,u);CHKERRQ(ierr);

  /*
     Search for good parameters
  */
  if (search_direct || search_nm) {
    submat_ctx.abc_type= 3; submat_ctx.abc_order= 2;
    PetscReal *popt, iteropt;
    Vec uinit;
    VecDuplicate(udd,&uinit);  VecCopy(udd,uinit);
    KSPDDCtx  kspdd_ctx= {4,A,&part,petsc_nature_ao,&submat_ctx,init_rand,f,udd,uinit,nd,cform,search_form,search_maxit};
    switch (search_form){
    case 2:
      kspdd_ctx.np= 4;
      break;
    case 0: case 1: default:
      kspdd_ctx.np= 2;
      break;
    }
    ierr= PetscMalloc(kspdd_ctx.np*sizeof(PetscReal),&popt);CHKERRQ(ierr);

    if (search_direct){ /* direct search */
      PetscReal *xl, *xr;
      PetscInt *nj;
      ierr= PetscMalloc(kspdd_ctx.np*sizeof(PetscReal),&xl);CHKERRQ(ierr);
      ierr= PetscMalloc(kspdd_ctx.np*sizeof(PetscReal),&xr);CHKERRQ(ierr);
      ierr= PetscMalloc(kspdd_ctx.np*sizeof(PetscInt),&nj);CHKERRQ(ierr);
      xl[0]= 0; xl[1]= -90;  xl[2]= 0; xl[3]= 0.002;
      xr[0]= 0; xr[1]= -100; xr[2]= 0; xr[3]= 0.004;
      nj[0]= 0; nj[1]= 1;    nj[2]= 0; nj[3]= 1;
      DirectSearch(popt,&iteropt,"direct_search_result.m",Param2Res,&kspdd_ctx,kspdd_ctx.np,xl,xr,nj,!rank);
      ierr= PetscFree(xl);CHKERRQ(ierr);
      ierr= PetscFree(xr);CHKERRQ(ierr);
      ierr= PetscFree(nj);CHKERRQ(ierr);
    }
  
    if (search_nm) {  /* Nelder-Mead */
      PetscReal *pinit;
      PetscInt i;
      ierr= PetscMalloc(kspdd_ctx.np*(kspdd_ctx.np+1)*sizeof(PetscReal),&pinit);CHKERRQ(ierr);
      switch (search_form){
      case 1:
	pinit[0]= 0;		/* TO0 fixed + second-order free */
	pinit[1]= 0.0045; //0.5/helm_bvp.k_coef;
	submat_ctx.abc_coef[0]= -helm_bvp.k_coef*PETSC_i;
	break;
      case 2:
	  pinit[0]= 0.0007;
	  pinit[1]= -164.698;//-helm_bvp.k_coef;
	  pinit[2]= 0.0;
	  pinit[3]= 0.0041; //0.5/helm_bvp.k_coef;
	  break;
      default:
	printf("main(): search_form must be one of 1, 2\n");
	break;
      }
      /* initial simplex */
      for (i = 1; i < kspdd_ctx.np+1; ++i) {
	ierr= PetscMemcpy(pinit+i*kspdd_ctx.np,pinit,kspdd_ctx.np*sizeof(PetscReal));CHKERRQ(ierr);
	pinit[i*kspdd_ctx.np+i-1] += 0.05;
      }
      /* optimization by Nelder-Mead */
      NelderMead(popt,&iteropt,Param2Res,&kspdd_ctx,kspdd_ctx.np,pinit,1e-4,1e-15,1000);
      ierr= PetscFree(pinit);CHKERRQ(ierr);
    }

    /* use the optimal parameters */
    switch (search_form){
    case 1:			/* TO0 fixed + second-order free */
      submat_ctx.abc_coef[0]= -helm_bvp.k_coef*PETSC_i;
      submat_ctx.abc_coef[1]= popt[0]+popt[1]*PETSC_i;
      break;
    case 2:
      submat_ctx.abc_coef[0]= popt[0] + popt[1]*PETSC_i;
      submat_ctx.abc_coef[1]= popt[2] + popt[3]*PETSC_i;
      break;
    default:
      printf("main(): search_form must be one of 1, 2\n");
      break;
    }

    ierr= VecCopy(uinit,udd);CHKERRQ(ierr);
    ierr= PetscFree(popt);CHKERRQ(ierr);
    ierr= VecDestroy(&uinit);CHKERRQ(ierr);
  }


   
  /*
     Setup and solve by DD
  */
  DDSetupKSP(&kspdd, A, &part, petsc_nature_ao, &submat_ctx, init_rand, nd, cform); 
  ierr= KSPSolve(kspdd,f,udd);CHKERRQ(ierr);
  
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Check solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  
  /*
     Check the error and print iteration numbers
  */
  VecDuplicate(u,&uerr); 
  PetscViewer binv;
  

  /* the discretization error */
  if (helm_bvp.model_type<3) {
    PetscPrintf(PETSC_COMM_WORLD, "discretization error: ");
    CHKErrNorm(uerr, u, ustar, hx, hy);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"ustar",FILE_MODE_WRITE,&binv);
    VecView(ustar,binv);  PetscViewerDestroy(&binv);
    PetscViewerBinaryOpen(PETSC_COMM_WORLD,"ufem",FILE_MODE_WRITE,&binv);
    VecView(u,binv);  PetscViewerDestroy(&binv);
  }

  /* the iterative error with PCASM */
  PetscPrintf(PETSC_COMM_WORLD, "iterative error: ");
  CHKErrNorm(uerr, udd, u, hx, hy);

  /* write residual history */
  if (rank==0){
    WriteResHistory(kspdd,"resplot.m");
  }

  /*
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
  */
  mypart2d_destroy(&part); 
  helm_bvp_destroy(&helm_bvp); 
  submat_ctx_destroy(&submat_ctx);
  ierr= KSPDestroy(&ksp);CHKERRQ(ierr);  
  ierr= KSPDestroy(&kspdd);CHKERRQ(ierr);
  ierr= VecDestroy(&u);CHKERRQ(ierr); ierr= VecDestroy(&ustar);CHKERRQ(ierr);
  ierr= VecDestroy(&udd);CHKERRQ(ierr); ierr= VecDestroy(&uerr);CHKERRQ(ierr); 
  ierr= VecDestroy(&f);CHKERRQ(ierr); 
  ierr= MatDestroy(&A);CHKERRQ(ierr);
  ierr= ISLocalToGlobalMappingDestroy(&ltog);CHKERRQ(ierr);
  ierr= AODestroy(&petsc_nature_ao);CHKERRQ(ierr);
 
  PetscFinalize();
  
  return 0;
}

/* ----------------------------------------------------------------------- 
   Param2Res:
     given the parameters of the optimized Schwarz method, return the
     residual.
     The user is responsible to make sure (ctx->submat_ctx)->abc_type=3 
     and ..->abc_order are correct.
   ----------------------------------------------------------------------- */

#undef __FUNCT__ 
#define __FUNCT__ "Param2Res"
PetscReal Param2Res(PetscReal *p, KSPDDCtx *ctx)
{
  KSP                kspdd;
  PetscErrorCode     ierr;
  /* PetscInt           iter; */
  /* KSPConvergedReason reason; */
  PetscReal          rnorm;

  PetscFunctionBegin;

  printf("p: %f, %f, %f, %f\n",p[0],p[1],p[2],p[3]);
  switch (ctx->search_form){
  case 1:	    /* TO0 fixed + second-order free */
    (ctx->submat_ctx)->abc_coef[1]= p[0]+p[1]*PETSC_i;
    break;
  case 2:
    (ctx->submat_ctx)->abc_coef[0]= p[0] + p[1]*PETSC_i;
    (ctx->submat_ctx)->abc_coef[1]= p[2] + p[3]*PETSC_i;
    break;
  default:
    printf("Param2Res: ctx->np (number of parameters) must be one of 2, 4\n");
    break;
  }

  /* TODO: any way to re-use kspdd and its pc */
  DDSetupKSP(&kspdd, ctx->A, ctx->part, ctx->ao, ctx->submat_ctx, ctx->irand,ctx->nd, ctx->cform);

  ierr= KSPSetTolerances(kspdd,1.0e-20,1.0e-20,1.0e+6,ctx->maxit); /* adjust these */
  // ierr= KSPSetPCSide(kspdd,PC_RIGHT);CHKERRQ(ierr);
  VecCopy(ctx->uinit,ctx->udd);CHKERRQ(ierr);
  ierr= KSPSolve(kspdd,ctx->f,ctx->udd);CHKERRQ(ierr);
  /* ierr= KSPGetConvergedReason(kspdd,&reason);CHKERRQ(ierr); */
  /* if (reason>0) { */
  /*   ierr= KSPGetIterationNumber(kspdd,&iter);CHKERRQ(ierr); */
  /* } */
  /* else { */
  /*   iter= 1e4; */
  /* } */
  ierr= KSPGetResidualNorm(kspdd,&rnorm);CHKERRQ(ierr);
 
  ierr= KSPDestroy(&kspdd);CHKERRQ(ierr);

  PetscFunctionReturn(rnorm);
}
