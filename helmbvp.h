/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
            Global Variables
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#ifndef _HELM2D_GLOBALS
#define _HELM2D_GLOBALS

#include <petscsys.h>

typedef struct
{
  /* 
     the frequency and the wavenumber 
  */
  PetscReal  freq;
  PetscReal  cspeed; 
  PetscReal  k_coef;
  PetscReal  k2;

  /* 
     the domain (xl,xr) X (yl,yr) 
  */
  PetscReal  xl;
  PetscReal  xr;
  PetscReal  yl;
  PetscReal  yr; 

  /* 
     The absorbing-like boundary conditions with u are
     u_n + abc_coef[0] u - abc_coef[1] u_{ss},  on the edges,
     and 
     u_{n1}+u_{n2} + abc_coef[1] u,  at corners, 
     while abc_coef12 is from substitution of the corner condition to the
     integration by parts of u_{ss}.
  */
  PetscInt    abc_order; 
  PetscScalar *abc_coef;

  /* 
     boundary conditions on the exterior four sides, the order is left, right, 
     bottom, and top, 0 for Neumann, 1 for absorbing, 2 for Dirichlet 
  */ 
  PetscInt    *bctype;

  /*
    0: random vector as exact discrete solution; 
    1: continuous exact solution and continuous source term; 
    2: point sources, zero abc, with exact solution unknown;
    3: random vector as right hand side
  */
  PetscInt    model_type; 

  /*
     point sources, useful only when model_type==2
  */
  PetscInt    nsrc;
  PetscReal  *xsrc;
  PetscReal  *ysrc;
} HelmBVP;

extern HelmBVP helm_bvp; 

PetscErrorCode helm_bvp_destroy(HelmBVP *bvp);
PetscErrorCode helm_bvp_view(HelmBVP *bvp);


#endif
