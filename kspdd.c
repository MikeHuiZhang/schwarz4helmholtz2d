#include <petscksp.h>
#include <petscao.h>
#include "modify.h"
#include "mypart.h"
#include <lapacke/lapacke.h>

PetscErrorCode ModifySubMatrices(PC pc,PetscInt nsub,const IS *row,const IS *col,Mat *submat,SubMatCtx *ctx);

static PetscErrorCode DDSetupPCSubs(PC *pcsubs, Mat A, MyPartition *part, AO ao, SubMatCtx *ctx);

static PetscInt myFindInSorted(PetscInt value, const PetscInt *array, PetscInt length);

static PetscErrorCode DDSetupCoarseBasisSparse(Mat *C, Mat *CO, PC pcsubs, MyPartition *part, AO ao, PetscInt nd, PetscReal epsilon, SubMatCtx *ctx);

static PetscErrorCode CoarseGetAllEdgesModes(PetscScalar ****edge_modes, PetscInt ***num_modes, PetscInt *nc, PetscInt *max_ncs, PetscInt nd, PetscReal epsilon, MyPartition *part, SubMatCtx *ctx);

static PetscErrorCode CoarseGetEdgeModes(PetscScalar **modes, PetscInt *num_modes, PetscInt ne, PetscReal h, PetscInt nd, PetscReal k_coef, PetscReal epsilon, PetscInt edge_direction);

static PetscErrorCode CoarseExtendFromEdgeSparse(Mat *C, Mat *CO, PetscInt jC, KSP ksp, Vec f, PetscScalar *edge_mode, PetscInt *iedge, PetscInt ledge, PetscInt *ii, PetscInt *iC, PetscInt dim, PetscScalar *fi, PetscInt *iiO, PetscInt *iCO, PetscInt dimO, PetscScalar *fO);

static PetscErrorCode ExtensionGetIndices(PetscInt **ii, PetscInt **iC, PetscInt *dim, PetscInt **iiO, PetscInt **iCO, PetscInt *dimO, PetscInt i, MyPartition *part, AO ao, ISLocalToGlobalMapping ltog);

static PetscErrorCode DDSetupCoarseBasis(Mat *C, Mat *CO, PC pcsubs, MyPartition *part, AO ao, PetscInt nd, PetscReal epsilon, SubMatCtx *ctx);

static PetscErrorCode CoarseGetBoundaryModes(PetscScalar **modes, PetscInt *num_modes, PetscInt **inodes, PetscInt *nnodes, PetscInt bctype[], PetscInt nx, PetscInt ny, PetscReal hx, PetscReal hy, PetscInt nd, PetscReal k_coef, PetscReal epsilon);

/* ----------------------------------------------------------------------- 
   Setup the KSP using the one- or two-level Schwarz method
INPUT:
  A
  part
  ao
  ctx
  initrand
  nd   number of directions for plane waves
  cform 0~3

OUTPUT:
  kspdd
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DDSetupKSP"

PetscErrorCode DDSetupKSP(KSP *kspdd, Mat A, MyPartition *part, AO ao, SubMatCtx *ctx, PetscBool initrand, PetscInt nd, PetscInt cform)
{
  PC                  pcdd, *subpc, pcc;
  PetscErrorCode      ierr;
  Mat                 C, CO, CH, AC, Acc;
  PetscReal           epsilon= 0.2;  /* threshold for filtering coarse basis */
  KSP                 kspcoarse;
  PetscInt            subdomain= 0, coarse= 1; /* choose order 0 or 1 for composition */

  PetscFunctionBegin;

  ierr= KSPCreate(PETSC_COMM_WORLD,kspdd);CHKERRQ(ierr);
  ierr= PetscObjectSetOptionsPrefix((PetscObject)(*kspdd), "dd_"); CHKERRQ(ierr);
  ierr= KSPSetOperators(*kspdd,A,A);CHKERRQ(ierr);
  ierr= KSPGetPC(*kspdd,&pcdd);CHKERRQ(ierr);
  /* ierr= KSPSetPCSide(*kspdd,PC_RIGHT);CHKERRQ(ierr); /\* PC_RIGHT to get true residuals *\/ */

  /* define PC's */
  if (!nd) {
    DDSetupPCSubs(&pcdd, A, part, ao, ctx); 
  }
  else {
    ierr= PCSetType(pcdd,PCCOMPOSITE);CHKERRQ(ierr);
    ierr= PCCompositeSetType(pcdd,PC_COMPOSITE_MULTIPLICATIVE);CHKERRQ(ierr); /* SYMMETRIC_MULTIPLICATIVE */
    if (subdomain<coarse) {
      PCCompositeAddPC(pcdd,PCASM);  PCCompositeAddPC(pcdd,PCGALERKIN);
    }
    else {
      PCCompositeAddPC(pcdd,PCGALERKIN);  PCCompositeAddPC(pcdd,PCASM);
    }
    ierr= PetscMalloc(2*sizeof(PC),&subpc);CHKERRQ(ierr);
    PCCompositeGetPC(pcdd,0,subpc); PCCompositeGetPC(pcdd,1,subpc+1);
    DDSetupPCSubs(subpc+subdomain, A, part, ao, ctx); 
    ierr= PCSetOperators(subpc[subdomain],A,A);CHKERRQ(ierr);
    ierr= PCSetUp(subpc[subdomain]);CHKERRQ(ierr);
    switch (cform) {
    case 3:			/* CO (CO^T A CO)^(-1) CO^T */
      DDSetupCoarseBasisSparse(PETSC_NULL, &CO, subpc[subdomain], part, ao, nd, epsilon, ctx);
      ierr= MatTranspose(CO,MAT_INITIAL_MATRIX,&CH);CHKERRQ(ierr); /* MatHermitianTranspose worse */
      ierr= MatMatMult(A,CO,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&AC);CHKERRQ(ierr);
      ierr= MatMatMult(CH,AC,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Acc);CHKERRQ(ierr);
      MatDestroy(&AC);
      ierr= PCGalerkinSetRestriction(subpc[coarse],CH);CHKERRQ(ierr);
      ierr= PCGalerkinSetInterpolation(subpc[coarse],CO);CHKERRQ(ierr);
      break;
    case 2:			/* C (CO^H A CO)^(-1) CO^H */
      DDSetupCoarseBasisSparse(&C, &CO, subpc[subdomain], part, ao, nd, epsilon, ctx);
      ierr= MatHermitianTranspose(CO,MAT_INITIAL_MATRIX,&CH);CHKERRQ(ierr); /* MatTranspose worse */
      ierr= MatMatMult(A,CO,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&AC);CHKERRQ(ierr);
      ierr= MatMatMult(CH,AC,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Acc);CHKERRQ(ierr);
      MatDestroy(&AC);
      ierr= PCGalerkinSetRestriction(subpc[coarse],CH);CHKERRQ(ierr);
      ierr= PCGalerkinSetInterpolation(subpc[coarse],C);CHKERRQ(ierr);
      break;
    case 1:			/* C (CO^T A C)^(-1) CO^T */
      /* DDSetupCoarseBasis(&C, &CO, subpc[subdomain], part, ao, nd, epsilon, ctx); */
      DDSetupCoarseBasisSparse(&C, &CO, subpc[subdomain], part, ao, nd, epsilon, ctx);
      ierr= MatTranspose(CO,MAT_INITIAL_MATRIX,&CH);CHKERRQ(ierr); /* MatHermitianTranspose much worse */
      ierr= MatMatMult(A,C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&AC);CHKERRQ(ierr);
      ierr= MatMatMult(CH,AC,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Acc);CHKERRQ(ierr);
      MatDestroy(&AC);
      ierr= PCGalerkinSetRestriction(subpc[coarse],CH);CHKERRQ(ierr);
      ierr= PCGalerkinSetInterpolation(subpc[coarse],C);CHKERRQ(ierr);
      break;
    case 0: default: 		/* C (C^T A C)^(-1) C^T */
      DDSetupCoarseBasisSparse(&C, PETSC_NULL, subpc[subdomain], part, ao, nd, epsilon, ctx);
      ierr= MatTranspose(C,MAT_INITIAL_MATRIX,&CH);CHKERRQ(ierr); /* MatHermitianTranspose worse */
      ierr= MatMatMult(A,C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&AC);CHKERRQ(ierr);
      ierr= MatMatMult(CH,AC,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Acc);CHKERRQ(ierr);
      MatDestroy(&AC);
      ierr= PCGalerkinSetRestriction(subpc[coarse],CH);CHKERRQ(ierr);
      ierr= PCGalerkinSetInterpolation(subpc[coarse],C);CHKERRQ(ierr);
      break;
    }
    /* MatView(Acc,PETSC_VIEWER_STDOUT_WORLD); */
    PCGalerkinGetKSP(subpc[coarse],&kspcoarse); KSPSetOperators(kspcoarse,Acc,Acc);
    KSPSetType(kspcoarse,KSPPREONLY); KSPGetPC(kspcoarse,&pcc); 
    ierr= PCSetType(pcc,PCLU);CHKERRQ(ierr);
    ierr= PCFactorSetMatSolverPackage(pcc,MATSOLVERPASTIX);CHKERRQ(ierr);
  }

  /* setup kspdd: residual history, random initial guess, use prefix sub 
     to set the same ksp options for the subdomain problems -sub_pc_type <pc> 
     -sub_ksp_type <ksp> -sub_ksp_rtol 1.e-4 */
  ierr= KSPSetResidualHistory(*kspdd,PETSC_NULL,PETSC_DECIDE,PETSC_TRUE);CHKERRQ(ierr);
  if (initrand) {
    ierr= KSPSetInitialGuessNonzero(*kspdd,PETSC_TRUE);CHKERRQ(ierr);
  }
  ierr= KSPSetTolerances(*kspdd,1.0e-8,1.0e-6,1.0e+3,3000);
  ierr= PetscOptionsSetValue("-dd_ksp_monitor",PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsSetValue("-dd_ksp_gmres_restart","200");CHKERRQ(ierr);
  ierr= KSPSetFromOptions(*kspdd);CHKERRQ(ierr);    

  PetscFunctionReturn(0);
}

/* ----------------------------------------------------------------------- 
   DDSetupPCSubs: 
   For the moment, if we set PCASMSetSortIndices to FALSE, PC(G)ASM has
   bug itself. So we still set it to FALSE, but sort the IS as PCASM does 
   before passing to PCASM, and build the ltog accordingly. 
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DDSetupPCSubs"
PetscErrorCode DDSetupPCSubs(PC *pcsubs, Mat A, MyPartition *part, AO ao, SubMatCtx *ctx)
{
  PetscErrorCode      ierr;
  IS                 *is_local, *is;
  PetscInt           *indices, i, j, ix, iy, count, *numbering;
  PetscBool           sort= PETSC_FALSE;
  ISLocalToGlobalMapping *ltog;
  const PetscInt *newindices; 

  PetscFunctionBegin;

  /* ierr = PCCreate(PETSC_COMM_WORLD,pcsubs);CHKERRQ(ierr); */
  ierr= PCSetType(*pcsubs,PCASM);CHKERRQ(ierr);
  ierr= PCASMSetType(*pcsubs,PC_ASM_RESTRICT);CHKERRQ(ierr);
  ierr= PCASMSetSortIndices(*pcsubs,sort);CHKERRQ(ierr);

  /* define IS for non-overlapping subdomains */
  ierr= PetscMalloc(part->s*sizeof(IS),&is_local);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    count= 0;
    ierr= PetscMalloc(part->dims[i]*sizeof(PetscInt),&indices);CHKERRQ(ierr);
    for (ix = part->selem_startx[i]; ix < part->selem_startx[i] + part->dimxs[i]; ++ix) {
      for (iy = part->selem_starty[i]; iy < part->selem_starty[i] + part->dimys[i]; ++iy) {
        indices[count++]= (ix + part->elem_startx)*(part->ny+1) + iy + part->elem_starty;
      }
    }
    ierr= AOApplicationToPetsc(ao,part->dims[i],indices);CHKERRQ(ierr);
    ierr= ISCreateGeneral(PETSC_COMM_SELF,part->dims[i],indices,PETSC_OWN_POINTER,&(is_local[i]));CHKERRQ(ierr);
    ISSort(is_local[i]);
    /* DEBUG: */
    /* ISView(is_local[i],PETSC_VIEWER_STDOUT_SELF); */
  }
 
  /* define IS for overlapping subdomains */
  ierr= PetscMalloc(part->s*sizeof(IS),&is);CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(ISLocalToGlobalMapping),&ltog);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    count= 0;
    ierr= PetscMalloc(part->dimso[i]*sizeof(PetscInt),&indices);CHKERRQ(ierr);
    for (ix = part->startxo[i]; ix < part->endxo[i]; ++ix) {
      for (iy = part->startyo[i]; iy < part->endyo[i]; ++iy) {
        indices[count++]= ix*(part->ny+1) + iy;
      }
    }
    ierr= AOApplicationToPetsc(ao,part->dimso[i],indices);CHKERRQ(ierr);
    ierr= ISCreateGeneral(PETSC_COMM_SELF,part->dimso[i],indices,PETSC_COPY_VALUES,&(is[i]));CHKERRQ(ierr);
    
    /* create local indices that corresponds to sorted global indices is[i] */
    
    /* DEBUG: */
    {
      /* printf("before sort\n"); */
      /* ISView(is[i],PETSC_VIEWER_STDOUT_SELF); */
    }
    ISSort(is[i]); ISGetIndices(is[i],&newindices);
    PetscMalloc(part->dimso[i]*sizeof(PetscInt),&numbering);
    for (j = 0; j < part->dimso[i]; ++j) {
      numbering[j]= myFindInSorted(indices[j], newindices, part->dimso[i]);
    }
    ISRestoreIndices(is[i],&newindices);  
    PetscFree(indices);
    ISLocalToGlobalMappingCreate(PETSC_COMM_SELF,1,part->dimso[i],numbering,PETSC_COPY_VALUES,ltog+i);
    
    /* DEBUG: */
    {
      /* printf("after sort\n"); */
      /* ISView(is[i],PETSC_VIEWER_STDOUT_SELF); */
      /* ISLocalToGlobalMappingView(ltog[i],PETSC_VIEWER_STDOUT_SELF); */
    }
  }
  
  /* set (*pcsubs) with the subdomains IS's */
  ierr= PCASMSetLocalSubdomains(*pcsubs,part->s,is,is_local);CHKERRQ(ierr);

  /* set (*pcsubs) with the routine for modifying submatrices */
  ctx->ltog= ltog;
  ierr= PCSetModifySubMatrices(*pcsubs,ModifySubMatrices,ctx);CHKERRQ(ierr);

  /* free work space: after reading the source codes of PCASM, we know
     these IS are copied by PCASMSetLocalSubdomains */
  for (i=0; i<part->s; i++) {
    ierr = ISDestroy(&is[i]);CHKERRQ(ierr);
    ierr = ISDestroy(&is_local[i]);CHKERRQ(ierr);
  }
  ierr= PetscFree(is);CHKERRQ(ierr);  ierr= PetscFree(is_local);CHKERRQ(ierr);

  /* set command-line options */
  /* FIXME: choose cholesky or lu, choose petsc, mumps, our Dirichlet treatment
   is not symmetric so can not use cholesky */
  ierr= PetscObjectSetOptionsPrefix((PetscObject)(*pcsubs), "dd_"); CHKERRQ(ierr);
  ierr= PetscOptionsSetValue("-dd_sub_ksp_type","preonly");CHKERRQ(ierr);
  ierr= PetscOptionsSetValue("-dd_sub_pc_type","lu");CHKERRQ(ierr);
  ierr= PetscOptionsSetValue("-dd_sub_pc_factor_mat_solver_package","mumps");CHKERRQ(ierr);
  ierr= PCSetFromOptions(*pcsubs);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoarseGetEdgeModes"
/* ----------------------------------------------------------------------- 
   ne:  number of elements on the edge
   h:   mesh size along the edge
   nd:  number of positive frequencies on the edge
   edge_direction: 0: x, 1: y
   Note that according to the construction of PCASM, the Dirichlet transmission
   is implemented so that the submatrices are for the interior nodes excluding
   the Dirichlet boundary!  In that case, the modes are *not* Dirichlet 
   data but source terms.
   ----------------------------------------------------------------------- */
PetscErrorCode CoarseGetEdgeModes(PetscScalar **modes, PetscInt *num_modes, PetscInt ne, PetscReal h, PetscInt nd, PetscReal k_coef, PetscReal epsilon, PetscInt edge_direction)
{
  PetscInt       i, j, *jpvt, info, m, n, *pass;
  PetscErrorCode ierr;
  PetscScalar    *a, *tau;
  
  PetscFunctionBegin;

  /* initial modes */
  m= ne + 1;
  ierr= PetscMalloc(2*nd*m*sizeof(PetscScalar),&a);CHKERRQ(ierr);
  ierr= PetscMemzero(a, 2*nd*m*sizeof(PetscScalar));CHKERRQ(ierr);
#if 1			/* choose this or the next */
  /* determine modes from frequency along interface: better */
  PetscInt mk, jl, jr;
  PetscReal H, kmin; 
  H= ne*h;
  kmin= PETSC_PI/H;
  mk= floor(k_coef/kmin); 
  jl= (mk - nd + 1 > 0) ? mk-nd+1 : 0; jr= mk+1;
  /* jl= 0;  jr= nd; */
  n= 0;
  for (j = jl; j < jr; ++j) { /* for each frequency */
    for (i = 0; i < m; ++i) {			
      a[n*m+i]= cos(j*kmin*i*h); /* i-th node is i*h, natural order */
      a[(n+1)*m+i]= sin(j*kmin*i*h);
    }
    n+= 2;
  }
#else 
  /* use traces of plane waves: worse */
  PetscReal      t, dt, p;
  dt= PETSC_PI/nd; n= 0;
  for (t = 0; t < PETSC_PI; t+=dt) { /* for each angle */
    p= cos(t)*(1-edge_direction) + sin(t)*edge_direction;
    for (i = 0; i < m; ++i) {
      a[n*m+i]= cos(k_coef*p*i*h);
      a[(n+1)*m+i]= sin(k_coef*p*i*h);
    }
    n+= 2;
  }
#endif

  

  /* DEBUG: */
  {
    /* printf("a: before qr \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }
  /* filtering, currently use laplacke (sequential) */
  ierr= PetscMalloc(n*sizeof(PetscInt),&jpvt);CHKERRQ(ierr);
  ierr= PetscMalloc(n*sizeof(PetscScalar),&tau);CHKERRQ(ierr);
  info= LAPACKE_zgeqp3(LAPACK_COL_MAJOR, m, n, a, m, jpvt, tau);
  
  /* DEBUG: */
  {
    /* printf("a: after qr \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }
  
  n= (m<n)?m:n; 		/* number of columns of Q */
  ierr= PetscMalloc(n*sizeof(PetscInt),&pass);CHKERRQ(ierr);
  (*num_modes)= 0;
  for (j = 0; j < n; ++j) {
    pass[j]= (PetscAbsScalar(a[j*m+j])>epsilon) ? 1 : 0;
    /* printf("abs(a) = %g", PetscAbsScalar(a[j*m+j])); */
    if (pass[j]) (*num_modes)++;
  }
  info= LAPACKE_zungqr(LAPACK_COL_MAJOR, m, n, n, a, m, tau);

  /* DEBUG: */
  {
    /* printf("a: got q \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }

  /* copy passed modes to the output 'modes' */
  ierr= PetscMalloc((*num_modes)*m*sizeof(PetscScalar),modes);CHKERRQ(ierr);
  i= 0;
  for (j = 0; j < n; ++j) {
    if (pass[j]) {
      PetscMemcpy((*modes)+i*m,a+j*m,m*sizeof(PetscScalar));
      i++;
    }
  } 

  PetscFree(a); PetscFree(jpvt); PetscFree(tau); PetscFree(pass);
  PetscFunctionReturn(0);
}


/* ----------------------------------------------------------------------- 
   DDSetupCoarseBasisSparse: copy and modifed from the dense version
   nd        - directions of plane waves
   epsilon   - threshold for filtering
   *C        - created (if C!=PETSC_NULL) Mat with columns restricted coarse basis
   *CO       - created (if CO!=PETSC_NULL) Mat with columns coarse basis support on
               overlapping subdomains
   TODO: 
         Do we need to output ncs?
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DDSetupCoarseBasisSparse"
PetscErrorCode DDSetupCoarseBasisSparse(Mat *C, Mat *CO, PC pcsubs, MyPartition *part, AO ao, PetscInt nd, PetscReal epsilon, SubMatCtx *ctx)
{
  PetscInt nc, max_ncs;  /* number of coarse modes on this proc, and maximum number on subdomains */
  PetscInt  **num_modes=PETSC_NULL;      	/* number of modes on each edge of each subdomain */
  PetscInt i, j, jC, istart, iend, jstart, jend, *ii=PETSC_NULL, *iC=PETSC_NULL, *iiO=PETSC_NULL, *iCO=PETSC_NULL, l, *iedge=PETSC_NULL, ledge, dim=0, dimO=0;
  Vec f;			/* rhs for one subdomain problem */
  KSP *subksp=PETSC_NULL;	/* subdomains' ksp */
  PetscErrorCode ierr;
  PetscScalar ***edge_modes=PETSC_NULL, *fi=PETSC_NULL, *fO=PETSC_NULL;

  PetscFunctionBegin;

  if (!C && !CO) {
    PetscPrintf(PETSC_COMM_WORLD,"DDSetupCoarseBasisSparse: nothing to do because PETSC_NULL are found for both outputs\n");
    PetscFunctionReturn(0);
  }

  /* 
     Get edge modes and calculate nc-- number of coarse basis on this processor 
  */
  CoarseGetAllEdgesModes(&edge_modes, &num_modes, &nc, &max_ncs, nd, epsilon, part, ctx);

  /* 
     Initialize the coarse basis matrices C and CO 
  */
  if (C) {
    ierr= MatCreateAIJ(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,max_ncs,PETSC_NULL,0,PETSC_NULL,C);CHKERRQ(ierr);
    ierr= MatGetOwnershipRange(*C,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*C,&jstart,&jend);CHKERRQ(ierr);
    /* printf("C: istart= %d, iend= %d\n",istart,iend); */
    /* printf("C: jstart= %d, jend= %d\n",jstart,jend); */
  }
  if (CO) {
    ierr= MatCreateAIJ(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,4*max_ncs,PETSC_NULL,3*max_ncs,PETSC_NULL,CO);CHKERRQ(ierr); /* assume one node belongs to at most 4 overlapping subdomains */
    ierr= MatGetOwnershipRange(*CO,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*CO,&jstart,&jend);CHKERRQ(ierr);
    /* printf("CO: istart= %d, iend= %d\n",istart,iend); */
    /* printf("CO: jstart= %d, jend= %d\n",jstart,jend); */
  }
  jC= jstart;  			/* start column of C and CO */


  /* 
     Extend edge modes in subdomains and insert restricted results to C 
  */
  ierr= PCASMGetSubKSP(pcsubs,PETSC_NULL,PETSC_NULL,&subksp);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    ierr= VecCreateSeq(PETSC_COMM_SELF,part->dimso[i],&f);CHKERRQ(ierr);
    if (CO) ExtensionGetIndices(&ii, &iC, &dim, &iiO, &iCO, &dimO, i, part, ao, ctx->ltog[i]);
    else ExtensionGetIndices(&ii, &iC, &dim, PETSC_NULL, PETSC_NULL, PETSC_NULL, i, part, ao, ctx->ltog[i]);
    ierr= PetscMalloc(dim*sizeof(PetscScalar),&fi);CHKERRQ(ierr);
    if(CO) {ierr= PetscMalloc(dimO*sizeof(PetscScalar),&fO);CHKERRQ(ierr);}


    /* modes on left edge */  
    ledge= part->dimyso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= l;
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    for (j = 0; j < num_modes[i][0]; ++j) {
      CoarseExtendFromEdgeSparse(C, CO, jC, subksp[i], f, edge_modes[i][0]+j*ledge, iedge, ledge, ii, iC, dim, fi, iiO, iCO, dimO, fO);
      jC++;
    }
    PetscFree(iedge);

    /* modes on right edge */
    ledge= part->dimyso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= (part->dimxso[i]-1)*(part->dimyso[i]) + l;
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    for (j = 0; j < num_modes[i][1]; ++j) {
      CoarseExtendFromEdgeSparse(C, CO, jC, subksp[i], f, edge_modes[i][1]+j*ledge, iedge, ledge, ii, iC, dim, fi, iiO, iCO, dimO, fO);
      jC++;
    }
    PetscFree(iedge);

    /* modes on bottom edge */
    ledge= part->dimxso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= l*(part->dimyso[i]); /* natural numbering in this subdomain, corresponds to edge_mode[i][2] */
    }
    ierr= ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);CHKERRQ(ierr);
    for (j = 0; j < num_modes[i][2]; ++j) {
      CoarseExtendFromEdgeSparse(C, CO, jC, subksp[i], f, edge_modes[i][2]+j*ledge, iedge, ledge, ii, iC, dim, fi, iiO, iCO, dimO, fO);
      jC++;
    }
    PetscFree(iedge);

    /* modes on top edge */
    ledge= part->dimxso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= (part->dimyso[i]-1) + l*(part->dimyso[i]);
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    for (j = 0; j < num_modes[i][3]; ++j) {
      CoarseExtendFromEdgeSparse(C, CO, jC, subksp[i], f, edge_modes[i][3]+j*ledge, iedge, ledge, ii, iC, dim, fi, iiO, iCO, dimO, fO);
      jC++;
    }
    PetscFree(iedge);

    /* after all edges */
    ierr= VecDestroy(&f);CHKERRQ(ierr);
    ierr= PetscFree(ii);CHKERRQ(ierr);  ierr= PetscFree(iC);CHKERRQ(ierr);  ierr= PetscFree(fi);CHKERRQ(ierr);
    if (CO) { ierr= PetscFree(iiO);CHKERRQ(ierr);  ierr= PetscFree(iCO);CHKERRQ(ierr); ierr= PetscFree(fO);CHKERRQ(ierr);}
  }

  if (C) { MatAssemblyBegin(*C,MAT_FINAL_ASSEMBLY);  MatAssemblyEnd(*C,MAT_FINAL_ASSEMBLY); }
  if (CO) { MatAssemblyBegin(*CO,MAT_FINAL_ASSEMBLY); MatAssemblyEnd(*CO,MAT_FINAL_ASSEMBLY); }
  /* DEBUG: */
  /* if (C) MatView(*C,PETSC_VIEWER_STDOUT_WORLD); */
  /* if (CO) MatView(*CO,PETSC_VIEWER_STDOUT_WORLD); */

  /* free space */
  for (i = 0; i < part->s; ++i) {
    PetscFree(num_modes[i]); 
    for (j = 0; j < 4; ++j) {
      PetscFree(edge_modes[i][j]);
    }
    PetscFree(edge_modes[i]);
  }
  PetscFree(num_modes); PetscFree(edge_modes);
  PetscPrintf(PETSC_COMM_WORLD,"DDSetupCoarseBasisSparse: I'm done!\n");
  PetscFunctionReturn(0);  
}


/* ----------------------------------------------------------------------- 
   C           -  the restricted coarse matrix C 
   CO          -  the matrix CO for overlapping coarse basis
   jC          -  the column to be written in C and CO
  ksp          -  the subdomain solver
  f            -  rhs for the subdomain solver 'ksp'
  edge_mode    -  edge mode
  iedge        -  locations of edge mode in f, f[iedge[l]]= edge_mode[l]
  ledge        -  length of iedge 
  ii,iC,dim,fi -  restricted extension of solution (stored in f) to C
  iiO,iCO,dimO,fO -  for overlap region		  
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "CoarseExtendFromEdgeSparse"
PetscErrorCode CoarseExtendFromEdgeSparse(Mat *C, Mat *CO, PetscInt jC, KSP ksp, Vec f, PetscScalar *edge_mode, PetscInt *iedge, PetscInt ledge, PetscInt *ii, PetscInt *iC, PetscInt dim, PetscScalar *fi, PetscInt *iiO, PetscInt *iCO, PetscInt dimO, PetscScalar *fO)
{
  PetscInt    l;
  PetscScalar *fa;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  /* add edge mode to rhs and solve the subdomain problem */
  ierr= VecZeroEntries(f);CHKERRQ(ierr);
  ierr= VecGetArray(f,&fa);CHKERRQ(ierr);
  for (l = 0; l < ledge; ++l) {
    fa[iedge[l]]= edge_mode[l];
   }
  ierr= VecRestoreArray(f,&fa);
  
  /* DEBUG: */
  /* ierr= VecView(f,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* Mat Amat, Pmat; */
  /* MatStructure flag; */
  /* ierr= KSPGetOperators(ksp,&Amat,&Pmat,&flag);CHKERRQ(ierr); */
  /* ierr= MatView(Amat,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  
  ierr= KSPSolve(ksp,f,f);CHKERRQ(ierr);

  /* DEBUG: */
  /* ierr= VecView(f,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */

  /* copy the restricted part of f to a column of C
       this could be done by restricted extension if it is available */
  /* printf("CoarseExtendFromEdgeDense: I'm here!\n"); */
  ierr= VecGetArray(f,&fa);CHKERRQ(ierr);
  for(l = 0; l < dim; ++l){
    fi[l]= fa[ii[l]];
  }
  if (C) {
    MatSetValues(*C,dim,iC,1,&jC,fi,INSERT_VALUES);
  } 
  if (CO) {
    MatSetValues(*CO,dim,iC,1,&jC,fi,INSERT_VALUES);
    for (l = 0; l < dimO; ++l) {
      fO[l]= fa[iiO[l]];
    }
    MatSetValues(*CO,dimO,iCO,1,&jC,fO,INSERT_VALUES);
  }
  ierr= VecRestoreArray(f,&fa);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


/* ----------------------------------------------------------------------- 
   DDSetupCoarseBasis: copy and modifed from the ~Sparse, in ~Sparse extension is 
     edge-by-edge, here we do it for boundary of each subdomain
   nd        - directions of plane waves
   epsilon   - threshold for filtering
   *C        - created (if C!=PETSC_NULL) Mat with columns restricted coarse basis
   *CO       - created (if CO!=PETSC_NULL) Mat with columns coarse basis support on
               overlapping subdomains
  TODO: complete this function
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DDSetupCoarseBasis"
PetscErrorCode DDSetupCoarseBasis(Mat *C, Mat *CO, PC pcsubs, MyPartition *part, AO ao, PetscInt nd, PetscReal epsilon, SubMatCtx *ctx)
{
  PetscScalar **modes, *fi, *fO; 
  PetscInt *num_modes, **inodes, *nnodes, *bctype, i, nc, maxncs, istart, iend, jstart, jend, jC, dim, dimO=0, *ii, *iC, *iiO, *iCO, j;
  PetscErrorCode ierr;
  Vec f;
  KSP *subksp;

  PetscFunctionBegin;

  /* 
     Get boundary modes
  */
  PetscMalloc(part->s*sizeof(PetscScalar *),&modes);
  PetscMalloc(part->s*sizeof(PetscInt),&num_modes);
  PetscMalloc(part->s*sizeof(PetscInt *),&inodes);
  PetscMalloc(part->s*sizeof(PetscInt),&nnodes);
  ierr= PetscMalloc(4*sizeof(PetscInt),&bctype);CHKERRQ(ierr);
  maxncs= 0;  nc= 0;
  for (i = 0; i < part->s; ++i) {
    bctype[0]= (part->startxo[i]) ? -2 : ctx->bctype[0]; 
    bctype[1]= (part->endxo[i] < (part->nx)+1) ? -2 : ctx->bctype[1];  
    bctype[2]= (part->startyo[i]) ? -2 : ctx->bctype[2];
    bctype[3]= (part->endyo[i] < (part->ny)+1) ? -2 : ctx->bctype[3];
    CoarseGetBoundaryModes(modes+i, num_modes+i, inodes+i, nnodes+i, bctype, part->dimxso[i]-1, part->dimyso[i]-1, ctx->hx, ctx->hy, nd, ctx->k_coef, epsilon);
    if (maxncs<num_modes[i]) maxncs= num_modes[i];   
    nc+= num_modes[i];
  }
  
  /* 
     Initialize the coarse basis matrices C and CO 
  */
  if (C) {
    ierr= MatCreateAIJ(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,maxncs,PETSC_NULL,0,PETSC_NULL,C);CHKERRQ(ierr);
    ierr= MatGetOwnershipRange(*C,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*C,&jstart,&jend);CHKERRQ(ierr);
    printf("C: istart= %d, iend= %d\n",istart,iend);
    printf("C: jstart= %d, jend= %d\n",jstart,jend);
  }
  if (CO) {
    ierr= MatCreateAIJ(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,4*maxncs,PETSC_NULL,3*maxncs,PETSC_NULL,CO);CHKERRQ(ierr); /* assume one node belongs to at most 4 overlapping subdomains */
    ierr= MatGetOwnershipRange(*CO,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*CO,&jstart,&jend);CHKERRQ(ierr);
    printf("CO: istart= %d, iend= %d\n",istart,iend);
    printf("CO: jstart= %d, jend= %d\n",jstart,jend);
  }
  jC= jstart;  			/* start column of C and CO */

  /* 
     Extend boundary modes in subdomains and insert restricted results to C 
  */
  ierr= PCASMGetSubKSP(pcsubs,PETSC_NULL,PETSC_NULL,&subksp);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    /* rhs for subdomain problem */
    ierr= VecCreateSeq(PETSC_COMM_SELF,part->dimso[i],&f);CHKERRQ(ierr);
    if (CO) ExtensionGetIndices(&ii, &iC, &dim, &iiO, &iCO, &dimO, i, part, ao, ctx->ltog[i]);
    else ExtensionGetIndices(&ii, &iC, &dim, PETSC_NULL, PETSC_NULL, PETSC_NULL, i, part, ao, ctx->ltog[i]);
    ierr= PetscMalloc(dim*sizeof(PetscScalar),&fi);CHKERRQ(ierr);
    if(CO) {ierr= PetscMalloc(dimO*sizeof(PetscScalar),&fO);CHKERRQ(ierr);}

    /* DEBUG: */
    {
      /* printf("inodes before map\n"); */
      /* for (l = 0; l < nnodes[i]; ++l) { */
      /*   printf("%d  ",inodes[i][l]); */
      /* } */
      /* printf("\n"); */
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],nnodes[i],inodes[i],inodes[i]);
    /* DEBUG: */
    {
      /* printf("inodes after map\n"); */
      /* for (l = 0; l < nnodes[i]; ++l) { */
      /*   printf("%d  ",inodes[i][l]); */
      /* } */
      /* printf("\n"); */
    }
    for (j = 0; j < num_modes[i]; ++j) {
      CoarseExtendFromEdgeSparse(C, CO, jC, subksp[i], f, modes[i]+j*nnodes[i], inodes[i], nnodes[i], ii, iC, dim, fi, iiO, iCO, dimO, fO);
      jC++;
    }
    ierr= VecDestroy(&f);CHKERRQ(ierr);
    ierr= PetscFree(ii);CHKERRQ(ierr);  ierr= PetscFree(iC);CHKERRQ(ierr);  ierr= PetscFree(fi);CHKERRQ(ierr);
    if (CO) {ierr= PetscFree(iiO);CHKERRQ(ierr);  ierr= PetscFree(iCO);CHKERRQ(ierr); ierr= PetscFree(fO);CHKERRQ(ierr);}
  }
  if (C) { MatAssemblyBegin(*C,MAT_FINAL_ASSEMBLY);  MatAssemblyEnd(*C,MAT_FINAL_ASSEMBLY); }
  if (CO) { MatAssemblyBegin(*CO,MAT_FINAL_ASSEMBLY); MatAssemblyEnd(*CO,MAT_FINAL_ASSEMBLY); }
  /* DEBUG: */
  /* if (C) MatView(*C,PETSC_VIEWER_STDOUT_WORLD); */
  /* if (CO) MatView(*CO,PETSC_VIEWER_STDOUT_WORLD); */

  /* 
     Free work space 
  */
  for (i = 0; i < part->s; ++i) {
    PetscFree(modes[i]); PetscFree(inodes[i]);
  }
  PetscFree(modes); PetscFree(num_modes); PetscFree(inodes); PetscFree(nnodes); PetscFree(bctype);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoarseGetBoundaryModes"
/* ----------------------------------------------------------------------- 
   *inodes:     1D indices of inner boundary nodes, in first y then x ordering 
   *nnodes:     length of each mode, i.e. number of free d.o.f. on innner boundary
   bctype:      for left, right, bottom, top edges, >0 for exterior boundary, <0 for innner boundary
   nx, ny:  number of elements along x and y directions
   hx, hy:  mesh sizes
   nd:  number of directions of plane waves
   The difference from EdgesModes is that the filtering is done for the 
   mode on boundary but not only for an edge.
   ----------------------------------------------------------------------- */
PetscErrorCode CoarseGetBoundaryModes(PetscScalar **modes, PetscInt *num_modes, PetscInt **inodes, PetscInt *nnodes, PetscInt bctype[], PetscInt nx, PetscInt ny, PetscReal hx, PetscReal hy, PetscInt nd, PetscReal k_coef, PetscReal epsilon)
{
  PetscInt       i, j, *jpvt, info, *il, *ir, m, n, count, *pass;
  PetscErrorCode ierr;
  PetscScalar    *a, *tau;
  PetscReal      t, dt, lx, ly;

  PetscFunctionBegin;
  
  /* count number of d.o.f. on inner boundary */ 
  ierr= PetscMalloc(4*sizeof(PetscInt),&il);CHKERRQ(ierr);
  ierr= PetscMalloc(4*sizeof(PetscInt),&ir);CHKERRQ(ierr);
  m= 0;
  if (bctype[0] < 0) {
    il[0]= (bctype[2]==2); ir[0]= ny + (bctype[3]<2); 
    m+= ir[0] - il[0]; 
  }
  if (bctype[1] < 0) {
    il[1]= (bctype[2]==2); ir[1]= ny + (bctype[3]<2);
    m+= ir[1] - il[1];  
  }
  if (bctype[2]<0) {
    il[2]= (bctype[0]==2 || bctype[0]<0); ir[2]= nx + (bctype[1]<2 && bctype[1]>0);
    m+= ir[2] - il[2];
  } 
  if (bctype[3]<0) {
    il[3]= (bctype[0]==2 || bctype[0]<0); ir[3]= nx + (bctype[1]<2 && bctype[1]>0);
    m+= ir[3] - il[3];
  }
  (*nnodes)= m;
  
  /* set indices of inner boundary nodes, in local natural order */
  PetscMalloc(m*sizeof(PetscScalar),inodes);
  m= 0;
  if (bctype[0] < 0) {
    for (i = il[0]; i < ir[0]; ++i) {
      (*inodes)[m]= i;  m++;
    }
  }
  if (bctype[1] < 0) {
    for (i = il[1]; i < ir[1]; ++i) {
      (*inodes)[m]= i + nx*(ny+1);  m++;
    }
  }
  if (bctype[2] < 0) {
    for (i = il[2]; i < ir[2]; ++i) {
      (*inodes)[m]= i*(ny+1);  m++;
    }
  }
  if (bctype[3] < 0) {
    for (i = il[3]; i < ir[3]; ++i) {
      (*inodes)[m]= i*(ny+1) + ny;  m++;
    }
  }
  if (m-(*nnodes)) printf("CoarseGetBoundaryModes: wrong here!\n");


  /* initial modes, use traces of plane waves exp(ik lx x + ik ly y) */
  ierr= PetscMalloc(2*nd*m*sizeof(PetscScalar),&a);CHKERRQ(ierr);
  ierr= PetscMemzero(a, 2*nd*m*sizeof(PetscScalar));CHKERRQ(ierr);
  dt= PETSC_PI/nd;   n= 0;
  for (t = 0; t < PETSC_PI; t+=dt) { /* for each angle */
    lx= cos(t);  ly= sin(t);
    count= 0;  			/* location in one column */
    if (bctype[0]<0) {
      for (i = il[0]; i < ir[0]; ++i) {
	a[n*m+count]= exp(PETSC_i*k_coef*ly*i*hy);
	a[(n+1)*m+count]= conj(a[n*m+count]);
	count++;
      }
    }
    if (bctype[1]<0) {
      for (i = il[1]; i < ir[1]; ++i) {
	a[n*m+count]= exp(PETSC_i*k_coef*ly*i*hy + PETSC_i*k_coef*lx*nx*hx);
	a[(n+1)*m+count]= conj(a[n*m+count]);
	count++;
      }
    }
    if (bctype[2]<0) {
      for (i = il[2]; i < ir[2]; ++i) {
	a[n*m+count]= exp(PETSC_i*k_coef*lx*i*hx);
	a[(n+1)*m+count]= conj(a[n*m+count]);
	count++;
      }
    }
    if (bctype[3]<0) {
      for (i = il[3]; i < ir[3]; ++i) {
	a[n*m+count]= exp(PETSC_i*k_coef*lx*i*hx + PETSC_i*k_coef*ly*ny*hy);
	a[(n+1)*m+count]= conj(a[n*m+count]);
	count++;
      }
    }
    n+= 2;
  }

  /* DEBUG: */
  {
    /* printf("a: before qr \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }

  /* filtering, currently use laplacke (sequential) */
  ierr= PetscMalloc(n*sizeof(PetscInt),&jpvt);CHKERRQ(ierr);
  ierr= PetscMalloc(n*sizeof(PetscScalar),&tau);CHKERRQ(ierr);
  info= LAPACKE_zgeqp3(LAPACK_COL_MAJOR, m, n, a, m, jpvt, tau);
  
  /* DEBUG: */
  {
    /* printf("a: after qr \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }
  
  n= (m<n)?m:n; 		/* number of columns of Q */
  ierr= PetscMalloc(n*sizeof(PetscInt),&pass);CHKERRQ(ierr);
  (*num_modes)= 0;
  for (j = 0; j < n; ++j) {
    pass[j]= (PetscAbsScalar(a[j*m+j])>epsilon) ? 1 : 0;
    /* printf("abs(a) = %g", PetscAbsScalar(a[j*m+j])); */
    if (pass[j]) (*num_modes)++;
  }
  info= LAPACKE_zungqr(LAPACK_COL_MAJOR, m, n, n, a, m, tau);

  /* DEBUG: */
  {
    /* printf("a: got q \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < n; ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart(a[j*m+i]),PetscImaginaryPart(a[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }

  /* copy passed modes to the output 'modes' */
  ierr= PetscMalloc((*num_modes)*m*sizeof(PetscScalar),modes);CHKERRQ(ierr);
  i= 0;
  for (j = 0; j < n; ++j) {
    if (pass[j]) {
      PetscMemcpy((*modes)+i*m,a+j*m,m*sizeof(PetscScalar));
      i++;
    }
  } 
  
  /* DEBUG: */
  {
    /* printf("filtered modes: \n"); */
    /* for (i = 0; i < m; ++i) { */
    /*   for (j = 0; j < (*num_modes); ++j) { */
    /* 	printf("%f + %f i   ",PetscRealPart((*modes)[j*m+i]),PetscImaginaryPart((*modes)[j*m+i])); */
    /*   } */
    /*   printf("\n"); */
    /* } */
  }

  PetscFree(il); PetscFree(ir); PetscFree(a); PetscFree(jpvt); PetscFree(tau); PetscFree(pass);
  PetscFunctionReturn(0);
}

#undef __FUNCT__ 
#define __FUNCT__ "CoarseGetAllEdgesModes"
PetscErrorCode CoarseGetAllEdgesModes(PetscScalar ****edge_modes, PetscInt ***num_modes, PetscInt *nc, PetscInt *max_ncs, PetscInt nd, PetscReal epsilon, MyPartition *part, SubMatCtx *ctx)
{
  PetscInt nedges= 4, i, *ncs;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  (*nc)= 0;  (*max_ncs)= 0;
  ierr= PetscMalloc(part->s*sizeof(PetscInt), &ncs);CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt*),num_modes);CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscScalar**),edge_modes);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    ncs[i]= 0;
    ierr= PetscMalloc(nedges*sizeof(PetscScalar*),(*edge_modes)+i);CHKERRQ(ierr);
    ierr= PetscMalloc(nedges*sizeof(PetscInt),(*num_modes)+i);CHKERRQ(ierr);

    /* left edge: 0 */
    if (part->elem_startx + part->selem_startx[i]) {
      CoarseGetEdgeModes((*edge_modes)[i],(*num_modes)[i],part->dimyso[i]-1,ctx->hy,nd,ctx->k_coef,epsilon,1);
    printf("num_modes[%d][0]= %d\n",i,(*num_modes)[i][0]);
    }
    else {
      (*num_modes)[i][0]= 0;  (*edge_modes)[i][0]= PETSC_NULL;
    }
  ncs[i]+= (*num_modes)[i][0];
        
    /* right edge: 1 */
    if (part->elem_startx + part->selem_endx[i] - part->nx) {
      CoarseGetEdgeModes((*edge_modes)[i]+1,(*num_modes)[i]+1,part->dimyso[i]-1,ctx->hy,nd,ctx->k_coef,epsilon,1);
      printf("num_modes[%d][1]= %d\n",i,(*num_modes)[i][1]);
    }
    else {
      (*num_modes)[i][1]= 0;  (*edge_modes)[i][1]= PETSC_NULL;
    }
    ncs[i]+= (*num_modes)[i][1];

    /* bottom edge: 2 */
    if (part->elem_starty + part->selem_starty[i]) {
      CoarseGetEdgeModes((*edge_modes)[i]+2,(*num_modes)[i]+2,part->dimxso[i]-1,ctx->hx,nd,ctx->k_coef,epsilon,0);
      printf("num_modes[%d][2]= %d\n",i,(*num_modes)[i][2]);
    }
    else {
      (*num_modes)[i][2]= 0;  (*edge_modes)[i][2]= PETSC_NULL;
    }
    ncs[i]+= (*num_modes)[i][2];

    /* top edge: 3 */
    if (part->elem_starty + part->selem_endy[i] - part->ny) {
      CoarseGetEdgeModes((*edge_modes)[i]+3,(*num_modes)[i]+3,part->dimxso[i]-1,ctx->hx,nd,ctx->k_coef,epsilon,0);
      printf("num_modes[%d][3]= %d\n",i,(*num_modes)[i][3]);
    }
    else {
      (*num_modes)[i][3]= 0;  (*edge_modes)[i][3]= PETSC_NULL;
    }
    ncs[i]+= (*num_modes)[i][3];

    if (ncs[i]>(*max_ncs)) {
      (*max_ncs)= ncs[i];
    }
    (*nc)+= ncs[i];
  }

  PetscFree(ncs);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ExtensionGetIndices"
/* ----------------------------------------------------------------------- 
   i   -   the subdomain number
   ----------------------------------------------------------------------- */
PetscErrorCode ExtensionGetIndices(PetscInt **ii, PetscInt **iC, PetscInt *dim, PetscInt **iiO, PetscInt **iCO, PetscInt *dimO, PetscInt i, MyPartition *part, AO ao, ISLocalToGlobalMapping ltog)
{
  PetscInt count, l, m;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  
  /* build two arrays of indices for restricted extension, 
     ii  - restriction of f
     iC  - restriction of C,  global indices 
     These are also needed for CO even when C is empty!
  */
  (*dim)= part->dims[i];
  ierr= PetscMalloc((*dim)*sizeof(PetscInt),ii);CHKERRQ(ierr);
  ierr= PetscMalloc((*dim)*sizeof(PetscInt),iC);CHKERRQ(ierr);
  count= 0;
  for (l = part->startxo[i]; l < part->endxo[i]; ++l) {
    if (l < part->elem_startx+part->selem_startx[i] || l >= part->elem_startx+part->selem_startx[i]+part->dimxs[i]) {
      continue;
    }
    for (m = part->startyo[i]; m < part->endyo[i]; ++m) {
      if (m < part->elem_starty+part->selem_starty[i] || m >= part->elem_starty+part->selem_starty[i]+part->dimys[i]) {
	continue;
      }
      (*iC)[count]= l*(part->ny+1) + m;
      (*ii)[count]= (l-part->startxo[i])*(part->dimyso[i]) + m-part->startyo[i];
      count++;
    }
  }
  if (count - (*dim)) printf("ExtensionGetIndices: something wrong here!");
  ierr= AOApplicationToPetsc(ao,(*dim),(*iC));CHKERRQ(ierr);
  /* DEBUG: */
  {
    /* printf("(*ii) before map\n");  */
    /* for (l = 0; l < (*dim); ++l) { */
    /*   printf("%d  ",(*ii)[l]); */
    /* }  */
    /* printf("\n"); */
  }
  ierr= ISLocalToGlobalMappingApply(ltog,(*dim),(*ii),(*ii));CHKERRQ(ierr);
  /* DEBUG: */
  {
    /* printf("(*ii) after map\n"); */
    /* for (l = 0; l < (*dim); ++l) { */
    /*   printf("%d  ",(*ii)[l]); */
    /* }  */
    /* printf("\n"); */
  }
    
    
  /* build two arrays of indices for extension from overlapping region, excluding non-overlapping subdomain 
     iiO  - overlap part of f
     iCO  - overlap part of C,  global indices
  */
  if (iiO && iCO) {
    (*dimO)= part->dimso[i]-part->dims[i];
    ierr= PetscMalloc((*dimO)*sizeof(PetscInt),iiO);CHKERRQ(ierr);
    ierr= PetscMalloc((*dimO)*sizeof(PetscInt),iCO);CHKERRQ(ierr);
    count= 0;
    for (l = part->startxo[i]; l < part->endxo[i]; ++l) {
      for (m = part->startyo[i]; m < part->endyo[i]; ++m) {
	if ((l >= part->elem_startx+part->selem_startx[i]) && (l < part->elem_startx+part->selem_startx[i]+part->dimxs[i]) && (m >= part->elem_starty+part->selem_starty[i]) && (m < part->elem_starty+part->selem_starty[i]+part->dimys[i])) {
	  continue;
	}
	(*iCO)[count]= l*(part->ny+1) + m;
	(*iiO)[count]= (l-part->startxo[i])*(part->dimyso[i]) + m-part->startyo[i];
	count++;
      }
    }
    if (count - (*dimO)) printf("DDSetupCoarseBasisSparse: something wrong here 2!\n");
    ierr= AOApplicationToPetsc(ao,(*dimO),(*iCO));CHKERRQ(ierr);
    /* DEBUG: */
    {
      /* printf("iiO before map\n");  */
      /* for (l = 0; l < (*dimO); ++l) { */
      /*   printf("%d  ",(*iiO)[l]); */
      /* }  */
      /* printf("\n"); */
    }
    ierr= ISLocalToGlobalMappingApply(ltog,(*dimO),(*iiO),(*iiO));CHKERRQ(ierr);
    /* DEBUG: */
    {
      /* printf("iiO after map\n"); */
      /* for (l = 0; l < (*dimO); ++l) { */
      /*   printf("%d  ",(*iiO)[l]); */
      /* }  */
      /* printf("\n"); */
    } 
  }

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "myFindInSorted"
static PetscInt myFindInSorted(PetscInt value, const PetscInt *array, PetscInt length)
{
  PetscInt left, right, i;
  left= 0; right= length - 1;
  
  while (right-left) {
    i= (right+left)/2;
    if (value < array[i]) {
      right= i;
    }
    else if (value > array[i]) {
      left= i + 1;
    }
    else {
      return i;
    }
  }
  
  return left;
}


/* ----------------------------------------------------------------------- 
   Legacy codes: begin
   ----------------------------------------------------------------------- */
#if 0

/* ----------------------------------------------------------------------- 
   DDSetupCoarseBasisDense
   Limitation: in petsc, dense matrix can only be multiplied with dense(but not sparse) matrix.
   nd        - directions of plane waves
   epsilon   - threshold for filtering
   *C        - created (if C!=NULL) Mat with columns restricted coarse basis
   *CO       - created (if CO!=NULL) Mat with columns coarse basis support on
               overlapping subdomains
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DDSetupCoarseBasisDense"
PetscErrorCode DDSetupCoarseBasisDense(Mat *C, Mat *CO, PC pcsubs, MyPartition *part, AO ao, PetscInt nd, PetscReal epsilon, SubMatCtx *ctx)
{
  /* const PetscInt nedges= 4;	/\* four edges per subdomain: 0 left, 1 right, 2 bottom, 3 top *\/ */
  PetscInt nc, max_ncs /* , *ncs=PETSC_NULL */; /* number of coarse modes on this proc. or subdomain */
  PetscInt  **num_modes=PETSC_NULL;      	/* number of modes on each edge of each subdomain */
  PetscInt i, j, jC, istart, iend, jstart, jend, *ii=PETSC_NULL, *iC=PETSC_NULL, *iiO=PETSC_NULL, *iCO=PETSC_NULL, l, m, count, *iedge=PETSC_NULL, ledge, dim=0, dimO=0;
  Vec f;			/* rhs for one subdomain problem */
  KSP *subksp=PETSC_NULL;	/* subdomains' ksp */
  PetscErrorCode ierr;
  PetscScalar *Ca= PETSC_NULL, ***edge_modes=PETSC_NULL, *fO=PETSC_NULL;

  PetscFunctionBegin;

  if (!C && !CO) {
    PetscPrintf(PETSC_COMM_WORLD,"DDSetupCoarseBasisDense: nothing to do because PETSC_NULL are found for both outputs\n");
    PetscFunctionReturn(0);
  }

  /* 
     Get edge modes and calculate nc-- number of coarse basis on this processor 
  */  
  CoarseGetAllEdgesModes(&edge_modes, &num_modes, &nc, &max_ncs, nd, epsilon, part, ctx);

  
  /* 
     Initialize the coarse basis matrices C and CO 
  */
  if (C) {
    ierr= MatCreateDense(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,PETSC_NULL,C);CHKERRQ(ierr);
    ierr= MatGetOwnershipRange(*C,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*C,&jstart,&jend);CHKERRQ(ierr);
    printf("C: istart= %d, iend= %d\n",istart,iend);
    printf("C: jstart= %d, jend= %d\n",jstart,jend);
    ierr= MatGetArray(*C,&Ca);CHKERRQ(ierr);
  }
  if (CO) {
    ierr= MatCreateDense(PETSC_COMM_WORLD,part->dimp,nc,PETSC_DECIDE,PETSC_DECIDE,PETSC_NULL,CO);CHKERRQ(ierr);
    ierr= MatGetOwnershipRange(*CO,&istart,&iend);CHKERRQ(ierr);
    ierr= MatGetOwnershipRangeColumn(*CO,&jstart,&jend);CHKERRQ(ierr);
    printf("CO: istart= %d, iend= %d\n",istart,iend);
    printf("CO: jstart= %d, jend= %d\n",jstart,jend);
  }
  jC= jstart;  			/* start column of C and CO */


  /* 
     Extend edge modes in subdomains and insert restricted results to C 
  */
  ierr= PCASMGetSubKSP(pcsubs,PETSC_NULL,PETSC_NULL,&subksp);CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    /* rhs for subdomain problem */
    ierr= VecCreateSeq(PETSC_COMM_SELF,part->dimso[i],&f);CHKERRQ(ierr);

    /* build two arrays of indices for restricted extension, 
       ii  - restriction of f
       iC  - restriction of C, indices in local array
       These are also needed for CO even when C is empty!
    */
    dim= part->dims[i];
    ierr= PetscMalloc(dim*sizeof(PetscInt),&ii);CHKERRQ(ierr);
    ierr= PetscMalloc(dim*sizeof(PetscInt),&iC);CHKERRQ(ierr);
    count= 0;
    for (l = part->startxo[i]; l < part->endxo[i]; ++l) {
      if (l < part->elem_startx+part->selem_startx[i] || l >= part->elem_startx+part->selem_startx[i]+part->dimxs[i]) {
	continue;
      }
      for (m = part->startyo[i]; m < part->endyo[i]; ++m) {
	if (m < part->elem_starty+part->selem_starty[i] || m >= part->elem_starty+part->selem_starty[i]+part->dimys[i]) {
	  continue;
	}
	iC[count]= l*(part->ny+1) + m;
	ii[count]= (l-part->startxo[i])*(part->dimyso[i]) + m-part->startyo[i];
	count++;
      }
    }
    if (count - dim) printf("DDSetupCoarseBasisDense: something wrong here!");
    ierr= AOApplicationToPetsc(ao,dim,iC);CHKERRQ(ierr);
    for (l = 0; l < dim; ++l) {
      iC[l]-= istart;  /* MatGetArray gives only local rows on this proc. */
    } 
    /* DEBUG: */
    {
      /* printf("ii before map\n");  */
      /* for (l = 0; l < dim; ++l) { */
      /*   printf("%d  ",ii[l]); */
      /* }  */
      /* printf("\n"); */
    }
    ierr= ISLocalToGlobalMappingApply(ctx->ltog[i],dim,ii,ii);CHKERRQ(ierr);
    /* DEBUG: */
    {
      /* printf("ii after map\n"); */
      /* for (l = 0; l < dim; ++l) { */
      /*   printf("%d  ",ii[l]); */
      /* }  */
      /* printf("\n"); */
    }
    
    
    /* build two arrays of indices for extension from overlapping region, excluding non-overlapping subdomain 
       iiO  - overlap part of f
       iCO  - overlap part of C, global indices
    */
    if (CO) {
      dimO= part->dimso[i]-part->dims[i];
      ierr= PetscMalloc(dimO*sizeof(PetscInt),&iiO);CHKERRQ(ierr);
      ierr= PetscMalloc(dimO*sizeof(PetscInt),&iCO);CHKERRQ(ierr);
      ierr= PetscMalloc(dimO*sizeof(PetscScalar),&fO);CHKERRQ(ierr);
      count= 0;
      for (l = part->startxo[i]; l < part->endxo[i]; ++l) {
	for (m = part->startyo[i]; m < part->endyo[i]; ++m) {
	  if ((l >= part->elem_startx+part->selem_startx[i]) && (l < part->elem_startx+part->selem_startx[i]+part->dimxs[i]) && (m >= part->elem_starty+part->selem_starty[i]) && (m < part->elem_starty+part->selem_starty[i]+part->dimys[i])) {
	    continue;
	  }
	  iCO[count]= l*(part->ny+1) + m;
	  iiO[count]= (l-part->startxo[i])*(part->dimyso[i]) + m-part->startyo[i];
	  count++;
	}
      }
      if (count - dimO) printf("DDSetupCoarseBasisDense: something wrong here 2!\n");
      ierr= AOApplicationToPetsc(ao,dimO,iCO);CHKERRQ(ierr);
      /* DEBUG: */
      {
	/* printf("iiO before map\n");  */
	/* for (l = 0; l < dimO; ++l) { */
	/*   printf("%d  ",iiO[l]); */
	/* }  */
	/* printf("\n"); */
      }
      ierr= ISLocalToGlobalMappingApply(ctx->ltog[i],dimO,iiO,iiO);CHKERRQ(ierr);
      /* DEBUG: */
      {
	/* printf("iiO after map\n"); */
	/* for (l = 0; l < dimO; ++l) { */
	/*   printf("%d  ",iiO[l]); */
	/* }  */
	/* printf("\n"); */
      } 
    }

    
    /* modes on left edge */  
    ledge= part->dimyso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= l;
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    if (C) {
      for (j = 0; j < num_modes[i][0]; ++j) {
	CoarseExtendFromEdgeDense(Ca+jC*(part->dimp), CO, jC*(part->dimp), subksp[i], f, edge_modes[i][0]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    else {
      for (j = 0; j < num_modes[i][0]; ++j) {
	CoarseExtendFromEdgeDense(PETSC_NULL, CO, jC*(part->dimp), subksp[i], f, edge_modes[i][0]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    PetscFree(iedge);

    /* modes on right edge */
    ledge= part->dimyso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= (part->dimxso[i]-1)*(part->dimyso[i]) + l;
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    if (C) {
      for (j = 0; j < num_modes[i][1]; ++j) {
	CoarseExtendFromEdgeDense(Ca+jC*(part->dimp), CO, jC*(part->dimp), subksp[i], f, edge_modes[i][1]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    else {
      for (j = 0; j < num_modes[i][1]; ++j) {
	CoarseExtendFromEdgeDense(PETSC_NULL, CO, jC*(part->dimp), subksp[i], f, edge_modes[i][1]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    PetscFree(iedge);

    /* modes on bottom edge */
    ledge= part->dimxso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= l*(part->dimyso[i]); /* natural numbering in this subdomain, corresponds to edge_mode[i][2] */
    }
    /* DEBUG: */
    {
      /* if (!i && num_modes[i][2]) { */
      /*   printf("(%d) iedge before map\n",i);  */
      /*   for (j = 0; j < ledge; ++j) { */
      /* 	printf("%d  ",iedge[j]); */
      /*   } */
      /*   printf("\n"); */
      /* } */
    }
    ierr= ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);CHKERRQ(ierr);
    /* DEBUG: */
    {
      /* if (!i && num_modes[i][2]) { */
      /*   printf("(%d) iedge after map\n",i);  */
      /*   for (j = 0; j < ledge; ++j) { */
      /* 	printf("%d  ",iedge[j]); */
      /*   } */
      /*   printf("\n"); */
      /* } */
    }
    if (C) {
      for (j = 0; j < num_modes[i][2]; ++j) {
	CoarseExtendFromEdgeDense(Ca+jC*(part->dimp), CO, jC*(part->dimp), subksp[i], f, edge_modes[i][2]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    else {
      for (j = 0; j < num_modes[i][2]; ++j) {
	CoarseExtendFromEdgeDense(PETSC_NULL, CO, jC*(part->dimp), subksp[i], f, edge_modes[i][2]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    PetscFree(iedge);

    /* modes on top edge */
    ledge= part->dimxso[i];
    ierr= PetscMalloc(ledge*sizeof(PetscInt),&iedge);CHKERRQ(ierr);
    for (l = 0; l < ledge; ++l) {
      iedge[l]= (part->dimyso[i]-1) + l*(part->dimyso[i]);
    }
    ISLocalToGlobalMappingApply(ctx->ltog[i],ledge,iedge,iedge);
    if (C) {
      for (j = 0; j < num_modes[i][3]; ++j) {
	CoarseExtendFromEdgeDense(Ca+jC*(part->dimp), CO, jC*(part->dimp), subksp[i], f, edge_modes[i][3]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    else {
      for (j = 0; j < num_modes[i][3]; ++j) {
	CoarseExtendFromEdgeDense(PETSC_NULL, CO, jC*(part->dimp), subksp[i], f, edge_modes[i][3]+j*ledge, iedge, ledge, ii, iC, dim, iiO, iCO, jC, dimO, fO);
	jC++;
      }
    }
    PetscFree(iedge);

    /* after all edges */
    ierr= VecDestroy(&f);CHKERRQ(ierr);
    ierr= PetscFree(ii);CHKERRQ(ierr);  ierr= PetscFree(iC);CHKERRQ(ierr);
    if (CO) { 
      ierr= PetscFree(iiO);CHKERRQ(ierr);  ierr= PetscFree(iCO);CHKERRQ(ierr); ierr= PetscFree(fO);CHKERRQ(ierr); 
    }
  }
  if (C) { ierr= MatRestoreArray(*C,&Ca);CHKERRQ(ierr);  MatAssemblyBegin(*C,MAT_FINAL_ASSEMBLY);  MatAssemblyEnd(*C,MAT_FINAL_ASSEMBLY); }
  if (CO) { MatAssemblyBegin(*CO,MAT_FINAL_ASSEMBLY); MatAssemblyEnd(*CO,MAT_FINAL_ASSEMBLY); }
  /* DEBUG: */
  /* if (C) myMatView(*C,"% f %+f i    "); */
  /* if (CO) myMatView(*CO,"% f %+f i    "); */

  /* free space */
  /* PetscFree(ncs);  */
  for (i = 0; i < part->s; ++i) {
    PetscFree(num_modes[i]); 
    for (j = 0; j < 4; ++j) {
      PetscFree(edge_modes[i][j]);
    }
    PetscFree(edge_modes[i]);
  }
  PetscFree(num_modes); /* PetscFree(subksp); */ PetscFree(edge_modes);
  PetscPrintf(PETSC_COMM_WORLD,"DDSetupCoarseBasisDense: I'm done!\n");
  PetscFunctionReturn(0);
}

/* ----------------------------------------------------------------------- 
   Cj          -  the array owned by this processor, of the coarse matrix C
   CO          -  the matrix CO for overlapping coarse basis
   dispCOa     -  when we get array of CO, disp to the column to be written
  ksp          -  the subdomain solver
  f            -  rhs for the subdomain solver 'ksp'
  edge_mode    -  edge mode
  iedge        -  locations of edge mode in f, f[iedge[l]]= edge_mode[l]
  ledge        -  length of iedge 
  ii,iC,dim    -  Cj[ii[l]]= f[ii[l]], l= 0,..,dim-1, i.e. restricted extension
                  of solution (stored in f) to C
  iiO,iCO,dimO -  for overlap region		  
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "CoarseExtendFromEdgeDense"
PetscErrorCode CoarseExtendFromEdgeDense(PetscScalar *Cj, Mat *CO, PetscInt dispCOa, KSP ksp, Vec f, PetscScalar *edge_mode, PetscInt *iedge, PetscInt ledge, PetscInt *ii, PetscInt *iC, PetscInt dim, PetscInt *iiO, PetscInt *iCO, PetscInt jCO, PetscInt dimO, PetscScalar *fO)
{
  PetscInt    l;
  PetscScalar *fa, *COa;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  /* add edge mode to rhs and solve the subdomain problem */
  ierr= VecZeroEntries(f);CHKERRQ(ierr);
  ierr= VecGetArray(f,&fa);CHKERRQ(ierr);
  for (l = 0; l < ledge; ++l) {
    fa[iedge[l]]= edge_mode[l];
   }
  ierr= VecRestoreArray(f,&fa);
  
  /* DEBUG: */
  /* ierr= VecView(f,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  /* Mat Amat, Pmat; */
  /* MatStructure flag; */
  /* ierr= KSPGetOperators(ksp,&Amat,&Pmat,&flag);CHKERRQ(ierr); */
  /* ierr= MatView(Amat,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */
  
  ierr= KSPSolve(ksp,f,f);CHKERRQ(ierr);

  /* DEBUG: */
  /* ierr= VecView(f,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr); */

  /* copy the restricted part of f to a column of C
       this could be done by restricted extension if it is available */
  /* printf("CoarseExtendFromEdgeDense: I'm here!\n"); */
  ierr= VecGetArray(f,&fa);CHKERRQ(ierr);
  if (Cj) {
    for(l = 0; l < dim; l++){
      Cj[iC[l]]= fa[ii[l]];
    }
  }
  if (CO) {
    MatGetArray(*CO,&COa);
    for(l = 0; l < dim; l++){
      COa[dispCOa+iC[l]]= fa[ii[l]];
    }
    MatRestoreArray(*CO,&COa);
    for (l = 0; l < dimO; ++l) {
      fO[l]= fa[iiO[l]];
    }
    MatSetValues(*CO,dimO,iCO,1,&jCO,fO,INSERT_VALUES);
  }
  ierr= VecRestoreArray(f,&fa);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__ 
#define __FUNCT__ "myMatView"
PetscErrorCode myMatView(Mat A, const char *format)
{
  PetscInt istart, iend, m, n, i, j;
  PetscErrorCode ierr;
  PetscScalar *array;
  PetscMPIInt rank;

  PetscFunctionBegin;
  
  ierr= MatGetOwnershipRange(A,&istart,&iend);CHKERRQ(ierr);
  ierr= MatGetArray(A,&array);CHKERRQ(ierr);
  ierr= PetscPrintf(PETSC_COMM_WORLD,"myMatView:\n");CHKERRQ(ierr);
  ierr= MatGetSize(A,&m,&n);CHKERRQ(ierr);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d]: \n",rank);
  for (i = 0; i < iend-istart; ++i) {
    for (j = 0; j < n; ++j) {
      PetscSynchronizedPrintf(PETSC_COMM_WORLD,format,PetscRealPart(array[j*(iend-istart)+i]),PetscImaginaryPart(array[j*(iend-istart)+i]));
    }
    PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n");
  }
  ierr= MatRestoreArray(A,&array);CHKERRQ(ierr);
  ierr= PetscSynchronizedFlush(PETSC_COMM_WORLD);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


/* ----------------------------------------------------------------------- 
   Legacy codes, end
   ----------------------------------------------------------------------- */
#endif
