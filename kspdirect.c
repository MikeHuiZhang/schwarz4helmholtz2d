#include <petscksp.h>

/* ----------------------------------------------------------------------- 
   Create and setup the direct solver KSP
 INPUT
   A
 OUTPUT
   ksp
   ----------------------------------------------------------------------- */
PetscErrorCode DirectSetupKSP(KSP *ksp, Mat A)
{
  PC                pc;
  PetscErrorCode    ierr;
  PetscMPIInt       np;

  /*
     Create linear solver context
  */
  ierr = KSPCreate(PETSC_COMM_WORLD,ksp);CHKERRQ(ierr);

  /*
     Set operators. Here the matrix that defines the linear system
     also serves as the preconditioning matrix.
  */
  ierr = KSPSetOperators(*ksp,A,A);CHKERRQ(ierr);

  /*
     Set KSP solver and PC solver
  */
  ierr= KSPSetType(*ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr= KSPGetPC(*ksp,&pc);CHKERRQ(ierr);
  MPI_Comm_size(PETSC_COMM_WORLD,&np);
  /* package to do factorization, e.g., MATSOLVERSUPERLU_DIST, MATSOLVERPETSC(only one proc.), c.f. definition of MatSolverPackage, -pc_factor_mat_solver_package */
  /* FIXME: choose PCLU or PCCHOLESKY, our treatment of Dirichlet is 
     non-symmetric */

  /* ierr= PCSetType(pc,PCLU);CHKERRQ(ierr); */
  /* if (np==1) { */
  /*   ierr= PCFactorSetMatSolverPackage(pc,MATSOLVERSUPERLU);CHKERRQ(ierr); */
  /* } */
  /* else */
  /*   ierr= PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);CHKERRQ(ierr); */
  
  ierr= PCSetType(pc,PCLU);CHKERRQ(ierr);
  ierr= PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);CHKERRQ(ierr);

  /*
    Set runtime options, e.g.,
        -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
  */
  ierr = KSPSetFromOptions(*ksp);CHKERRQ(ierr);

  return 0;
}
