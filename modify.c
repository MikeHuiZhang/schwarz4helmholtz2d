/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
   Define the function to modify submatrices for PCASM. The input submat
   are restrictions to interior nodes or exterior boundary of the original
   domain. A test shows that pcgasm sorts the nodes inside each subdomain
   with increasing (in petsc ordering) global indices. So submat keep the
   ordering of the original mat but not the ordering of user-defined subdomain
   nodes. It seems petsc does not change the ordering between subdomains. 
   Furthermore, we always get 'col' the same as 'row' from petsc.

   Question: am I allowed to SetLocalToGlobalMapping() for submat's?
   There is GetLocalToGlobalMapping in current petsc (see web page) for 
   backup and reset back. I found submat's has not been setted with any
   LocalToGlobalMapping yet when ModifSubMatrices was called from PCASM.
   So it is safe to set it when we modify submatrices.
   
   We assume a submat is supported only on one processor. 
   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

#include <petscksp.h>
/* #include <petscao.h> */
#include "modify.h"
#include "assembly.h"
#include "getabc.h"

static PetscInt called_times= 0;

#undef __FUNCT__
#define __FUNCT__ "ModifySubMatrices"
PetscErrorCode ModifySubMatrices(PC pc,PetscInt nsub,const IS *row,const IS *col,Mat *submat,SubMatCtx *ctx)
{
  PetscInt i,j,*dimys,*dimxs; 
  /* PetscInt ix,iy,count,*indices; */
  /* const PetscInt *myrowid; */
  /* IS myrow; */
  /* ISLocalToGlobalMapping ltog; /\* rmapping, cmapping; *\/ */
  PetscErrorCode ierr;
  PetscScalar v= 0.0; /* a value for corner condition */
  PetscScalar abc_coef[4]; /* exterior b.c. */

  PetscFunctionBegin;

  PetscPrintf(PETSC_COMM_WORLD,"ModifySubMatrices: called %d times\n",++called_times);

  /* Negative -sub_abc_type means classical Schwarz */
  if (ctx->abc_type < 0) {
    PetscFunctionReturn(0);
  }
  
  /* DEBUG: test the inputs */
  /* for (i = 0; i < nsub; ++i) { */
  /*   ISEqual(row[i],col[i],&flg); */
  /*   if (!flg){ */
  /*     printf("row and col are not the same\n"); */
  /*   } */
  /*   /\* ISView(row[i],PETSC_VIEWER_STDOUT_SELF); *\/ */
  /*   printf("x: %i~%i, y: %i~%i\n",ctx->startx[i],ctx->endx[i]-1,ctx->starty[i],ctx->endy[i]-1); */
  /*   ISGetSize(row[i],&count); */
  /*   PetscMalloc(count*sizeof(PetscInt),&indices); */
  /*   count= 0; */
  /*   for (ix = ctx->startx[i]; ix < ctx->endx[i]; ++ix) { */
  /*     for (iy = ctx->starty[i]; iy < ctx->endy[i]; ++iy) { */
  /*   	indices[count++]= ix*ctx->dimy+iy; */
  /*     } */
  /*   } */
  /*   AOApplicationToPetsc(ctx->ao,count,indices);  */
  /*   ISCreateGeneral(PETSC_COMM_SELF,count,indices,PETSC_OWN_POINTER, &myrow); */
  /*   ISSort(myrow); */
  /*   ISEqual(row[i],myrow,&flg); */
  /*   if (!flg) { */
  /*     printf("row and myrow are not the same\n"); */
  /*   } */
  /*   /\* ISView(myrow,PETSC_VIEWER_STDOUT_SELF); *\/ */
  /*   ISDestroy(&myrow); */
  /* } */
  
  /*
     Get size of each dimension
  */
  ierr= PetscMalloc(nsub*sizeof(PetscInt),&dimys);CHKERRQ(ierr);
  ierr= PetscMalloc(nsub*sizeof(PetscInt),&dimxs);CHKERRQ(ierr);
  for (i = 0; i < nsub; ++i) {
    dimys[i]= ctx->endy[i] - ctx->starty[i];
    dimxs[i]= ctx->endx[i] - ctx->startx[i];
  }

  /*
     Set ISLocalToGlobalMapping for the submatrices
  */
  for (i = 0; i < nsub; ++i) {
    MatSetLocalToGlobalMapping(submat[i],ctx->ltog[i],ctx->ltog[i]);
  }

  /* Old Codes to build ltog */
  /* /\* Set GlobalToLocalMapping in two steps:  */
  /*    (1) from row[i] to global 2D subscripts then to local 2D and 1D  */
  /*    (2) so 0~numrow-1 in petsc ordering of submat[i] are mapped to the  */
  /*        indices obtained from (1). */
  /*    If we set PCASMSetSortIndices to false, we may not use ltog here, but */
  /*    to call assembly.c which is used also for assembly of the whole domain */
  /*    matrix, we still do ltog here *\/ */
  /* for (i = 0; i < nsub; ++i) { */
  /*   ISDuplicate(row[i],&myrow); ISCopy(row[i],myrow); */
  /*   AOPetscToApplicationIS(ctx->ao,myrow); */
  /*   ISGetIndices(myrow,&myrowid); */
  /*   ISGetSize(row[i],&count); */
  /*   PetscMalloc(count*sizeof(PetscInt),&indices); */
  /*   for (j = 0; j < count; ++j) { */
  /*     ix= myrowid[j]/ctx->dimy - ctx->startx[i]; */
  /*     iy= (myrowid[j] % ctx->dimy) - ctx->starty[i]; */
  /*     indices[ix*dimys[i] + iy]= j;  /\* in old order the index is j *\/ */
  /*   } */
  /*   ISRestoreIndices(myrow,&myrowid); */
  /*   ISDestroy(&myrow); */
  /*   ISCreateGeneral(PETSC_COMM_SELF,count,indices,PETSC_OWN_POINTER,&myrow); */
  /*   ISLocalToGlobalMappingCreateIS(myrow,&ltog); */
  /*   ISDestroy(&myrow); */

  /*   /\* DEBUG: I found rmapping and cmapping are NULL *\/ */
  /*   /\* MatGetLocalToGlobalMapping(submat[i],&rmapping,&cmapping); *\/ */
  /*   /\* if (rmapping) { *\/ */
  /*   /\*   ISLocalToGlobalMappingView(rmapping,PETSC_VIEWER_STDOUT_SELF); *\/ */
  /*   /\* } *\/ */

  /*   MatSetLocalToGlobalMapping(submat[i],ltog,ltog); */

  /*   /\* DEBUG: *\/ */
  /*   /\* ISView(row[i],PETSC_VIEWER_STDOUT_SELF); *\/ */
  /*   /\* if (ctx->starty[i] && ctx->startx[i]) ISLocalToGlobalMappingView(ltog,PETSC_VIEWER_STDOUT_SELF); *\/ */

  /*   /\* FIXME: it seems we should NOT destroy ltog *\/ */
  /*   /\* ISLocalToGlobalMappingDestroy(&ltog); *\/ */
  /* } */


  /* Apply new boundary conditions */
  OptParamCtx optctx; optctx.CL= ctx->overlap-1; optctx.mode= ctx->mode;
  optctx.g= ctx->g;
  for (i = 0; i < nsub; ++i) {
    /* first apply Neumann for all sides */
    if (ctx->startx[i]!=0) { /* left side */  
      /* DEBUG: */
      /* printf("apply Neumann on left side of sub %i\n",i); */
      /* PetscBool assembled= PETSC_FALSE; */
      /* MatAssembled(submat[i], &assembled); */
      /* if (assembled) printf("submatrix has been assembled\n"); */
      /* MatView(submat[i],PETSC_VIEWER_STDOUT_SELF); */
      ApplyNeumann(submat[i],ctx->k2,ctx->hy,ctx->hx,0,1,dimys[i]-1);
    }
    
    if (ctx->endx[i]!=ctx->dimx) { /* right side */
      /* printf("apply Neumann on right side of sub %i\n",i); */
      ierr= MatAssemblyBegin(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ierr= MatAssemblyEnd(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ApplyNeumann(submat[i],ctx->k2,ctx->hy,ctx->hx,(dimxs[i]-1)*dimys[i],1,dimys[i]-1);
    }

    if (ctx->starty[i]!=0) { /* bottom side */
      /* printf("apply Neumann on bottom side of sub %i\n",i); */
      ierr= MatAssemblyBegin(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ierr= MatAssemblyEnd(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ApplyNeumann(submat[i], ctx->k2, ctx->hx, ctx->hy, 0, dimys[i], dimxs[i]-1);
    }

    if (ctx->endy[i]!=ctx->dimy) { /* top side */
      /* printf("apply Neumann on top side of sub %i\n",i); */
      ierr= MatAssemblyBegin(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ierr= MatAssemblyEnd(submat[i],MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ApplyNeumann(submat[i], ctx->k2, ctx->hx, ctx->hy, dimys[i]-1, dimys[i], dimxs[i]-1);
    }

    /* then apply ABC, after all ApplyNeumann so that ABC is not zeroed */
    optctx.h= ctx->hy; optctx.kmin= PETSC_PI/ctx->hy/(dimys[i]-1);
    GetABCCoef(ctx->abc_coef,ctx->k_coef,ctx->abc_type,ctx->abc_order,&optctx);

    if (ctx->startx[i]!=0) { /* left side */  
      ApplyABC(submat[i], ctx->abc_coef, ctx->hy, 0, 1, dimys[i]-1);
    }
    
    if (ctx->endx[i]!=ctx->dimx) { /* right side */
      ApplyABC(submat[i], ctx->abc_coef, ctx->hy, (dimxs[i]-1)*dimys[i], 1, dimys[i]-1);
    }

    optctx.h= ctx->hx; optctx.kmin= PETSC_PI/ctx->hx/(dimxs[i]-1);
    GetABCCoef(ctx->abc_coef,ctx->k_coef,ctx->abc_type,ctx->abc_order,&optctx);

    if (ctx->starty[i]!=0) { /* bottom side */
      ApplyABC(submat[i], ctx->abc_coef, ctx->hx, 0, dimys[i], dimxs[i]-1);
    }

    if (ctx->endy[i]!=ctx->dimy) { /* top side */
      ApplyABC(submat[i], ctx->abc_coef, ctx->hx, dimys[i]-1, dimys[i], dimxs[i]-1);
    }


    
    /* corner condition arising from second-order term of the ABC */
    if (ctx->use_corner && ctx->abc_type>0 && ctx->abc_type<3 && ctx->abc_order==2) {
      if (ctx->startx[i]!=0 && ctx->starty[i]!=0) { /* corner (0,0) */
    	j= 0; v= ctx->abc_coef[3];
    	ierr=MatSetValuesLocal(submat[i],1,&j,1,&j,&v,ADD_VALUES);CHKERRQ(ierr);
      }
      if (ctx->startx[i]!=0 && ctx->endy[i]!=ctx->dimy) { /* corner (0,nys) */
    	j= dimys[i]-1; v= ctx->abc_coef[3];
    	ierr=MatSetValuesLocal(submat[i],1,&j,1,&j,&v,ADD_VALUES);CHKERRQ(ierr);
      }
      if (ctx->endx[i]!=ctx->dimx && ctx->starty[i]!=0) { /* corner (nxs,0) */
    	j= (dimxs[i]-1)*dimys[i]; v= ctx->abc_coef[3];
    	ierr=MatSetValuesLocal(submat[i],1,&j,1,&j,&v,ADD_VALUES);CHKERRQ(ierr);
      }
      if (ctx->endx[i]!=ctx->dimx && ctx->endy[i]!=ctx->dimy) {/* (nxs,nys) */
    	j= dimxs[i]*dimys[i]-1; v= ctx->abc_coef[3];
    	ierr=MatSetValuesLocal(submat[i],1,&j,1,&j,&v,ADD_VALUES);CHKERRQ(ierr);
      }
    }

    /* recover absorbing conditions for exterior boundary nodes */
    GetABCCoef(abc_coef,ctx->k_coef,1,ctx->ext_abc_order,PETSC_NULL);
    if (ctx->bctype[0]==1 && ctx->startx[i]==0 && ctx->starty[i]!=0) { /* (0,0) */
      ApplyABCNode(submat[i],abc_coef,ctx->hy,0);
    }
    else if (ctx->bctype[2]==1 && ctx->starty[i]==0 && ctx->startx[i]!=0) {
      ApplyABCNode(submat[i],abc_coef,ctx->hx,0);
    }
    if (ctx->bctype[0]==1&&ctx->startx[i]==0 && ctx->endy[i]!=ctx->dimy){/*(0,nys)*/
      ApplyABCNode(submat[i],abc_coef,ctx->hy,dimys[i]-1);
    }
    else if (ctx->bctype[3]==1 && ctx->endy[i]==ctx->dimy && ctx->startx[i]!=0) {
      ApplyABCNode(submat[i],abc_coef,ctx->hx,dimys[i]-1);
    }
    if (ctx->bctype[1]==1&&ctx->endx[i]==ctx->dimx && ctx->starty[i]!=0){/*nxs,0*/
      ApplyABCNode(submat[i],abc_coef,ctx->hy,(dimxs[i]-1)*dimys[i]);
    }
    else if (ctx->bctype[2]==1 && ctx->starty[i]==0 && ctx->endx[i]!=ctx->dimx) {
      ApplyABCNode(submat[i],abc_coef,ctx->hx,(dimxs[i]-1)*dimys[i]);
    }
    if (ctx->bctype[1]==1&&ctx->endx[i]==ctx->dimx&&ctx->endy[i]!=ctx->dimy){/*n,n*/
      ApplyABCNode(submat[i],abc_coef,ctx->hy,dimxs[i]*dimys[i]-1);
    }
    else if (ctx->bctype[3]==1&&ctx->endy[i]==ctx->dimy && ctx->endx[i]!=ctx->dimx) {
      ApplyABCNode(submat[i],abc_coef,ctx->hx,dimxs[i]*dimys[i]-1);
    }

    /* final assembly */
    ierr= MatAssemblyBegin(submat[i],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr= MatAssemblyEnd(submat[i],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    /* recover Dirichlet conditions for exterior Dirichlet nodes */
    if ((ctx->bctype[0]==2 && ctx->startx[i]==0 && ctx->starty[i]!=0) || (ctx->bctype[2]==2 && ctx->starty[i]==0 && ctx->startx[i]!=0)) { /* (0,0) */
      j= 0;
      ierr= MatZeroRowsLocal(submat[i],1,&j,1.0,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    }
    if ((ctx->bctype[0]==2 && ctx->startx[i]==0 && ctx->endy[i]!=ctx->dimy) || (ctx->bctype[3]==2 && ctx->endy[i]==ctx->dimy && ctx->startx[i]!=0)) { /* (0,nys) */
      j= dimys[i]-1;
      ierr= MatZeroRowsLocal(submat[i],1,&j,1.0,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    }
    if ((ctx->bctype[1]==2 && ctx->endx[i]==ctx->dimx && ctx->starty[i]!=0) || (ctx->bctype[2]==2 && ctx->starty[i]==0 && ctx->endx[i]!=ctx->dimx)) { /* (nxs,0) */
      j= (dimxs[i]-1)*dimys[i];
      ierr= MatZeroRowsLocal(submat[i],1,&j,1.0,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    }
    if ((ctx->bctype[1]==2 && ctx->endx[i]==ctx->dimx && ctx->endy[i]!=ctx->dimy) || (ctx->bctype[3]==2 && ctx->endy[i]==ctx->dimy && ctx->endx[i]!=ctx->dimx)) { /* (nxs,nys) */
      j= dimxs[i]*dimys[i]-1;
      ierr= MatZeroRowsLocal(submat[i],1,&j,1.0,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
    }

    /* DEBUG: */
    /* ierr= MatAssemblyBegin(submat[i],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
    /* ierr= MatAssemblyEnd(submat[i],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
    /* if (ctx->starty[i] && ctx->startx[i]) MatView(submat[i],PETSC_VIEWER_STDOUT_SELF); */


  } /* end for i */

  ierr= PetscFree(dimys);CHKERRQ(ierr); ierr= PetscFree(dimxs);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
