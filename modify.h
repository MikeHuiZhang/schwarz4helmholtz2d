#ifndef _HELM2D_SUBMAT
#define _HELM2D_SUBMAT

/* #include <petscao.h> */
#include <petscis.h>

typedef struct
{
  PetscReal k_coef;
  PetscReal k2;
  PetscInt *bctype;
  PetscInt  ext_abc_order;
  ISLocalToGlobalMapping *ltog;	/* natural to global for submatrices */
  PetscInt dimx;        /* number of nodes along x and y     */
  PetscInt dimy;        /* directions in the original domain */
  PetscInt *startx;     /* - - - - - - - - - - - - - - - - */ 
  PetscInt *endx;       /* bounds for global 2D subscripts */
  PetscInt *starty;     /* of overlapping subdomains nodes */  
  PetscInt *endy;       /* - - - - - - - - - - - - - - - - */
  PetscInt *crossx;	/* - - - - - - - - - - - - - - - - - - - */
  PetscInt *crossy;     /* global 2D subscripts of cross-points  */
  PetscInt *crossend;   /* - - - - - - - - - - - - - - - - - - - */
  PetscReal hx;
  PetscReal hy;
  PetscReal overlap;    /* number of overlapping nodes */
  PetscInt  abc_type;   /* type of abc, -1 for no modification, see GetABCCoef() for the meaning of positive values */
  PetscInt  abc_order;  /* order of abc */
  char     *mode;       /* scaling mode for optimized methods */
  PetscReal    g;       /* for the mode=="w", h^g w = Cw */
  PetscBool use_corner; /* use corner conditions */
  PetscScalar *abc_coef;/* specified coefficients when abc_type==3 */
} SubMatCtx;

typedef struct
{
  PetscReal    h;       /* mesh size along interface */
  PetscReal kmin;       /* minimum spatial frequency on interface */
  PetscInt    CL;       /* number of overlapping mesh elements, is overlap -1 */
  char     *mode;       /* which scaling is considered */
  PetscReal    g;       /* for the mode=="w", h^g w = Cw */
} OptParamCtx;

PetscErrorCode submat_ctx_destroy(SubMatCtx *submat_ctx);

PetscErrorCode submat_ctx_view(SubMatCtx *submat_ctx);

#endif
