#include "helmbvp.h"
#include "mypart.h"
#include "modify.h"
#include "getabc.h"

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   set default options and load the non-default options from command line

   INOUT
     helm_bvp     extern GLOBAL 
     part
     nquad
     submat_ctx   but .ltog is left blank to be setted elsewhere
     
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#undef __FUNCT__
#define __FUNCT__ "HelmDDInit"
PetscErrorCode HelmDDInit(PetscMPIInt rank, MyPartition *part, SubMatCtx *submat_ctx, PetscInt *nquad, PetscInt *nd, PetscInt *cform, PetscBool *init_rand, PetscBool *search_direct, PetscBool *search_nm, PetscInt *search_form, PetscInt *search_maxit)
{
  PetscInt i, nsetted=4;
  PetscBool setted[4];
  PetscErrorCode ierr;

  /*
     helm_bvp 
  */
  helm_bvp.freq= 1.;
  helm_bvp.cspeed= 1.;
  helm_bvp.k_coef= 2.*PETSC_PI;
  helm_bvp.k2= 4.*PETSC_PI*PETSC_PI;
  helm_bvp.xl= 0.; helm_bvp.xr= 1.; helm_bvp.yl= 0.; helm_bvp.yr= 1.;
  helm_bvp.abc_order= 2; /* 1st or 2nd order absorbing b.c. */
  ierr= PetscMalloc(4*sizeof(PetscScalar),&(helm_bvp.abc_coef));CHKERRQ(ierr);
  ierr= PetscMalloc(4*sizeof(PetscInt),&(helm_bvp.bctype));CHKERRQ(ierr);
  for (i = 0; i < 4; ++i) {
    helm_bvp.bctype[i]= 1;
  }
  helm_bvp.model_type= 0;
  ierr= PetscOptionsGetReal(PETSC_NULL,"-xl",&helm_bvp.xl,PETSC_NULL);CHKERRQ(ierr);
  helm_bvp.yl= helm_bvp.xl; ierr= PetscOptionsGetReal(PETSC_NULL,"-yl",&helm_bvp.yr,PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetReal(PETSC_NULL,"-xr",&helm_bvp.xr,PETSC_NULL);CHKERRQ(ierr);
  helm_bvp.yr= helm_bvp.xr; ierr= PetscOptionsGetReal(PETSC_NULL,"-yr",&helm_bvp.yr,PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetInt(PETSC_NULL,"-model_type",&(helm_bvp.model_type),PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetIntArray(PETSC_NULL,"-bctype",helm_bvp.bctype,&nsetted,setted);CHKERRQ(ierr);
  if (helm_bvp.model_type==2) {
    PetscPrintf(PETSC_COMM_WORLD, "model_type==2, bctype is resetted to abc\n");
    helm_bvp.bctype[0]= 1; helm_bvp.bctype[1]= 1; helm_bvp.bctype[2]= 1; helm_bvp.bctype[3]= 1;
    ierr= PetscOptionsGetInt(PETSC_NULL,"-nsrc",&helm_bvp.nsrc,PETSC_NULL);CHKERRQ(ierr);
    ierr= PetscMalloc(helm_bvp.nsrc*sizeof(PetscReal),&helm_bvp.xsrc);CHKERRQ(ierr);
    ierr= PetscMalloc(helm_bvp.nsrc*sizeof(PetscReal),&helm_bvp.ysrc);CHKERRQ(ierr);
    nsetted= 4;
    helm_bvp.xsrc[0]= 0.3; ierr= PetscOptionsGetRealArray(PETSC_NULL,"-xsrc",helm_bvp.xsrc,&nsetted,setted);CHKERRQ(ierr); helm_bvp.nsrc= nsetted;
    for (i = 0; i < helm_bvp.nsrc; ++i) {
      helm_bvp.ysrc[i]= helm_bvp.xsrc[i];
    }
    ierr= PetscOptionsGetRealArray(PETSC_NULL,"-ysrc",helm_bvp.ysrc,&nsetted,setted);CHKERRQ(ierr);
  }
  ierr= PetscOptionsGetInt(PETSC_NULL,"-abc_order",&helm_bvp.abc_order,PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetReal(PETSC_NULL,"-freq",&helm_bvp.freq,PETSC_NULL);CHKERRQ(ierr);
  if (helm_bvp.abc_order==2 && helm_bvp.freq==0 && (helm_bvp.bctype[0]==1 || helm_bvp.bctype[1]==1 || helm_bvp.bctype[2]==1 || helm_bvp.bctype[3]==1)) {
    PetscPrintf(PETSC_COMM_WORLD,"helm2d.c: freq>0 is required when using 2nd order abc \n");
    ierr= PetscFinalize();
    return 0;
  }
  ierr= PetscOptionsGetReal(PETSC_NULL,"-cspeed",&helm_bvp.cspeed,PETSC_NULL);CHKERRQ(ierr);
  if (helm_bvp.cspeed==0) {
    PetscPrintf(PETSC_COMM_WORLD,"helm2d.c: cspeed must be nonzero!\n");
    ierr= PetscFinalize();
    return 0;
  }
  helm_bvp.k_coef= 2*PETSC_PI*helm_bvp.freq/helm_bvp.cspeed;  
  helm_bvp.k2= helm_bvp.k_coef*helm_bvp.k_coef;
  GetABCCoef(helm_bvp.abc_coef,helm_bvp.k_coef,1,helm_bvp.abc_order,PETSC_NULL);

  /*
     "part" options
  */
  part->nx= 2; part->ny= 2; part->px= 1; part->py= 1; part->sx=1; part->sy= 1;
  part->overlap= 3;
  ierr= PetscOptionsGetInt(PETSC_NULL,"-nx",&(part->nx),PETSC_NULL);CHKERRQ(ierr);
  part->ny= part->nx;ierr= PetscOptionsGetInt(PETSC_NULL,"-ny",&(part->ny),PETSC_NULL);CHKERRQ(ierr);
  PetscPrintf (PETSC_COMM_WORLD, "nx=%i, ny=%i\n",part->nx,part->ny);
  ierr= PetscOptionsGetInt(PETSC_NULL,"-px",&(part->px),PETSC_NULL);CHKERRQ(ierr);
  part->py= part->px;ierr= PetscOptionsGetInt(PETSC_NULL,"-py",&(part->py),PETSC_NULL);CHKERRQ(ierr);
  MPI_Comm_size(PETSC_COMM_WORLD,&(part->p));
  if (part->px*(part->py)!=part->p) {
    PetscPrintf (PETSC_COMM_WORLD, "helm2d.c: We require np equals to px*py\n");
    ierr= PetscFinalize();
    return 0;
  }
  ierr= PetscPrintf (PETSC_COMM_WORLD, "px=%i, py=%i, np=%i\n",part->px,part->py,part->p);CHKERRQ(ierr);
  ierr= PetscOptionsGetInt(PETSC_NULL,"-sx",&(part->sx),PETSC_NULL);CHKERRQ(ierr);
  part->sy= part->sx;ierr= PetscOptionsGetInt(PETSC_NULL,"-sy",&(part->sy),PETSC_NULL);CHKERRQ(ierr);
  PetscPrintf (PETSC_COMM_WORLD, "sx=%i, sy=%i\n",part->sx,part->sy);
  if (part->px > part->nx || part->py > part->ny) {
    PetscPrintf (PETSC_COMM_WORLD, "helm2d.c: Please use px<=nx, py<=ny \n");
      ierr= PetscFinalize();
      return 0;
  }
  if (part->sx > part->nx/part->px || part->sy > part->ny/part->py) {
    PetscPrintf (PETSC_COMM_WORLD, "helm2d.c: Please use sx<=nx/px, sy<=ny/py \n");
      ierr= PetscFinalize();
      return 0;
  }
  ierr= PetscOptionsGetInt(PETSC_NULL,"-overlap",&(part->overlap),PETSC_NULL);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"nodal overlap %i\n",part->overlap);
  part->dim= (part->nx+1)*(part->ny+1);  
  PetscPrintf(PETSC_COMM_WORLD, "dim=%i\n",part->dim);
  part->s= part->sx*(part->sy);
  mypart2d(part, rank);
  if (helm_bvp.freq!=0) {
      PetscPrintf(PETSC_COMM_WORLD, "elements number per wavelength along x, y: %g, %g\n",part->nx*helm_bvp.cspeed/(helm_bvp.xr-helm_bvp.xl)/helm_bvp.freq,part->ny*helm_bvp.cspeed/(helm_bvp.yr-helm_bvp.yl)/helm_bvp.freq);
  }


  /*
     context for modifying submatrices 
  */
  /* memory allocation */
  ierr= PetscMalloc(10*sizeof(char),&(submat_ctx->mode));CHKERRQ(ierr);
  ierr= PetscMalloc(4*sizeof(PetscScalar),&(submat_ctx->abc_coef));CHKERRQ(ierr);
  /* default values */
  submat_ctx->k_coef= helm_bvp.k_coef; 
  submat_ctx->k2= helm_bvp.k2;
  submat_ctx->bctype= helm_bvp.bctype;
  submat_ctx->ext_abc_order= helm_bvp.abc_order;
  submat_ctx->dimx= part->nx+1; submat_ctx->dimy= part->ny+1;
  submat_ctx->startx= part->startxo; submat_ctx->endx= part->endxo; 
  submat_ctx->starty= part->startyo; submat_ctx->endy= part->endyo; 
  submat_ctx->crossx= part->crossx;  submat_ctx->crossy= part->crossy;
  submat_ctx->crossend= part->crossend;
  submat_ctx->hx= (helm_bvp.xr-helm_bvp.xl)/part->nx; 
  submat_ctx->hy= (helm_bvp.yr-helm_bvp.yl)/part->ny;
  submat_ctx->overlap= part->overlap;
  submat_ctx->abc_type= 2; submat_ctx->abc_order= 2;
  sprintf(submat_ctx->mode,"w");
  submat_ctx->g= 1.0; 
  submat_ctx->use_corner= PETSC_FALSE;
  /* load options from command line */
  ierr= PetscOptionsGetInt(PETSC_NULL,"-sub_abc_type",&(submat_ctx->abc_type),PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetInt(PETSC_NULL,"-sub_abc_order",&(submat_ctx->abc_order),PETSC_NULL);CHKERRQ(ierr);
  ierr= PetscOptionsGetString(PETSC_NULL,"-sub_mode",submat_ctx->mode,2*sizeof(char),setted);CHKERRQ(ierr);
  ierr= PetscOptionsGetReal(PETSC_NULL,"-sub_g",&(submat_ctx->g),PETSC_NULL);CHKERRQ(ierr);
  PetscOptionsGetBool(PETSC_NULL,"-sub_use_corner",&(submat_ctx->use_corner),PETSC_NULL); 
  if (submat_ctx->abc_type>2) {
    ierr= PetscOptionsGetScalar(PETSC_NULL,"-sub_coef0",submat_ctx->abc_coef,setted);CHKERRQ(ierr);
    if (!setted[0]) {
      PetscPrintf(PETSC_COMM_WORLD,"helm2d.c: -sub_coef0 must be given when -sub_abc_type>2. Exit!\n");
      PetscFinalize();
      return 0;
    }
    ierr= PetscOptionsGetScalar(PETSC_NULL,"-sub_coef1",(submat_ctx->abc_coef)+1,setted);CHKERRQ(ierr);
    if (!setted[0]) {
      PetscPrintf(PETSC_COMM_WORLD,"helm2d.c: -sub_coef1 must be given when -sub_abc_type>2. Exit!\n");
      PetscFinalize();
      return 0;
    }
    submat_ctx->abc_coef[2]= -PETSC_i*(submat_ctx->k_coef)*(2+PETSC_i*(submat_ctx->k_coef)*(submat_ctx->abc_coef[1])); /* c.f. 1990 Bamberg */
    submat_ctx->abc_coef[3]= -submat_ctx->abc_coef[1]*(submat_ctx->abc_coef[2]);
  }
  if (submat_ctx->abc_type==1 && helm_bvp.freq==0 && submat_ctx->abc_order==2) {
    PetscPrintf(PETSC_COMM_WORLD,"helm2d.c: freq>0 is required when using 2nd order taylor abc for subdomains\n");
    ierr= PetscFinalize();
    return 0;
  }
  

  /*
     other options
  */
  *nquad= 4;  			/* quadrature order */
  ierr= PetscOptionsGetInt(PETSC_NULL,"-nquad",nquad,PETSC_NULL);CHKERRQ(ierr);
  *nd= 0;  			/* number of directions for plane waves */
  ierr= PetscOptionsGetInt(PETSC_NULL,"-nd",nd,PETSC_NULL);CHKERRQ(ierr);
  *cform= 1;  			/* form of coarse matrix construction, see kspdd.c */
  ierr= PetscOptionsGetInt(PETSC_NULL,"-cform",cform,PETSC_NULL);CHKERRQ(ierr);
  *init_rand= PETSC_FALSE;   	/* random initial guess? */
  ierr= PetscOptionsGetBool(PETSC_NULL,"-init_rand",init_rand,PETSC_NULL);CHKERRQ(ierr);
  *search_direct= PETSC_FALSE; *search_nm= PETSC_FALSE;
  ierr= PetscOptionsGetBool(PETSC_NULL,"-search_direct",search_direct,PETSC_NULL);
  ierr= PetscOptionsGetBool(PETSC_NULL,"-search_nm",search_nm,PETSC_NULL);
  *search_form= 1; 
  ierr= PetscOptionsGetInt(PETSC_NULL,"-search_form",search_form,PETSC_NULL);CHKERRQ(ierr);
  *search_maxit= 30;
  ierr= PetscOptionsGetInt(PETSC_NULL,"-search_maxit",search_maxit,PETSC_NULL);CHKERRQ(ierr);
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "helm_bvp_destroy"
PetscErrorCode helm_bvp_destroy(HelmBVP *bvp)
{
  PetscErrorCode ierr;

  ierr= PetscFree(bvp->abc_coef);CHKERRQ(ierr);
  ierr= PetscFree(bvp->bctype);CHKERRQ(ierr);
  ierr= PetscFree(bvp->xsrc);CHKERRQ(ierr);
  ierr= PetscFree(bvp->ysrc);CHKERRQ(ierr);
  
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "helm_bvp_view"
PetscErrorCode helm_bvp_view(HelmBVP *bvp)
{
  PetscInt i;

  PetscPrintf (PETSC_COMM_WORLD, "------------ begin helm_bvp_view --------------\n");
  PetscPrintf(PETSC_COMM_WORLD, "freq= %g, cspeed= %g, k= %g\n",helm_bvp.freq,helm_bvp.cspeed,helm_bvp.k_coef);
  PetscPrintf (PETSC_COMM_WORLD, "the domain (%g, %g) X (%g, %g)\n",helm_bvp.xl,helm_bvp.xr,helm_bvp.yl,helm_bvp.yr);
  if (helm_bvp.freq!=0) {
      PetscPrintf(PETSC_COMM_WORLD, "wavelength= %g, (xr-xl)/wavelength= %g, (yr-yl)/wavelength= %g\n",helm_bvp.cspeed/helm_bvp.freq,(helm_bvp.xr-helm_bvp.xl)*helm_bvp.freq/helm_bvp.cspeed,(helm_bvp.yr-helm_bvp.yl)*helm_bvp.freq/helm_bvp.cspeed);
  }
  PetscPrintf(PETSC_COMM_WORLD,"order of absorbing bc: %i\n",helm_bvp.abc_order);
  PetscPrintf (PETSC_COMM_WORLD, "bctype= [%i  %i  %i  %i]\n",helm_bvp.bctype[0],helm_bvp.bctype[1],helm_bvp.bctype[2],helm_bvp.bctype[3]);
  PetscPrintf(PETSC_COMM_WORLD, "model_type= %i\n",helm_bvp.model_type);
  if (helm_bvp.model_type==2) {
    for (i = 0; i < helm_bvp.nsrc; ++i) {
      PetscPrintf(PETSC_COMM_WORLD,"xsrc= %g   ",helm_bvp.xsrc[i]);
    }
    PetscPrintf(PETSC_COMM_WORLD,"\n");
    for (i = 0; i < helm_bvp.nsrc; ++i) {
      PetscPrintf(PETSC_COMM_WORLD,"ysrc= %g   ",helm_bvp.ysrc[i]);
    }
    PetscPrintf(PETSC_COMM_WORLD,"\n");
  }  
  PetscPrintf (PETSC_COMM_WORLD, "------------ end helm_bvp_view --------------\n");
  
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "submat_ctx_destroy"
PetscErrorCode submat_ctx_destroy(SubMatCtx *submat_ctx)
{
  PetscErrorCode ierr;

  ierr= PetscFree(submat_ctx->mode);CHKERRQ(ierr);
  ierr= PetscFree(submat_ctx->abc_coef);CHKERRQ(ierr);
  /* TODO: destroy ->ltog[i] and free ->ltog */

  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "submat_ctx_view"
PetscErrorCode submat_ctx_view(SubMatCtx *submat_ctx)
{
  PetscErrorCode ierr;
  
  ierr= PetscPrintf(PETSC_COMM_WORLD,"coef0= %g+%gi, coef1= %g+%gi\n",PetscRealPart(submat_ctx->abc_coef[0]),PetscImaginaryPart(submat_ctx->abc_coef[0]),PetscRealPart(submat_ctx->abc_coef[1]),PetscImaginaryPart(submat_ctx->abc_coef[1]));CHKERRQ(ierr);


  return 0;
}
  
