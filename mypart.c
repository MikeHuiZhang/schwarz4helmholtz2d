#include "mypart.h"

PetscErrorCode mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx);

/*
   INPUT:
     rank,
     part.
        nx, ny,
        px, py,
        sx, sy,

   OUTPUT:
   part.
     rx, ry,

     nxp, elem_startx, elem_endx, dimxp, 
     nyp, elem_starty, elem_endy, dimyp,

     isx, isy,  subdomain 2d subscripts 
     nxs, selem_startx, selem_endx, dimxs,
     nys, selem_starty, selem_endy, dimys,
     dims,

     startxo, endxo, dimxso, 
     startyo, endyo, dimyso, 
     dimso, crossx, crossy, crossend

    We assume each subdomain is supported by one and only one processor.
    First, partition onto processors, then for each processor partition
    to local subdomains.
*/

#undef __FUNCT__
#define __FUNCT__ "mypart2d"
PetscErrorCode mypart2d(MyPartition *part, PetscMPIInt rank)
{
  PetscInt i, count;
  PetscErrorCode ierr;
  
  ierr= PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD, "------- Entering mypart2d ------------\n");

  /*
     input checks and reset  TODO: alert 
  */
  if ((part->nx+1)*(part->ny+1)!=part->dim) {
    part->dim= (part->nx+1)*(part->ny+1);
  }
  if (part->px*(part->py)!=part->p) {
    part->p= part->px * (part->py);
  }
  if (part->sx*(part->sy)!=part->s) {
    part->s= part->sx * (part->sy);
  }
  

  /*
     pseudo-subdomains: partition of elements and nodes onto processors, 
     the proc. owns nodes on its left boundary, excludes those on the right 
     unless the right side is part of the exterior boundary
  */
  part->rx= rank / part->py;  part->ry= rank % part->py;
  mypart1d(part->rx,part->nx,part->px,&(part->nxp),&(part->elem_startx),&(part->elem_endx));
  mypart1d(part->ry,part->ny,part->py,&(part->nyp),&(part->elem_starty),&(part->elem_endy));
  part->dimxp= part->nxp + ((part->rx!=part->px-1)?0:1); 
  part->dimyp= part->nyp + ((part->ry!=part->py-1)?0:1);
  part->dimp= part->dimxp*(part->dimyp);
  
  /*
    non-overlapping subdomains inside a pseudo-domain (processor) 
  */
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->isx));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->isy));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->nxs));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->nys));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->selem_startx));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->selem_starty));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->selem_endx));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->selem_endy));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dims));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dimxs));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dimys));CHKERRQ(ierr);
  for (i=0; i< part->s; ++i)  /* subdomain index starts from 0 */
  {
    part->isx[i]= i / part->sy;  part->isy[i]= i % part->sy;
    mypart1d(part->isx[i],part->nxp,part->sx,&(part->nxs[i]),&(part->selem_startx[i]),&(part->selem_endx[i]));
    mypart1d(part->isy[i],part->nyp,part->sy,&(part->nys[i]),&(part->selem_starty[i]),&(part->selem_endy[i]));
    part->dimxs[i]= part->nxs[i] + ((part->rx!=part->px-1 || part->isx[i]!=part->sx-1)?0:1);
    part->dimys[i]= part->nys[i] + ((part->ry!=part->py-1 || part->isy[i]!=part->sy-1)?0:1);
    part->dims[i]= part->dimxs[i]*(part->dimys[i]);
  }
  
  /*
    overlapping subdomains: different from pseudo-domains, we partition by
    the *nodes*, a subdomain is between its start and ending nodes,
    endxo is the one larger than the index of the ending node
    overlap: number of nodal layers in overlapping region, 0 for algebraic (nodes) non-overlapping, 1 for geometric non-overlapping, for odd overlap, the added layers for the left subdomain is one more than the right
  */
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->startxo));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->startyo));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->endxo));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->endyo));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dimso));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dimxso));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->dimyso));CHKERRQ(ierr);
  for (i = 0; i < part->s; ++i) {
    part->startxo[i]= part->elem_startx + part->selem_startx[i] - ((part->rx==0 && part->isx[i]==0)?0:(part->overlap/2));
    if (part->startxo[i] < 0) {  /* exceed the original domain */
      PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%i] subdomain (%i) with overlap exceeds the original domain! Restted to the boundary.\n",rank,i);
      part->startxo[i]= 0;
    }
    part->endxo[i]= part->elem_startx + part->selem_startx[i] + part->dimxs[i] + ((part->rx==part->px-1 && part->isx[i]==part->sx-1)?0:((part->overlap+1)/2));
    if (part->endxo[i] > part->nx+1) {  /* exceed the original domain */
      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] subdomain (%i) with overlap exceeds the original domain! Restted to the boundary.\n",rank,i);
      part->endxo[i]= part->nx + 1;
    }
    part->dimxso[i]= part->endxo[i] - part->startxo[i];
    part->startyo[i]= part->elem_starty + part->selem_starty[i] - ((part->ry==0 && part->isy[i]==0)?0:(part->overlap/2));
    if (part->startyo[i] < 0) {  /* exceed the original domain */
      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] subdomain (%i) with overlap exceeds the original domain! Restted to the boundary.\n",rank,i);
      part->startyo[i]= 0;
    }
    part->endyo[i]= part->elem_starty + part->selem_starty[i] + part->dimys[i] + ((part->ry==part->py-1 && part->isy[i]==part->sy-1)?0:((part->overlap+1)/2));
    if (part->endyo[i] > part->ny+1) {  /* exceed the original domain */
      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] subdomain (%i) with overlap exceeds the original domain! Restted to the boundary.\n",rank,i);
      part->endyo[i]= part->ny + 1;
    }
    part->dimyso[i]= part->endyo[i] - part->startyo[i];
    part->dimso[i]= part->dimxso[i]*(part->dimyso[i]);
  }

  /*
    define the cross-points TODO: eliminate repeated cross-points in one 
    subdomain 
  */
  ierr= PetscMalloc(16*(part->s)*sizeof(PetscInt),&(part->crossx));CHKERRQ(ierr);
  ierr= PetscMalloc(16*(part->s)*sizeof(PetscInt),&(part->crossy));CHKERRQ(ierr);
  ierr= PetscMalloc(part->s*sizeof(PetscInt),&(part->crossend));CHKERRQ(ierr);
  count= 0;
  for (i = 0; i < part->s; ++i) {
    part->crossend[i]= 0;
    if (part->startxo[i]!=0 && (part->elem_starty + part->selem_starty[i])!=0) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 0!\n"); */
      part->crossx[count]= part->startxo[i]; 
      part->crossy[count++]= part->elem_starty + part->selem_starty[i];
      part->crossx[count]= part->crossx[count-1]; 
      part->crossy[count]= part->crossy[count-1]-1;
      count++; /* becareful if use ++ in last 'equal', wrong! */
    }
    if (part->startxo[i]!=0 && (part->elem_starty + part->selem_endy[i])!=part->ny) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]: (%i) adding cross point 1!\n",rank,i); */
      part->crossx[count]= part->startxo[i]; 
      part->crossy[count++]= part->elem_starty + part->selem_endy[i];
      part->crossx[count]= part->crossx[count-1]; 
      part->crossy[count]= part->crossy[count-1]-1;
      count++;
    }
    if (part->endxo[i]!=part->nx+1 && (part->elem_starty + part->selem_starty[i])!=0) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 2!\n"); */
      part->crossx[count]= part->endxo[i]-1; 
      part->crossy[count++]= part->elem_starty + part->selem_starty[i];
      part->crossx[count]= part->crossx[count-1]; 
      part->crossy[count]= part->crossy[count-1]-1;
      count++;
    }
    if (part->endxo[i]!=part->nx+1 && (part->elem_starty + part->selem_endy[i])!=part->ny) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 3!\n"); */
      part->crossx[count]= part->endxo[i]-1; 
      part->crossy[count++]= part->elem_starty + part->selem_endy[i];
      part->crossx[count]= part->crossx[count-1]; 
      part->crossy[count]= part->crossy[count-1]-1;
      count++;
    }
    if (part->startyo[i]!=0 && (part->elem_startx + part->selem_startx[i])!=0) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 4!\n"); */
      part->crossy[count]= part->startyo[i]; 
      part->crossx[count++]= part->elem_startx + part->selem_startx[i];
      part->crossy[count]= part->crossy[count-1]; 
      part->crossx[count]= part->crossx[count-1]-1;
      count++;
    }
    if (part->startyo[i]!=0 && (part->elem_startx + part->selem_endx[i])!=part->nx) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 5!\n"); */
      part->crossy[count]= part->startyo[i]; part->crossx[count++]= part->elem_startx+part->selem_endx[i];
      part->crossy[count]= part->crossy[count-1]; part->crossx[count]= part->crossx[count-1]-1;
      count++;
    }
    if (part->endyo[i]!=part->ny+1 && (part->elem_startx + part->selem_startx[i])!=0) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 6!\n"); */
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"(%i) endyo: %i\n",i,part->endyo[i]); */
      part->crossy[count]= part->endyo[i]-1; part->crossx[count++]= part->elem_startx+part->selem_startx[i];
      part->crossy[count]= part->crossy[count-1]; part->crossx[count]= part->crossx[count-1]-1;
      count++;
    }
    if (part->endyo[i]!=part->ny+1 && (part->elem_startx + part->selem_endx[i])!=part->nx) {
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"adding cross point 7!\n"); */
      part->crossy[count]= part->endyo[i]-1; part->crossx[count++]= part->elem_startx+part->selem_endx[i];
      part->crossy[count]= part->crossy[count-1]; part->crossx[count]= part->crossx[count-1]-1;
      count++;
      /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]: (%i) (%i,%i), (%i,%i)\n",rank,i,part->crossx[count-2],part->crossy[count-2],part->crossx[count-1],part->crossy[count-1]); */
    }
    part->crossend[i]= count;
    /* PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]: (%i) crossend= %i\n",rank,i,part->crossend[i]); */
  }

  PetscSynchronizedPrintf(PETSC_COMM_WORLD, "------- Leaving mypart2d ------------\n");
  ierr= PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
  

  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "mypart1d"
PetscErrorCode mypart1d(const PetscInt ix,const PetscInt nx,const PetscInt px,PetscInt *nxp, PetscInt *startx,PetscInt *endx)
{
  if (ix<(nx % px)) {
    (*nxp)= nx/px+1; (*startx)= ix*(*nxp);  (*endx)= (*startx) + (*nxp);
  }
  else {
    (*nxp)= nx/px; (*startx)= (nx%px)+ix*(*nxp); (*endx)= (*startx)+(*nxp);
  } 
  return 0;    
}

#undef __FUNCT__
#define __FUNCT__ "mypart2d_destroy"
PetscErrorCode mypart2d_destroy(MyPartition *part)
{
  PetscErrorCode ierr;
  ierr= PetscFree(part->isx);CHKERRQ(ierr);
  ierr= PetscFree(part->isy);CHKERRQ(ierr);
  ierr= PetscFree(part->nxs);CHKERRQ(ierr);
  ierr= PetscFree(part->nys);CHKERRQ(ierr);
  ierr= PetscFree(part->selem_startx);CHKERRQ(ierr);
  ierr= PetscFree(part->selem_starty);CHKERRQ(ierr);
  ierr= PetscFree(part->dimxs);CHKERRQ(ierr);
  ierr= PetscFree(part->dimys);CHKERRQ(ierr);
  ierr= PetscFree(part->dims);CHKERRQ(ierr);

  ierr= PetscFree(part->startxo);CHKERRQ(ierr);
  ierr= PetscFree(part->startyo);CHKERRQ(ierr);
  ierr= PetscFree(part->endxo);CHKERRQ(ierr);
  ierr= PetscFree(part->endyo);CHKERRQ(ierr);
  ierr= PetscFree(part->dimxso);CHKERRQ(ierr);
  ierr= PetscFree(part->dimyso);CHKERRQ(ierr);
  ierr= PetscFree(part->dimso);CHKERRQ(ierr);
  
  ierr= PetscFree(part->crossx);CHKERRQ(ierr);
  ierr= PetscFree(part->crossy);CHKERRQ(ierr);
  ierr= PetscFree(part->crossend);CHKERRQ(ierr);
  
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "mypart2d_view"
PetscErrorCode mypart2d_view(MyPartition *part, PetscMPIInt rank)
{
  PetscInt i;
  PetscErrorCode ierr;

  ierr= PetscSynchronizedPrintf (PETSC_COMM_WORLD,"[%i]: -------------- begin mypart2d_view -------------------\n",rank);
  ierr= PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: rx=%i, ry=%i\n",rank,part->rx,part->ry);CHKERRQ(ierr);
  PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: nxp=%i,  %i~%i, nyp=%i, %i~%i\n",rank,part->nxp,part->elem_startx,part->elem_endx-1,part->nyp,part->elem_starty,part->elem_endy-1);
  PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: dimp= %i, dimxp=%i, dimyp=%i\n",rank,part->dimp,part->dimxp,part->dimyp);
  for (i = 0; i < part->s; ++i) {
    PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: (%i): isx=%i, isy=%i\n",rank,i,part->isx[i],part->isy[i]);
    PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: (%i): nxs=%i,  %i~%i, nys=%i, %i~%i\n",rank,i,part->nxs[i],part->selem_startx[i],part->selem_endx[i]-1,part->nys[i],part->selem_starty[i],part->selem_endy[i]-1);
    PetscSynchronizedPrintf (PETSC_COMM_WORLD, "[%i]: (%i) dims= %i, dimxs=%i, dimys=%i\n",rank,i,part->dims[i],part->dimxs[i],part->dimys[i]);
    PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] (%i) with overlap: total %i nodes, x: %i~%i, y: %i~%i\n",rank,i,part->dimso[i],part->startxo[i],part->endxo[i]-1,part->startyo[i],part->endyo[i]-1);
  }
  /* for (i = 0; i < part->s; ++i) { */
  /*   PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]: (%i), cross-points 2D global subscripts: ",rank,i); */
  /*   for (int j = (i==0?0:part->crossend[i-1]); j < part->crossend[i]; ++j) { */
  /*     PetscSynchronizedPrintf(PETSC_COMM_WORLD,"(%i,%i) ", part->crossx[j],part->crossy[j]); */
  /*   } */
  /*   PetscSynchronizedPrintf(PETSC_COMM_WORLD,"\n"); */
  /* } */
  ierr= PetscSynchronizedPrintf (PETSC_COMM_WORLD,"[%i]: -------------- end mypart2d_view -------------------\n",rank);
  ierr= PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
  

  return 0;
}
