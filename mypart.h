#include <petscsys.h>

typedef struct
{
  PetscInt       nx;           /* number of elements along each direction */
  PetscInt       ny;
  PetscInt       px;           /* number of processors along each direction */
  PetscInt       py;
  PetscInt       sx;           /* number of subdomains along each direction */
  PetscInt       sy;
  PetscInt       overlap;      /* algebraic overlap */

  /* the following are determined by the above */
  PetscInt       dim;	       /* n. d. o. f. */
  PetscInt         p;          /* number of processors */
  PetscInt         s;          /* number of subdomains */
  PetscMPIInt    rx;           /* - - - - - - - - - - - - - - - - -  */
  PetscMPIInt    ry;           /* pseudo-subdomains: partition of    */
  PetscInt       nxp;          /* elements and nodes onto processors */     
  PetscInt       nyp;          /* rx, ry:                            */
  PetscInt       elem_startx;  /*    2D subscripts of processors     */
  PetscInt       elem_endx;    /*  nxp: number of elements on this   */
  PetscInt       elem_starty;  /*    processor along x-direction     */
  PetscInt       elem_endy;    /*  dimxp: number of nodes on this    */
  PetscInt       dimxp;	       /*    processor along x-direction     */
  PetscInt       dimyp;	       /*  endx is exclusive                 */
  PetscInt       dimp;         /* - - - - - - - - - - - - - - - - -  */

  PetscInt      *isx;	       /* - - - - - - - - - - - - - - - - - - - */
  PetscInt      *isy;	       /*  non-overlapping subdomains on this   */
  PetscInt      *nxs;	       /*      processor                        */
  PetscInt      *nys;	       /*  isx: 2D subscript of subdomain       */
  PetscInt      *selem_startx; /*  nxs: number of elements in subdomain */
  PetscInt      *selem_endx;   /*  dimxs: number of nodes in subdomain  */
  PetscInt      *selem_starty; /*  endx is exclusive                    */
  PetscInt      *selem_endy;   /*  numbering are local on this proc.    */
  PetscInt      *dimxs;	       /*                                       */
  PetscInt      *dimys;	       /*                                       */
  PetscInt      *dims;	       /* - - - - - - - - - - - - - - - - - - - */
  
  PetscInt      *startxo;      /* - - - - - - - - - - - - - - - - - - - */
  PetscInt      *endxo;	       /* overlapping subdomains on this proc.  */
  PetscInt      *startyo;      /* startxo, endxo are mark of the nodes  */
  PetscInt      *endyo;	       /*   but not elements                    */
  PetscInt      *dimxso;       /* numbering are for global domain       */
  PetscInt      *dimyso;       /*                                       */
  PetscInt      *dimso;	       /* - - - - - - - - - - - - - - - - - - - */

  PetscInt      *crossx;       /* where overlapping boundary cross the  */
  PetscInt      *crossy;       /* non-overlapping ones, stored in one   */
  PetscInt      *crossend;     /* array, crossend marks for subdomains, */
                               /* global 2D subscripts                  */
} MyPartition;

PetscErrorCode mypart2d_destroy(MyPartition *part);

PetscErrorCode mypart2d_view(MyPartition *part, PetscMPIInt rank);

PetscErrorCode mypart2d(MyPartition *part, PetscMPIInt rank);
