static char help[]= "read the file uvec and calculate the norms\n";

#include <petscvec.h>

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **args)
{
  PetscViewer binv;
  Vec u, u_old;
  PetscReal norm2, norminf;
  PetscErrorCode ierr;
  PetscScalar none= -1.0 + 0.0*PETSC_i;

  PetscInitialize(&argc,&args,(char *)0,help);

  ierr= VecCreate(PETSC_COMM_WORLD,&u);CHKERRQ(ierr);
  ierr= PetscViewerBinaryOpen(PETSC_COMM_WORLD, "ufem", FILE_MODE_READ, &binv);CHKERRQ(ierr);
  ierr= VecLoad(u,binv);CHKERRQ(ierr);  ierr= PetscViewerDestroy(&binv);CHKERRQ(ierr);
  ierr= VecDuplicate(u,&u_old);CHKERRQ(ierr);
  ierr= PetscViewerBinaryOpen(PETSC_COMM_WORLD, "ufem_old", FILE_MODE_READ, &binv);CHKERRQ(ierr);
  ierr= VecLoad(u_old,binv);CHKERRQ(ierr);  ierr= PetscViewerDestroy(&binv);CHKERRQ(ierr);
  ierr= VecAXPY(u,none,u_old);
  ierr= VecNorm(u,NORM_2,&norm2);CHKERRQ(ierr);
  ierr= VecNorm(u,NORM_INFINITY,&norminf);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"inf norm: %g,   2-norm: %g\n",norminf,norm2);
  ierr= VecDestroy(&u);CHKERRQ(ierr);
  ierr= VecDestroy(&u_old);CHKERRQ(ierr);

  PetscFinalize();
  return 0;
}
