#include "petscsys.h"

/* ----------------------------------------------------------------------- 
   Direct search for the optimal parameters by discretize the parameter
   space and evaluate the goal function at *every* point.
   
   ----------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DirectSearch"
PetscErrorCode DirectSearch(PetscReal *xopt, PetscReal *fopt, const char *filename, PetscReal (*f)(PetscReal *x, void *ctx), void *ctx, PetscInt dimx, const PetscReal *xl, const PetscReal *xr, const PetscInt *nj, PetscBool iwrite)
{
  PetscInt  *bj; /* when stride out to 1D from i-D, the block size */
  PetscInt  i, j, bn, ptsize, module;
  PetscErrorCode ierr;
  PetscReal *x, *dx, fval;
  FILE *file=0;

  PetscFunctionBegin;

  /* initialization */
  ierr= PetscMalloc(dimx*sizeof(PetscInt),&bj);CHKERRQ(ierr);
  bj[0]= 1;
  for (i = 1; i < dimx; ++i) {
    bj[i]= 1;
    for (j = 0; j < i; ++j) {
      bj[i]= bj[i]*(nj[j]+1);
    }
  }
  bn= bj[dimx-1]*(nj[dimx-1]+1);
  ptsize= dimx*sizeof(PetscReal);
  ierr= PetscMalloc(ptsize,&x);CHKERRQ(ierr);
  ierr= PetscMalloc(ptsize,&dx);CHKERRQ(ierr);
  for (j = 0; j < dimx; ++j) {
    if (nj[j]){
      dx[j]= (xr[j] - xl[j])/nj[j];
    }
    else {
      dx[j]= 0;
    }
  }
  if (iwrite){
    if (filename) {
      file= fopen(filename,"w");
    }
    if (!file) {
      printf("DirectSearch: Unable to open file!\n");
    }
    else {
      fprintf(file, "fvals= [ ");
    }
  }
  (*fopt)= 1e10;  /* a sufficiently large number */ 

  /* evaluate f at every point of the cartesian grid and find the minimum */
  for (i = 0; i < bn; ++i) {
    ierr= PetscMemzero(x,ptsize);CHKERRQ(ierr);
    module= i;
    for (j = dimx-1; j >=0; --j) {
      x[j]= xl[j] + (module/bj[j])*(dx[j]);
      module= module % bj[j];
    }
    fval= f(x,ctx); 
    if (file && iwrite) {
      fprintf(file, "%.0f  ", fval);
    }
    if ((*fopt)>fval) {
      (*fopt)= fval;
      ierr= PetscMemcpy(xopt,x,ptsize);CHKERRQ(ierr);
    }
  }
  if (file && iwrite) {
    fprintf(file, "];\n");
    fprintf(file, "xopt= [ ");
    for (i = 0; i < dimx; ++i) {
      fprintf(file, "%.4f  ",xopt[i]);
    }
    fprintf(file, "];\n");
    fprintf(file, "fopt= %.0f;",*fopt);
    fprintf(file, "p2= linspace(%f,%f,%d);\n",xl[1],xr[1],nj[1]+1);
    fprintf(file, "p4= linspace(%f,%f,%d);\n",xl[3],xr[3],nj[3]+1);
    fprintf(file, "fvals= reshape(fvals,%d,%d);\n",nj[3]+1,nj[1]+1);
    fprintf(file, "[p2, p4]= meshgrid(p2,p4);\n");
    fprintf(file, "[cc,ch]= contour(p2,p4,fvals,10);\n");
    fprintf(file, "clabel(cc,ch);\n");
  }
  
  ierr= PetscFree(bj);CHKERRQ(ierr);
  ierr= PetscFree(x);CHKERRQ(ierr);
  ierr= PetscFree(dx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
