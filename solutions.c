#include "helmbvp.h"

#undef __FUNCT__
#define __FUNCT__ "GreenFun"
inline PetscScalar GreenFun(PetscReal x,PetscReal y,PetscReal xs,PetscReal ys)
{
  PetscReal r= (x-xs)*(x-xs) + (y-ys)*(y-ys);
  r= sqrt(r);
  return PETSC_i*0.25*j0(helm_bvp.k_coef*r) - 0.25*y0(helm_bvp.k_coef*r);
}

#undef __FUNCT__
#define __FUNCT__ "ufun"
inline PetscScalar ufun(PetscReal x, PetscReal y)
{
  return cos(x+2.0*y);
}

#undef __FUNCT__
#define __FUNCT__ "SourceFunction"
inline PetscScalar SourceFunction(PetscReal x, PetscReal y)
{
  return 5.0*cos(x+2.0*y) - helm_bvp.k2*ufun(x,y); 
}

#undef __FUNCT__
#define __FUNCT__ "Dyufun"
inline PetscScalar Dyufun(PetscReal x, PetscReal y)
{
  return -2.0*sin(x+2.0*y);
}

#undef __FUNCT__
#define __FUNCT__ "Dxufun"
inline PetscScalar Dxufun(PetscReal x, PetscReal y)
{
  return -sin(x+2.0*y);
}

#undef __FUNCT__
#define __FUNCT__ "D2xufun"
inline PetscScalar D2xufun(PetscReal x, PetscReal y)
{
  return -cos(x+2.0*y);
}

#undef __FUNCT__
#define __FUNCT__ "D2yufun"
inline PetscScalar D2yufun(PetscReal x, PetscReal y)
{
  return -4.0*cos(x+2.0*y);
}

#undef __FUNCT__
#define __FUNCT__ "gabcxl"
inline PetscScalar gabcxl(PetscReal y)
{
  return ( -Dxufun(helm_bvp.xl, y) + helm_bvp.abc_coef[0]*ufun(helm_bvp.xl,y) - helm_bvp.abc_coef[1]*D2yufun(helm_bvp.xl,y) );
}

#undef __FUNCT__
#define __FUNCT__ "gabcxr"
inline PetscScalar gabcxr(PetscReal y)
{
  return ( Dxufun(helm_bvp.xr, y) + helm_bvp.abc_coef[0]*ufun(helm_bvp.xr,y) - helm_bvp.abc_coef[1]*D2yufun(helm_bvp.xr,y) );
}

#undef __FUNCT__
#define __FUNCT__ "gabcyl"
inline PetscScalar gabcyl(PetscReal x)
{
  return ( -Dyufun(x, helm_bvp.yl) + helm_bvp.abc_coef[0]*ufun(x,helm_bvp.yl) - helm_bvp.abc_coef[1]*D2xufun(x,helm_bvp.yl) );
}

#undef __FUNCT__
#define __FUNCT__ "gabcyr"
inline PetscScalar gabcyr(PetscReal x)
{
  return ( Dyufun(x, helm_bvp.yr) + helm_bvp.abc_coef[0]*ufun(x,helm_bvp.yr) - helm_bvp.abc_coef[1]*D2xufun(x,helm_bvp.yr) );
}

#undef __FUNCT__
#define __FUNCT__ "gnxl"
inline PetscScalar gnxl(PetscReal y)
{
  return ( -Dxufun(helm_bvp.xl, y) );
}

#undef __FUNCT__
#define __FUNCT__ "gnxr"
inline PetscScalar gnxr(PetscReal y)
{
  return ( Dxufun(helm_bvp.xr, y) );
}

#undef __FUNCT__
#define __FUNCT__ "gnyl"
inline PetscScalar gnyl(PetscReal x)
{
  return ( -Dyufun(x, helm_bvp.yl) );
}

#undef __FUNCT__
#define __FUNCT__ "gnyr"
inline PetscScalar gnyr(PetscReal x)
{
  return ( Dyufun(x, helm_bvp.yr) );
}

#undef __FUNCT__
#define __FUNCT__ "uxl"
inline PetscScalar uxl(PetscReal y)
{
  return ( ufun(helm_bvp.xl,y) );
}

#undef __FUNCT__
#define __FUNCT__ "uxr"
inline PetscScalar uxr(PetscReal y)
{
  return ( ufun(helm_bvp.xr,y) );
}

#undef __FUNCT__
#define __FUNCT__ "uyl"
inline PetscScalar uyl(PetscReal x)
{
  return ( ufun(x,helm_bvp.yl) );
}

#undef __FUNCT__
#define __FUNCT__ "uyr"
inline PetscScalar uyr(PetscReal x)
{
  return ( ufun(x,helm_bvp.yr) );
}
