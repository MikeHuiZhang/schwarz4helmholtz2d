#include "helmbvp.h"

#ifndef _HELM2D_SOLUTIONS
#define _HELM2D_SOLUTIONS

inline PetscScalar GreenFun(PetscReal x,PetscReal y,PetscReal xs,PetscReal ys);

inline PetscScalar ufun(PetscReal x, PetscReal y);

inline PetscScalar SourceFunction(PetscReal x, PetscReal y);

inline PetscScalar Dyufun(PetscReal x, PetscReal y);

inline PetscScalar Dxufun(PetscReal x, PetscReal y);

inline PetscScalar D2xufun(PetscReal x, PetscReal y);

inline PetscScalar D2yufun(PetscReal x, PetscReal y);

inline PetscScalar gabcxl(PetscReal y);

inline PetscScalar gabcxr(PetscReal y);

inline PetscScalar gabcyl(PetscReal x);

inline PetscScalar gabcyr(PetscReal x);

inline PetscScalar gnxl(PetscReal y);

inline PetscScalar gnxr(PetscReal y);

inline PetscScalar gnyl(PetscReal x);

inline PetscScalar gnyr(PetscReal x);

inline PetscScalar uxl(PetscReal y);

inline PetscScalar uxr(PetscReal y);

inline PetscScalar uyl(PetscReal x);

inline PetscScalar uyr(PetscReal x);

#endif
